// FUNCTION BASE  ------------------------------------//

var $http = "https://www.itimgest.com/api/";
var $httpBase = $http + "web_base.php";


//COOKIE -----------------------------------------------
function setCookie(cname, cvalue, exdays) {
	var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("client_ii");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("client_ii", user, 365);
        }
    }
}

// -----------------------------------------------------



function logout(){
	setCookie('client_ii','',0)
	setCookie('client_iie','',0)
	setCookie('client_iin','',0)
	
	location.reload();
}
$(".logout").click(function(){
	logout();
});

function set_session_ii(cfg) {
    var data = {
        op: 'set_session',
        values: cfg
    }
    $.post($httpBase, data)
}

function client_log_menu(){
	if(getCookie('client_iie')){
		$("#client_log_menu").html('<a href=\"favorites_list.php\" style=\"width:auto;\">Ciao ' + getCookie('client_iie') + '</a>');
	}
}
client_log_menu();

/* PREFERITI */

function remove_favourite(e, t, o) {
		toggle_favourite(e, t, o);
	    var n = $(e).closest(".estate-list-item");
	    $(n).fadeOut(400, function() {
	        $(this).remove()
	    });
	
}
function toggle_favourite(e, t, o) {
	
	var user = getCookie("client_ii");
	if (user != "") {
		$(e).toggleClass("like-icon-active");
		
	    var n = {
	        estate_id: t,
	        client_id: user,
	        op: "toggle_favourite"
	    };
	    console.log(n);
	    $.post($httpBase, n, function(e) {
	    	console.log(e);
	    });
	}else{
		alert('Ciao! Per accedere a questa funzionalità devi effettuare prima l\'accesso alla tua area riservata \"MY Italiana Immobiliare\"');
	}
	 
}
/* FINE PREFERITI */



!function(e) {
    e.fn.ajax_form = function(t) {
    	
        t.on_success = "undefined" != typeof t.on_success ? t.on_success : function() {
            location.reload();
        }, this.each(function() {
            var o = e(this);
            o.submit(function(n) {
                n.preventDefault(), o.find(".alert-danger").hide(), o.find(".alert-success").hide();
                var s = o.serialize();
                e.post(o.attr("action"), s, function(e) {
                	
                	switch (console.log(e), e.json_status) {
                        case 1:
                            t.on_success(e, o);
                            break;
                        case 2:
                            t.on_login(e, o);
                            break;    
                        case 0:
                            t.on_fail(e, o);
                            break;
                        default:
                            console.log(e)
                    }
                }, "json")
            })
        })
    }
}(jQuery), $(document).ready(function() {
	
	 $(".ajax-form").ajax_form({
        on_success: function(e, t) {
        	var o = t.find(".alert-success");
            o.addClass('show'), o.find(".message-value").html(e.json_content), t.hasClass("send-request-form") && ga("send", "event", "button", "click", "richiesta")
        },
        on_login: function(e, t) {
        	var o = t.find(".alert-success");
            o.addClass('show'), o.find(".message-value").html(e.json_content), t.hasClass("send-request-form") && ga("send", "event", "button", "click", "richiesta");
            // HA EFFETTUATO l'ACCESSO
        	//SETTO I COOKIE
            setCookie('client_ii', e.client.client_id, 30);
            setCookie('client_iie', e.client.client_email, 30);
            setCookie('client_iin', e.client.client_name, 30);            
            setInterval(function(){
            	location.reload();
            }, 2000);
            
        },
        on_fail: function(e, t) {
            var o = t.find(".alert-danger");
            o.addClass('show'), o.find(".message-value").html(e.json_content)
        }
    }), $(".only-numbers").only_numbers()
}), jQuery.fn.only_numbers = function() {
     this.each(function() {
        $(this).keydown(function(e) {
            if (1 == e.shiftKey)
                return e.preventDefault(), !1;
            var t = e.charCode || e.keyCode || 0;
            return 8 == t || 9 == t || 46 == t || 110 == t || 190 == t || t >= 35 && 40 >= t || t >= 48 && 57 >= t || t >= 96 && 105 >= t
        })
    })
};
