function supports_html5_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

var coords = [];
var OSMmap;

var icoRedDot = L.icon({
    iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg',
    iconSize:     [16, 16], // size of the icon
});

var estateIcon = L.icon({
    iconUrl: 'https://www.italianaimmobiliare.it/web2/favicon.png',
    iconSize:     [16, 16], // size of the icon
})

window.mapMarkers = new Array();

function initOSM(lat,lng) {
  coords['lat'] = lat;
  coords['lng'] = lng;
  window.L = L;
  //window.map = L.map('OSMmap').setView([lat, lng ], 14);
  window.map = L.map('OSMmap',{
    center: [lat, lng],
    zoom: 14,
    scrollWheelZoom:false
  });

  window.map.on('click', function() {
  if (window.map .scrollWheelZoom.enabled()) {
    window.map .scrollWheelZoom.disable();
    }
    else {
    window.map .scrollWheelZoom.enable();
    }
  });


  console.log("OSMmap" + window.map);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { attribution: 'Map data &copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors', maxZoom: 18 }).addTo(window.map);
  noLoc = false;
  var marker = window.L.marker([lat,lng], {icon: estateIcon});
  marker.addTo(window.map);
}

function loadPoi() {
  $('#POIresults').html('<p>Ricerco il dato richiesto...</p><img src="./images/spinner.gif" style="margin: auto;display: block;">');
  var q = '';
  $("select option:selected").each(function() {
    q += $(this).val() + ' ';
  });


  $.ajax({'url' :'POI_service.php',
          'data' : {'q': q, 'lat':coords.lat,
          'lon':coords.lng}}).done(function(msg) {
            $('#POIresults').html(msg);
        });
}
