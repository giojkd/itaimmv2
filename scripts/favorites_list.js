$(function() {
		var routes = [
		{
			path: '/',
			name:'result'
		}
	]

	var router = new VueRouter({
		mode:'history',
		routes // short for `routes: routes`
	});

	
	var estates = new Vue({
		  router:router,
		  route:{
			  canReuse:false
		  },
		  el: '#estatesList',
		  data: {
			  estatesList : []	    
		  },
		  watch:{
			  $route(to,from){
				  this.$route.name
			  }
		  },
		  created(){
			  
			  	var client_id = getCookie("client_ii");
			  	if (client_id != "") {
					
					$("#accedi").addClass("d-none");	
					
					$("#client_log_name").html(getCookie("client_iin"));
					$("#client_log_email").html(getCookie("client_iie"));
					
					fetch('https://www.itimgest.com/api/web_client_favorites.php?client_id=' + client_id)
					.then(response => response.json())
					.then(json => {
						this.estatesList = json;
						}
					 )
				}else{
					
					$("#message_preferiti").removeClass("d-none");
					$("#client_log").addClass("d-none");
					
					
				}
		  }
	});
});


