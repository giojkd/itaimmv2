var $http = "https://www.itimgest.com/api/";
var $httpBase = $http + "web_base.php";



function facebookLoginStatus() {
    FB.getLoginStatus(function(e) {
        "connected" === e.status || "not_authorized" === e.status && login_fb()
    })
}

function login_fb() {
    FB.login(function(e) {
        e.authResponse && FB.api("/me", function(e) {
            console.log(e), $.ajax({
                type: "POST",
                url: $httpBase,
                data: {
                    op: "facebook_login",
                    id_facebook: e.id,
                    first_name: e.first_name,
                    last_name: e.last_name,
                    email: e.email,
                    gender: e.gender,
                    last_name: e.last_name
                },
                success: function(e) {
                    location.reload()
                }
            })
        })
    }, {
        scope: "email, user_birthday, user_about_me"
    })
}

function testAPI() {
    console.log("Welcome!  Fetching your information.... "), FB.api("/me", function(e) {
        console.log("Good to see you, " + e.name + ".")
    })
}
window.fbAsyncInit = function() {
        FB.init({
            appId: "289607537906655",
            version: "v2.2",
            status: !0,
            cookie: !0,
            xfbml: !0
        }), function(){}
    },
    function(e) {
        var o, n = "facebook-jssdk",
            t = e.getElementsByTagName("script")[0];
        e.getElementById(n) || (o = e.createElement("script"), o.id = n, o.async = !0, o.src = "//connect.facebook.net/it_IT/all.js", t.parentNode.insertBefore(o, t))
    }(document);