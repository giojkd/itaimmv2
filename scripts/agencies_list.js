$(function() {
	
	
	Vue.component('v-select', VueSelect.VueSelect);
	var zones = new Vue({
		  el: '#agenzie_cities',
		  data: {
			  istatList : [],
			  selectedZone:'',
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_status' : 1
							},
							'order_by' : 'agency_city asc',
							'group_by' : 'agency_city asc'	
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.istatList = json;
					this.zoneTotList = json;
					}
				 )			
		}
		,computed : {
			zonelisted: function(){
				var returnList = []; 
				for(var item in this.istatList){
					returnList.push({'label': this.istatList[item].agency_city, 'value' : this.istatList[item].agency_city })
				}
				return returnList;
			}
		}
	});
	
	
	 
	
	var agencies = new Vue({
		  el: '#agenciesList_Firenze',
		  data: {
			  agenciesList_Firenze : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Firenze'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Firenze = json
					}
				 )
			
		}
	});
	
	
	
	var agencies2 = new Vue({
		  el: '#agenciesList_Bagno_a_Ripoli',
		  data: {
			  agenciesList_Bagno_a_Ripoli : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'in' : {
								'agency_city' : ['\'Bagno a Ripoli\'', '\'Grassina\'']
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Bagno_a_Ripoli = json
					}
				 )
			
		}
	});
	
	
	
	var agencies3 = new Vue({
		  el: '#agenciesList_Campi_Bisenzio',
		  data: {
			  agenciesList_Campi_Bisenzio : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Campi Bisenzio'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Campi_Bisenzio = json
					}
				 )
			
		}
	});
	
	
	var agencies4 = new Vue({
		  el: '#agenciesList_Scandicci',
		  data: {
			  agenciesList_Scandicci : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Scandicci'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Scandicci = json
					}
				 )
			
		}
	});
	
	var agencies5 = new Vue({
		  el: '#agenciesList_Prato',
		  data: {
			  agenciesList_Prato : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Prato'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Prato = json
					}
				 )
			
		}
	});
	
	
	
	var agencies6 = new Vue({
		  el: '#agenciesList_Pontassieve',
		  data: {
			  agenciesList_Pontassieve : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'in' : {
								'agency_city' : ['\'Pontassieve\'']
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Pontassieve = json
					}
				 )
			
		}
	});
	
	
	var agencies7 = new Vue({
		  el: '#agenciesList_Sesto_Fiorentino',
		  data: {
			  agenciesList_Sesto_Fiorentino : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Sesto Fiorentino'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Sesto_Fiorentino = json
					}
				 )
			
		}
	});
	
	
	
	var agencies8 = new Vue({
		  el: '#agenciesList_Fiesole',
		  data: {
			  agenciesList_Fiesole : []	    
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_agencies.php?'+jQuery.param(
					{
						'filters':{
							'equal' : {
								'agency_city' : 'Fiesole'
							},
							'order_by' : 'agency_ordinamento asc'
						}
					}
			)
			)
			.then(response => response.json())
			.then(json => {
					this.agenciesList_Fiesole = json
					}
				 )
			
		}
	});
	
	
	
	
	
	$("#button_agenzia_zona").click(function(){
		obj = "#" + $("#agenzia_zona").val();
		 $('html, body').animate({
             scrollTop: $(obj).offset().top - ($("#header").height() + 20)
         }, 2000);
		return false;
	});
	
	$("#button_to_map").click(function(){	
		
		//console.log($(this).attr("href"));
		obj = $(this).attr("href");		
		$('html, body').animate({
            scrollTop: $(obj).offset().top - ($("#header").height() + 20)
        }, 1000);
		return false;
	});
	
	
	
});

var map_center = {lat: 43.771033, lng: 11.248001}; //FIRENZE CENTRO
var map_center_google = new google.maps.LatLng(43.771033, 11.248001);

var infowindow;
var map;
var agency_markers_array = new Array();


function create_map_first_time() {
	create_map(map_center);
}

function create_map(map_center) {
    var mapOptions = {
        center: map_center,
        zoom: 11
    };
    map = new google.maps.Map(document.getElementById('map-canvas-agencies'),
            mapOptions);

    
    add_markers(map);

    /*
    google.maps.event.addListener(map, 'click', function (event) {
        if (infowindow) {
            infowindow.close();
        }
    })
    */
}
google.maps.event.addDomListener(window, 'load', create_map_first_time);



function add_markers(map) {
	
	$.post("https://www.itimgest.com/api/web_agencies.php", function(response) {
		agencies_details = response;
		
		for (var index in agencies_details) {
			
			var agency = agencies_details[index];
			
			
	        var agency_id = agency.agency_id;
	        var agency_address_id = agency.agency_address_id;
	        agency_markers_array[index] = new google.maps.Marker({
	            position: {lat: parseFloat(agency.agency_lat), lng: parseFloat(agency.agency_lng)},
	            map: map,
	            title: agency.agency_name,
	            //id: agency_address_id,
	            id: index
	            //icon: "../imgs/icone/map-estate-marker-grey.png"
	        });
	        
	        google.maps.event.addListener(agency_markers_array[index], 'click', function (evt) {
	        	
	        	
                if (infowindow) {
                    infowindow.close();
                }
                infowindow = new google.maps.InfoWindow();
                infowindow = new google.maps.InfoWindow({
                    content: 'Caricamento...'
                });
                /*
                var load_data = {
                    op: 'map_agency_marker_content',
                    id: this.id
                }
                $.post('/op.execute.php', load_data, function (risp) {
                    infowindow.setContent(risp);
                })*/
                
                risp = agency_info_window(agencies_details[this.id]);
                
                infowindow.setContent(risp);
                infowindow.open(map, this)
            });
	        
	        

	    }
		
		$('.agency-item').hover(function () {
	        var agency = $(this);
	        var agency_id = $(this).attr('agency-id');
	        var agency_address_id = $(this).attr('agency-address-id');
	        
	        //agency_markers_array[agency_address_id].setIcon("/imgs/icone/map-estate-marker-red.png");

	    }, function () {
	        var agency = $(this);
	        var agency_id = $(this).attr('agency-id');
	        var agency_address_id = $(this).attr('agency-address-id');
	        //agency_markers_array[agency_address_id].setIcon("/imgs/icone/map-estate-marker-grey.png");
	    });
		
	}, 'json'); 
	
    
}


function show_agencies() {
    $('#agencies-page-wrap').fadeOut(500, function () {
        $("#map-canvas-agencies").css({'position': 'fixed'}).animate({
            width: '40%'
        }, 500, function () {
            google.maps.event.trigger(map, 'resize');
            map.panTo(map_center_google);
            map.setZoom(11);
            for (var index in agencies_details) {
                var agency = agencies_details[index];
                var agency_id = agency.agency_id;
                var agency_address_id = agency.agency_address_id;
                google.maps.event.addListener(agency_markers_array[agency_address_id], 'click', function (evt) {
                    if (infowindow) {
                        infowindow.close();
                    }
                    infowindow = new google.maps.InfoWindow();
                    infowindow = new google.maps.InfoWindow({
                        content: 'Caricamento...'
                    })
                    var load_data = {
                        op: 'map_agency_marker_content',
                        id: this.id
                    }
                    $.post('/op.execute.php', load_data, function (risp) {
                        infowindow.setContent(risp);
                    })
                    infowindow.open(map, this)
                });
            }
        });
    })
    $('#agencies-list').fadeIn(500)
}

function watch_agency_on_map(agency_address_id) {
    map.panTo(agency_markers_array[agency_address_id].getPosition());
    map.setZoom(13);
    google.maps.event.trigger(agency_markers_array[agency_address_id], 'click');
}



function agency_info_window(agency){
	
	html = "<div class='text-center' style='margin-left:20px;padding:10px'>";
	html += "<img src='https://www.italianaimmobiliare.it/agenzie/" + agency.agency_logo_path + "' height='80' class='img-circle'>";
	html += "<h5>" + agency.agency_company_region + "</h5>";
	html += "<p>" + agency.agency_address + " - " + agency.agency_city + "</p>";
	html += "<p>" + agency.agency_phone + "<br>";	
	html += "" + agency.agency_email + "</p>";	
	html += "</div>"
	
	return html;
	
}





