UrlEncoder = {

	/**
	* Encode a [deeply] nested object for use in a url
	* Assumes Array.each is defined
	*/
	encode: function(params, prefix) {
	
		var items = [];
		
		for(var field in params) {
			
			var key  = prefix ? prefix + "[" + field + "]" : field;
			var type = typeof params[field];
			
			switch(type) {
			
				case "object":
					
					//handle arrays appropriately x[]=1&x[]=3
					if(params[field].constructor == Array) {
						params[field].each(function(val) {
							items.push(key + "[]=" + val);
						}, this);
					} else {
						//recusrively construct the sub-object
						items = items.concat(this.encode(params[field], key));
					}
					break;
				case "function":
					break;
				default:
					items.push(key + "=" + escape(params[field]));
					break;
			}
		}

		return items.join("&");
	},
	
	/**
	* Decode a deeply nested Url
	*/
	decode: function(params) {
		
		var obj	  = {};
		var parts = params.split("&");
		
		parts.each(function(kvs) {

			var kvp = kvs.split("=");
			var key = kvp[0];
			var val = unescape(kvp[1]);
			
			if(/\[\w+\]/.test(key)) {

				var rgx = /\[(\w+)\]/g;
				var top = /^([^\[]+)/.exec(key)[0];
				var sub = rgx.exec(key);
				
				if(!obj[top]) {
					obj[top] = {};
				}
				
				var unroot = function(o) {
					
					if(sub == null) {
						return;
					}
					
					var sub_key = sub[1];
					
					sub = rgx.exec(key);
					
					if(!o[sub_key]) {
						o[sub_key] = sub ? {} : val;
					}
					
					unroot(o[sub_key]);
				};
				
				
				unroot(obj[top]);
				
			//array
			} else if(/\[\]$/.test(key)) {
				key = /(^\w+)/.exec(key)[0];
				if(!obj[key]) {
					obj[key] = [];
				}
				obj[key].push(val);
			} else {
				obj[key] = val;
			}
			
		});
		
		return obj;
	}
	
};

$(function() {
	
Vue.component('v-select', VueSelect.VueSelect);
	
	var zones = new Vue({
		  el: '#zonesList',
		  data: {
			  istatList : [],
			  selectedZone:''
		  }
		,created (){
			var urlEncoder = new UrlEncoder();
			console.log(urlEncoder.decode(window.location.href));

			fetch('https://www.itimgest.com/api/zones_list.php')
			.then(response => response.json())
			.then(json => {
					this.istatList = json;
					this.zoneTotList = json;
					}
				 )
		}
		,computed : {
			zonelisted: function(){
				var returnList = []; 
				for(var item in this.istatList){
					returnList.push({'label': this.istatList[item].name, 'value' : this.istatList[item].ISTAT,'area_type' : this.istatList[item].area_type })
				}
				return returnList;
			}
		}
	});
	
	
	var tipologies = new Vue({
		  el: '.tipologiesList',
		  data: {
			  tipologiesList : [],
			  selectedTipologies:'',
		  }
		,created (){
			fetch('https://www.itimgest.com/api/tipologies_estates.php')
			.then(response => response.json())
			.then(json => {
					this.tipologiesList = json
					}
				 )
		}
		,computed : {
			tipologieslisted: function(){
				var returnList = []; 
				for(var item in this.tipologiesList){
					returnList.push({'label': this.tipologiesList[item].NomeTipologia, 'value' : this.tipologiesList[item].IDTipologia })
				}
				return returnList;
			}
		}
	});
	
	
	
	var routes = [
		{
			path: '/',
			name:'result'
		}
	]

	var router = new VueRouter({
		mode:'history',
		routes // short for `routes: routes`
	})

	
	var estates = new Vue({
		  router:router,
		  route:{
			  canReuse:false
		  },
		  el: '#estatesList',
		  data: {
			  estatesList : []	    
		  },
		  watch:{
			  $route(to,from){
				  this.$route.name
			  }
		  },
		  created(){
			  
			  console.log(query_string); // qui c'è quello che ti serve
			  
				fetch('https://www.itimgest.com/api/web_estates.php?filters[e.status]=0&'+$.param(this.$route.query))
				.then(response => response.json())
				.then(json => {
						this.estatesList = json;
						
						mainMap(this.estatesList);
						
						}
					 )
		  },
			methods: {
			    vuefavorite: function (obj, estate_id,user_id) {
			    	toggle_favourite(obj, estate_id, user_id); 
			    }
			  }
	});

});


$(document).ready(function(){
	var url_string = window.location.href
	var url = new URL(url_string);
	var ISTAT = url.searchParams.get("filters[area_type_and_id][0][ISTAT]");
	var area_type = url.searchParams.get("filters[area_type_and_id][0][area_type]");

	function selected(){
		$("#search_ISTAT").val(ISTAT);
		$("#search_area_type").val(area_type);	
	}
	
});


