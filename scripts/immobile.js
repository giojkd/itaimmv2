$(function() {

	var url_string = window.location.href
	var url = new URL(url_string);
	var estate_id = url.searchParams.get("estate_id");


	var imm = new Vue({
		  el: '#wrapper',
		  data: {
			  estates : [],
			  estatesList : []
		  }
		,created (){
			fetch('https://www.itimgest.com/api/web_estates.php?'+jQuery.param(
					{
						'filters':{
							'equal':{
								'e.estate_id': estate_id
							}
						}
					})
			)
			.then(response => response.json())
			.then(json => {
					this.estates = json;

					fetch('https://www.itimgest.com/api/web_estates.php?'+jQuery.param(
							{
								'filters':{
									'equal':{
										'Contratto': this.estates[0].Contratto,
										'e.status' : 0,
										'ISTAT': this.estates[0].ISTAT,
										'IDTipologia' : this.estates[0].IDTipologia
									},
									'notequal':{
										'e.estate_id' : this.estates[0].estate_id
									},
									'between' : {
										'Prezzo' : {
											'to' : this.estates[0].Prezzo,
										},
										'NrLocali':{
											'from' : this.estates[0].NrLocali
										}
									},
									'limit' : 3,
									'order_by' : 'insert_timestamp desc'
								}
							}
					)
					)
					.then(response => response.json())
					.then(json => {
							this.estatesList = json;
							this.inlineCSS();
							initOSM(this.estates[0].lat,this.estates[0].lng);
							
							// Single Property Map Init
						    var single_map =  document.getElementById('propertyMap');
						    if (typeof(single_map) != 'undefined' && single_map != null) {
						    	singlePropertyMap();
						    }
							
							
							}
						 )



					}
				 );

		},
		methods: {
			inlineCSS: function(){
				// Common Inline CSS
				$(".some-classes, section.fullwidth, .img-box-background, .flip-banner, .property-slider .item, .fullwidth-property-slider .item, .fullwidth-home-slider .item, .address-container").each(function() {
					var attrImageBG = $(this).attr('data-background-image');
					console.log(attrImageBG);
					var attrColorBG = $(this).attr('data-background-color');

							if(attrImageBG !== undefined) {
									$(this).css('background-image', 'url('+attrImageBG+')');
							}

							if(attrColorBG !== undefined) {
									$(this).css('background', ''+attrColorBG+'');
							}
				});

				var energyClass = $('#estateEnergyClass').html().trim();
				var text = ''

				text= "Classe Energetica: " + energyClass;

				switch (energyClass) {
					case 'A4':
						$("#energyClassA").append('<div class="curr-class">'+text+'</div>');
						break;
					case 'A3':
						$("#energyClassA").append('<div class="curr-class">'+text+'</div>');
						break;
					case 'A2':
						$("#energyClassA").append('<div class="curr-class">'+text+'</div>');
						break;
					case 'A1':
						$("#energyClassA").append('<div class="curr-class">'+text+'</div>');
						break;
					default:
						$("#energyClass"+energyClass).append('<div class="curr-class">'+text+'</div>');
						break;
				}

				$('.fullwidth-property-slider').slick({
					centerMode: true,
					centerPadding: '20%',
					slidesToShow: 1,
					responsive: [
						{
							breakpoint: 1367,
							settings: {
								centerPadding: '15%'
							}
						},
						{
							breakpoint: 993,
							settings: {
								centerPadding: '0'
							}
						}
					]
				});
			}
		}
	});

});
