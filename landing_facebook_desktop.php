<?
include $_SERVER['DOCUMENT_ROOT'] . '/include/include.inc.php';
//include ROOT . '/include/session_setter.inc.php';
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script type="text/javascript" src="landing/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="landing/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="landing/js/functions.js"></script>
        <script type="text/javascript" src="landing/js/autocomplete.google.js"></script>
        <script type="text/javascript" src="landing/js/facebook_login.js"></script>
        <script type="text/javascript" src="landing/js/lightbox.min.js"></script>
        
        
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7_6qXx1LlpVA576SUsX9OPSdh0jw2WIo&amp;libraries=places"></script>
        <link href="landing/css/style.css" rel="stylesheet">
        <link href="landing/css/masonry.css" rel="stylesheet">
        <script src="https://use.fontawesome.com/92b39d1d6f.js"></script>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        
		
		
		<script src="landing/bootstrap/js/bootstrap.js"></script>
        <script src="landing/bootstrap/js/bootstrap-affix.js"></script>
        <link href="landing/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!--  MULTISELECT  -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/it.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css" type="text/css"/>
        
        
        <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="ca07d4b0-7d94-41a1-b497-edb2a004ef47" type="text/javascript" async></script>
		
        <title>Dove stai cercando casa?</title>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-6955382049467124",
                enable_page_level_ads: true
            });
        </script>
        <style>
            #facebookLandingFooter{
                background-color:#333;
                color:#fff;
                text-align: center;
                padding:20px;
                margin-top:20px;
            }
        </style>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-57193554-1', 'auto');
          ga('send', 'pageview');
        
        </script>
    </head>
    <body>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WFSXVN" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WFSXVN');
        </script>
        <!-- End Google Tag Manager -->
        
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= SITE_URL ?>"><img src='imgs/italiana-immobiliare-spa_logo.png' height="40"></a>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <?
        height_spacer(70);
        $_GET['citta'] = (isset($_GET['citta'])) ? $_GET['citta'] : 'firenze';
        $comuni = qa3("SELECT nome_comune, ISTAT FROM immobiliareit_istat_2010 WHERE nome_citta_metropolitana LIKE '{$_GET['citta']}' OR sigla_automobilistica = 'PO' ");
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header">
                        Dove stai cercando casa?
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <form class="form" method="POST" action="op.execute.php">
                        <input type="hidden" name="op" value="newFacebookLandingRequest">
                        <input type='hidden' name='landing_type' value='landing_desktop'>
                        <input id='modal-landing-comune' type="hidden" name="form_data[comune]" value="">
                        <input id='modal-landing-comune' type="hidden" name="form_data[device]" value="desktop">
                        <div class='form-group form-group-lg'>
                            <label>In che comune?</label>
                            <select id='ISTAT-select' class='form-control' name='form_data[ISTAT]'>
                                <option value=''>Scegli...</option>
                                <?
                                foreach ($comuni as $comune) {
                                    ?>
                                    <option value='<?= $comune['ISTAT'] ?>' <?=($comune['ISTAT']=='048017') ? 'selected' : ''?>>
                                        <?= $comune['nome_comune'] ?>
                                    </option>
                                <? } ?>
                            </select>
                        </div>
                        <div id='modal-form-bottom-part'></div>
                    </form>
                </div>
                <div class="col-md-4 col-md-offset-1">
                    <ul class="list list-unstyled">
                        <li class="text-center">
                            <h2>Veloce</h2>
                            <i class="fa fa-rocket fa-4x"></i>
                            <p>
                            Con oltre 500 nuovi immobili al mese trovare la tua nuova casa sarà un lampo!
                            </p>
                        </li>
                        <li class="text-center">
                            <h2>Sicuro</h2>
                            <i class="fa fa-university fa-4x"></i>
                            <p>
                            I nostri 35 anni di esperienza a la nostra riconosciuta professionalità sono le nostre garanzie di eccellenza.
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('#ISTAT-select').change(function () {
                    $('#modal-landing-comune').val($(this).find('option:selected').text());
                    $('#modal-form-bottom-part').load('/landing_desktop.load.php?ISTAT=' + $(this).val());
                })
                $('#ISTAT-select').trigger('change');
            })
        </script>
        <div id="facebookLandingFooter">
            Italiana Immobiliare S.p.a. franchising immobiliare - PIVA: 03033690482 - Tutti i diritti riservati
        </div>
        <? cookies_alert() ?>
    </body>
</html>