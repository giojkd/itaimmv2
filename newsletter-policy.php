<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Privacy Policy - Termini e condizioni | Italiana Immobiliare</title>
<meta name="description" content="Informazioni, ai sensi dell’artt. 12, 13, 14 del Regolamento Privacy Europeo N. 2016/679"/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>
<!-- Banner
================================================== -->


<div class="container">
	<div class="row">
		<div class="col-md-12 contenuto">
			
            
             <h2>Newsletter Policy</h2>

                                    
                                        <p>Gentile Interessato,<br>
di seguito le forniamo alcune informazioni che è necessario portare alla sua conoscenza, non solo per ottemperare agli obblighi di legge, ma anche perché la trasparenza e la correttezza nei confronti degli interessati è parte fondante della nostra attività.</p>

<p><strong>Il trattamento in oggetto è quello relativo all’invio della newsletter</strong>.</p>

<p>I suoi dati personali sono raccolti e trattati per le finalità riportate di seguito insieme alla base giuridica di riferimento:</p>

<ul>
	<li><b>Finalità</b><br>
	Informazioni alla clientela di nuovi servizi/prodotti e/o informazione commerciali generiche</li>
	<li><strong>Dati trattati</strong><br>
	Nome, Cognome, Indirizzo e-mail</li>
	<li><strong>Base Giuridica</strong><br>
	I trattamenti posti in essere per queste finalità vengono effettuati con lo specifico consenso fornito dall’utente.</li>
</ul>

<p>Per semplificare la comprensione di cosa verrà effettuato, le diamo la seguente descrizione estesa: il trattamento in oggetto consiste nell'invio da parte del titolare del trattamento di newsletter a carattere informativo sui prodotti o servizi di interesse del proprietario della mail.</p>

<p>Il trattamento potrà avvenire solo ed esclusivamente dopo il doppio opt-in dell'utente (iscrizione e conferma della mail tramite apposito link) e quindi attraverso la raccolta del Suo consenso. Non sarà possibile iscrivere mail alla newsletter senza che l’utente abbia la possibilità di controllarne i contenuti (e quindi confermare l’iscrizione).</p>

<p><b>Destinatari:</b> I suoi dati non saranno comunicati ad alcun destinatario.</p>

<p><b>Trasferimenti</b>: I suoi dati non saranno trasferiti verso alcun paese terzo.</p>

<p>Oltre a queste informazioni, per garantirle un trattamento dei suoi dati il più corretto e trasparente possibile, deve essere a conoscenza del fatto che:</p>

<ul>
	<li><strong>La durata del trattamento è determinata come segue</strong>:<br>
	I dati saranno trattati per un periodo congruo rispetto alle finalità preposte dalla newsletter. Poiché l’iscrizione è sempre finalizzata all’aggiornamento continuo dell’utente, i dati saranno utilizzati per l’invio fino a quando non verrà effettuata la revoca del consenso o fino a quando decadrà la finalità del Titolare del trattamento;</li>
	<li>Data di inizio del trattamento: 01/07/2018;</li>
	<li>Ha il diritto di chiedere al titolare del trattamento l'accesso ai suoi dati personali e la rettifica o la cancellazione degli stessi o la limitazione del trattamento che la riguarda o di opporsi al loro trattamento.<br>
	Maggiori informazioni circa l’esercizio dei diritti potranno essere visionate nel Regolamento (UE) 2016/679 agli articoli 15-16-17-18-19-20-21 seguendo il seguente link<br>
	<a href="%22">https://www.garanteprivacy.it/documents/10160/0/Regolamento+UE+2016+679.+Con+riferimenti+ai+considerando</a></li>
	<li>Se ha fornito il consenso per una o più specifiche finalità, ha il diritto di revocare tale consenso in qualsiasi momento (anche cliccando direttamente sul link di disiscrizione contenuto nelle singole mail inviate);</li>
	<li>Ha il diritto di proporre reclamo alla seguente Autorità di Controllo: Garante per la protezione dei dati personali.</li>
</ul>

<p>Il titolare del trattamento è <strong>Italiana Immobiliare Spa,&nbsp;via Ponte alle Mosse, 136/138 – 50141 Firenze -&nbsp;P.IVA 03033690482</strong></p>

<p>Il titolare può essere contattato per l’esercizio dei diritti ai seguenti recapiti:</p>

<p><strong>Mail. italiana@italianaimmobiliare.it.</strong></p>

<p>&nbsp;</p>

<p>Firenze, 01/10/2018</p>

            
            <div style="height:100px;"></div>
		</div>
	</div>
</div>


<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>



</div>
<!-- Wrapper / End -->

				

</body>
</html>