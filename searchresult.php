<?php
parse_str($_SERVER['QUERY_STRING'],$query_string);
?>

<!DOCTYPE html>
<head>
<?php
$req = $_REQUEST['filters'];
?>
<!-- Basic Page Needs
================================================== -->
<title>Appartamenti e case in vendita e affitto a Firenze</title>
<meta name="description" content="Appartamenti e case in vendita e in affitto a Firenze. Affittare, comprare o vendere casa a Firenze è facile. Realizziamo i sogni degli italiani."/>

<?include 'include/top.inc.php'; ?>
<script type="text/javascript">
    var query_string = <?php echo json_encode($query_string)?>
</script>
</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>
<!-- Header Container / End -->

<!-- SEARCH -->
<div class="container-fluid">

    <div class="row">
    	<?include 'include/search_global.inc.php'; ?>
    </div>

</div>
<!-- END SEARCH -->


<!-- Content
================================================== -->
<div class="row">

	<div class="col-sm-12">

    <div class="row">

      <div class="col-md-6 col-md-push-6">
		<div class="style-2">
    		<ul class="tabs-nav">
    			<li class="active"><a href="#search_map"><i class="sl sl-icon-location"></i> Mappa</a></li>
    			<?
    			//FIRENZE || PRATO
    			if($req['area_type_and_id'][0]['ISTAT']=='048017' || $req['area_type_and_id'][0]['ISTAT']=='100005'){
    			?>
    			<li><a href="#search_mapsection"><i class="sl sl-icon-map"></i> Zona</a></li>
    			<?
    			}
    			?>
    		</ul>
    		<div class="tabs-container">
    			<div class="tab-content" id="search_map" style="height:80vh">
    				<div id="map-container">
            		    <div id="map" data-map-zoom="4" data-map-scroll="true">
            		        <!-- map goes here -->
            		    </div>

            		    <!-- Map Navigation -->
            			<a href="#" id="geoLocation" title="Your location"></a>
            			<ul id="mapnav-buttons" class="top">
            			    <li><a href="#" id="prevpoint" title="Previous point on map">Prev</a></li>
            			    <li><a href="#" id="nextpoint" title="Next point on mp">Next</a></li>
            			</ul>
            		</div>
    			</div>
    			<div class="tab-content" id="search_mapsection">

    				<div class="row">
    				
    				<?if($req['area_type_and_id'][0]['ISTAT']=='048017') {
    				
    				    //filters[area_type_and_id][0][area_id]
    				    
    				    $centro = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=707,713,737,740,741,11496,742";
    				    $oltrarno = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=695,713,722,725,727,732,738,11500";
    				    $PortaAlPrato = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=717,11502,731,747,751,710,739,700";
    				    $CampodiMarte = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=694,698,696,706,710,718,721,734,735,743,750,693";
    				    $FirenzeNord = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=709,715,720,724,729,739,697,723,726,733,699,701";
    				    $UgnanoMantignano = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=749,703,719,11498,702";
    				    $Isolotto = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=692,714,748,745,728";
    				    $LegnaiaSoffiano = "filters[area_type_and_id][0][area_type]=block&filters[area_type_and_id][0][area_id][in]=716,745,702,748";
    				    ?>
    					<div class="col-sm-12">
    						<!--  SEZIONE MAPPA FIRENZE -->
    						
            				<h4>Firenze - Provincia o comune</h4>
            				<div id="container-mapchart-firenze" style="display: block;">
            					<div id="box-container-area-firenze" style="width: 360px; height: 360px; background-image: url(&quot;imgs/maps-sections/FI/firenze.png&quot;);">
                                  <div id="box-selected-area-firenze"></div>
                                  <div id="box-area-firenze" style="background-image: url(&quot;imgs/maps-sections/transparent.gif&quot;);">
                                    <img src="imgs/maps-sections/transparent.gif" width="360px" height="360px" usemap="#Map-firenze">
                                    <map name="Map" id="Map-firenze">
                                    	<area data-adminlevel="4" data-next="0" title="Centro" href="?<?=$centro?>" shape="poly" coords="229,212,229,217,228,221,206,216,200,214,196,213,190,210,185,207,174,201,161,195,167,188,172,186,176,177,181,171,188,172,193,173,195,177,209,172,214,175,222,186,229,197,229,201,229,205,229,212" data-imageover="imgs/maps-sections/FI/map_zona_196.png" data-maparea="196" data-idarea="196" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Oltrarno" href="?<?=$oltrarno?>" shape="poly" coords="169,213,161,208,159,203,159,198,151,199,151,194,152,191,161,195,174,201,185,207,190,210,196,213,200,214,206,216,228,221,231,227,227,225,223,223,217,223,213,223,204,222,199,220,197,216,187,223,179,225,176,230,173,216,169,213" data-imageover="imgs/maps-sections/FI/map_zona_197.png" data-maparea="197" data-idarea="197" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Leopoldo, Porta al Prato" href="?<?=$PortaAlPrato?>" shape="poly" coords="173,149,183,149,181,143,184,139,198,140,203,140,201,144,199,149,199,156,211,166,193,173,188,172,181,171,176,177,172,186,167,188,161,195,152,191,131,182,126,180,118,176,96,160,90,156,99,153,109,155,119,156,126,158,132,164,141,170,147,165,152,158,159,154,160,146,168,142,170,141,173,149" data-imageover="imgs/maps-sections/FI/map_zona_202.png" data-maparea="202" data-idarea="202" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Campo di Marte, Libertà" href="?<?=$CampodiMarte?>" shape="poly" coords="250,204,237,221,228,221,229,217,229,212,229,205,229,201,229,197,222,186,214,175,209,172,195,177,193,173,211,166,221,163,225,169,233,169,239,170,247,166,254,163,258,165,263,168,267,169,272,168,272,173,266,184,259,195,250,204" data-imageover="imgs/maps-sections/FI/map_zona_199.png" data-maparea="199" data-idarea="199" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Firenze Nord" href="?<?=$FirenzeNord?>" shape="poly" coords="160,146,159,154,152,158,147,165,141,170,132,164,126,158,119,156,109,155,99,153,90,156,80,155,75,155,70,156,63,157,49,158,38,160,11,167,6,166,6,162,8,157,11,152,12,148,14,144,16,140,16,136,11,133,13,129,16,125,22,124,28,125,32,125,36,126,41,126,45,127,50,128,55,129,60,130,64,130,66,126,68,122,72,119,75,114,78,110,81,105,83,101,86,97,90,99,94,102,98,103,102,106,106,109,109,104,113,99,116,95,119,91,121,87,124,83,128,80,149,99,154,109,170,141,168,142,160,146" data-imageover="imgs/maps-sections/FI/map_zona_203.png" data-maparea="203" data-idarea="203" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Ugnano - Mantignano" href="?<?=$UgnanoMantignano?>" shape="poly" coords="52,215,48,216,44,214,40,210,36,209,32,208,28,206,24,203,20,200,16,197,12,195,8,190,10,186,11,181,7,178,9,174,12,169,11,167,38,160,49,158,63,157,70,156,75,155,80,155,90,156,84,166,76,171,74,176,71,181,70,185,72,189,72,196,74,205,77,212,68,218,58,220,53,216,52,215" data-imageover="imgs/maps-sections/FI/map_zona_204.png" data-maparea="204" data-idarea="204" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="L'Isolotto" href="?<?=$Isolotto?>" shape="poly" coords="118,176,126,180,131,182,152,191,151,194,151,199,142,201,132,203,126,205,118,206,121,199,107,201,94,201,81,204,74,205,72,196,72,189,70,185,71,181,74,176,76,171,84,166,90,156,96,160,118,176" data-imageover="imgs/maps-sections/FI/map_zona_205.png" data-maparea="205" data-idarea="205" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Legnaia, Soffiano" href="?<?=$LegnaiaSoffiano ?>" shape="poly" coords="159,203,161,208,159,213,157,217,152,216,147,215,140,209,135,217,129,225,124,234,118,232,109,229,106,233,102,235,97,233,95,229,91,226,87,223,83,222,81,218,78,214,74,216,70,218,66,221,62,222,58,220,68,218,77,212,74,205,81,204,94,201,107,201,121,199,118,206,126,205,132,203,142,201,151,199,159,198,159,203" data-imageover="imgs/maps-sections/FI/map_zona_206.png" data-maparea="206" data-idarea="206" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Bellosguardo - Galluzzo" href="#" shape="poly" coords="122,276,117,273,113,271,111,266,111,262,110,258,107,254,103,253,99,250,98,245,95,241,99,237,102,235,106,233,109,229,118,232,124,234,129,225,135,217,140,209,147,215,152,216,157,217,159,213,163,215,167,221,167,225,167,230,162,233,161,239,166,238,171,236,175,234,178,241,183,253,186,261,180,262,173,264,171,268,176,271,181,276,176,285,171,285,169,296,176,303,178,308,178,312,177,316,179,321,175,319,171,316,167,315,163,313,160,309,156,309,152,310,147,309,143,309,141,314,139,318,134,317,129,318,125,319,121,319,118,314,116,310,113,306,109,306,109,301,109,297,106,293,104,288,104,284,105,280,109,279,113,280,117,279,121,277,122,276" data-imageover="imgs/maps-sections/FI/map_zona_207.png" data-maparea="207" data-idarea="207" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Firenze Sud" href="#" shape="poly" coords="247,276,243,277,239,280,235,281,231,281,227,281,223,282,221,287,218,291,214,294,210,298,206,297,202,298,198,301,194,305,190,308,186,311,186,316,182,319,179,321,177,316,178,312,178,308,176,303,169,296,171,285,176,285,181,276,176,271,171,268,173,264,180,262,186,261,202,260,204,266,213,275,226,271,232,268,238,255,230,238,231,227,228,221,237,221,249,221,269,217,275,218,284,222,295,225,305,227,319,225,322,229,319,234,315,236,311,234,307,238,303,242,299,241,295,242,295,246,294,250,293,254,294,258,295,262,295,266,294,270,292,274,291,278,288,282,289,286,293,286,288,288,284,288,280,287,275,286,271,284,266,286,262,282,257,280,253,278,249,276,247,276" data-imageover="imgs/maps-sections/FI/map_zona_208.png" data-maparea="208" data-idarea="208" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Coverciano, Bellariva" href="#" shape="poly" coords="278,211,275,218,269,217,249,221,237,221,250,204,259,195,266,184,272,173,272,168,276,168,277,172,279,176,283,178,287,176,292,175,289,185,281,202,278,206,278,211" data-imageover="imgs/maps-sections/FI/map_zona_200.png" data-maparea="200" data-idarea="200" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Bolognese, Le Cure" href="#" shape="poly" coords="244,122,248,124,253,126,257,130,260,134,264,139,268,143,271,147,272,153,273,158,274,163,272,168,267,169,263,168,258,165,254,163,247,166,239,170,233,169,225,169,221,163,211,166,199,156,199,149,201,144,203,140,211,135,217,132,211,122,211,108,200,79,207,79,208,75,218,70,224,66,228,60,233,61,237,61,240,57,240,52,242,48,245,44,249,43,253,42,257,40,257,44,259,48,263,46,268,49,273,53,277,57,280,61,281,66,278,70,275,75,271,78,270,82,268,87,264,91,261,95,257,97,254,102,251,107,247,109,245,113,245,117,244,121,244,122" data-imageover="imgs/maps-sections/FI/map_zona_210.png" data-maparea="210" data-idarea="210" style="cursor: pointer; outline: none;"><area data-adminlevel="4" data-next="0" title="Serpiolle, Careggi" href="#" shape="poly" coords="150,69,153,64,156,60,160,56,163,60,165,64,169,68,174,66,176,62,178,58,179,54,180,50,184,51,184,47,188,44,192,40,196,40,200,41,204,42,208,42,211,46,215,49,218,53,220,57,224,56,225,60,228,60,224,66,218,70,208,75,207,79,200,79,211,108,211,122,217,132,211,135,203,140,198,140,184,139,181,143,183,149,173,149,170,141,154,109,149,99,131,78,135,76,140,74,144,73,148,72,150,69" data-imageover="imgs/maps-sections/FI/map_zona_211.png" data-maparea="211" data-idarea="211" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Settignano, Rovezzano" href="#" shape="poly" coords="289,185,295,174,299,171,302,167,304,161,306,156,309,152,314,153,318,153,322,153,324,149,327,145,328,141,333,141,337,144,339,148,343,153,345,158,348,162,352,165,355,169,352,173,347,176,346,180,351,181,351,186,353,190,352,194,347,196,346,200,343,204,341,208,336,211,332,214,328,218,324,222,320,224,319,225,305,227,295,225,284,222,275,218,278,211,278,206,281,202,289,185" data-imageover="imgs/maps-sections/FI/map_zona_10330.png" data-maparea="10330" data-idarea="10330" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Michelangelo, Porta Romana" href="#" shape="poly" coords="175,234,171,236,166,238,161,239,162,233,167,230,167,225,167,221,163,215,159,213,161,208,169,213,173,216,176,230,179,225,187,223,197,216,199,220,204,222,213,223,217,223,223,223,227,225,231,227,230,238,238,255,232,268,226,271,213,275,204,266,202,260,186,261,183,253,178,241,175,234" data-imageover="imgs/maps-sections/FI/map_zona_10331.png" data-maparea="10331" data-idarea="10331" style="cursor: pointer; outline: none;">
                                   </map>
                                 </div>
                                 <div id="box-hover-area-firenze" style="background-image: url(&quot;imgs/maps-sections/FI/map_zona_203.png&quot;); opacity: 0;">
                                    <img src="imgs/maps-sections/transparent.gif" width="360px" height="360px">
                                 </div>
                              </div>
                           </div>
                           <!-- FINE SEZIONE MAPPA FIRENZE -->
    					</div>
						<?} ?>
    					
    					<?if($req['area_type_and_id'][0]['ISTAT']=='100005') {?>
    					<div class="col-sm-12">
    						  <!--  SEZIONE MAPPA PRATO -->
                			  <h4>Prato - Provincia o comune</h4>
                              <div id="container-mapchart-prato" style="display: block;">
                              	<div id="box-container-area-prato" style="width: 360px; height: 360px; background-image: url(&quot;imgs/maps-sections/PO/prato.png&quot;);">
                                  <div id="box-selected-area-prato"></div>
                                  <div id="box-area-prato" style="background-image: url(&quot;imgs/maps-sections/transparent.gif&quot;);">
                                    <img src="imgs/maps-sections/transparent.gif" width="360px" height="360px" usemap="#Map-prato">
                                    <map name="Map" id="Map-prato">
                                    	<area data-adminlevel="4" data-next="0" title="Centro" href="#" shape="poly" coords="201,192,201,194,199,198,196,198,190,199,186,200,180,194,177,191,174,188,177,186,181,179,186,172,192,174,197,176,199,183,204,188,201,192" data-imageover="imgs/maps-sections/PO/map_zona_10115.png" data-maparea="10115" data-idarea="10115" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Pietà, Stazione, Stadio" href="#" shape="poly" coords="201,194,201,192,204,188,199,183,197,176,204,159,207,165,213,177,221,172,224,172,227,175,232,180,234,187,236,195,235,203,231,206,225,206,217,204,213,200,210,195,206,195,201,194" data-imageover="imgs/maps-sections/PO/map_zona_10116.png" data-maparea="10116" data-idarea="10116" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Chiesanuova, Sacrocuore, Piazza del Mercato Nuovo" href="#" shape="poly" coords="172,166,159,159,146,156,150,150,174,132,182,138,177,147,190,150,195,153,204,159,197,176,192,174,186,172,182,171,175,167,172,166" data-imageover="imgs/maps-sections/PO/map_zona_10117.png" data-maparea="10117" data-idarea="10117" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="San Paolo, Galcianese, Pistoiese" href="#" shape="poly" coords="177,186,174,188,168,199,163,205,153,199,140,194,127,190,132,179,142,161,146,156,159,159,172,166,175,167,182,171,186,172,181,179,177,186" data-imageover="imgs/maps-sections/PO/map_zona_10118.png" data-maparea="10118" data-idarea="10118" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="San Giusto, Vergaio, Tobbiana" href="#" shape="poly" coords="117,230,103,222,109,213,99,207,91,201,85,198,74,193,69,189,84,177,90,179,95,180,106,183,113,186,116,187,127,190,140,194,153,199,163,205,160,210,164,212,161,218,158,222,156,226,151,234,146,231,144,235,138,242,117,230" data-imageover="imgs/maps-sections/PO/map_zona_10119.png" data-maparea="10119" data-idarea="10119" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Cafaggio, Grignano, Le Badie" href="#" shape="poly" coords="189,223,208,236,212,239,220,245,228,251,229,254,230,256,224,255,219,254,208,254,185,253,173,262,170,268,143,255,134,249,138,242,144,235,146,231,151,234,156,226,158,222,161,218,164,212,160,210,163,205,189,223" data-imageover="imgs/maps-sections/PO/map_zona_10120.png" data-maparea="10120" data-idarea="10120" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Mezzana, Zarini, Soccorso" href="#" shape="poly" coords="229,207,231,206,235,209,250,221,267,234,256,250,253,249,250,248,247,246,244,247,243,251,246,252,246,256,245,260,243,263,242,264,233,257,230,256,229,254,228,251,220,245,212,239,208,236,189,223,163,205,168,199,174,188,177,191,180,194,186,200,190,199,196,198,199,198,201,194,206,195,210,195,213,200,217,204,225,206,229,207" data-imageover="imgs/maps-sections/PO/map_zona_10121.png" data-maparea="10121" data-idarea="10121" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="La Macine, La Querce" href="#" shape="poly" coords="280,219,288,213,288,216,289,219,291,222,290,225,289,228,287,231,285,234,285,237,284,241,282,244,279,244,276,243,275,246,273,249,270,250,269,253,268,256,265,260,262,257,259,255,257,252,256,250,267,234,250,221,235,209,231,206,235,203,236,195,280,219" data-imageover="imgs/maps-sections/PO/map_zona_10122.png" data-maparea="10122" data-idarea="10122" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Figline, Villa Fiorita, Galceti" href="#" shape="poly" coords="204,102,205,115,207,123,205,126,201,128,197,129,190,129,186,128,183,128,179,128,176,126,171,123,167,119,164,115,163,112,160,112,155,109,151,107,144,106,138,102,141,98,145,95,148,93,151,90,152,87,153,84,152,81,154,78,156,74,159,71,158,68,158,65,159,61,161,56,162,52,162,48,162,43,161,40,161,37,163,33,165,30,167,26,170,22,173,19,176,18,179,14,182,10,184,7,187,5,190,4,193,4,196,4,199,5,202,7,205,9,208,10,211,11,214,13,216,16,218,19,221,22,222,26,222,31,222,36,223,39,225,42,226,45,228,48,230,51,230,54,231,58,233,61,233,64,232,67,236,68,240,68,244,70,244,74,241,76,242,79,245,80,248,80,251,82,250,85,245,94,245,97,242,100,238,102,236,105,233,106,231,108,204,102" data-imageover="imgs/maps-sections/PO/map_zona_10123.png" data-maparea="10123" data-idarea="10123" style="cursor: pointer; outline: none;"><area data-adminlevel="4" data-next="0" title="Maliseti, Narnali, Viaccia" href="#" shape="poly" coords="109,123,112,121,115,120,117,117,120,115,123,112,126,114,129,115,132,114,135,111,135,107,136,104,138,102,144,106,151,107,155,109,160,112,163,112,164,115,167,119,171,123,176,126,179,128,174,132,150,150,146,156,136,154,130,152,123,156,118,159,112,158,109,152,105,150,99,144,62,132,63,128,68,130,71,131,74,131,76,128,79,126,83,128,86,124,89,126,92,126,96,126,100,124,103,123,106,123,109,123" data-imageover="imgs/maps-sections/PO/map_zona_10124.png" data-maparea="10124" data-idarea="10124" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Galciana, Sant'Ippolito" href="#" shape="poly" coords="111,157,112,158,118,159,123,156,130,152,136,154,146,156,142,161,132,179,127,190,116,187,113,186,106,183,95,180,90,179,84,177,69,189,57,173,48,166,44,164,45,161,46,157,47,154,50,149,52,146,54,142,58,137,61,134,62,132,99,144,105,150,109,152,111,157" data-imageover="imgs/maps-sections/PO/map_zona_10125.png" data-maparea="10125" data-idarea="10125" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Iolo, Casale" href="#" shape="poly" coords="103,222,117,230,100,260,73,304,69,300,67,297,64,297,65,293,62,290,59,288,56,285,53,282,52,279,52,276,51,273,51,270,51,267,52,264,53,260,54,256,54,252,54,249,52,246,50,241,48,238,45,238,43,235,41,232,39,229,39,225,38,221,35,219,31,216,31,213,31,210,32,207,32,204,33,201,33,198,34,195,34,192,34,188,35,184,36,180,38,176,40,173,42,170,43,167,44,164,48,166,57,173,69,189,85,198,91,201,99,207,109,213,103,222" data-imageover="imgs/maps-sections/PO/map_zona_10126.png" data-maparea="10126" data-idarea="10126" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Tavola, Le Fontanelle, Paperino, Castelnuovo" href="#" shape="poly" coords="230,256,233,257,242,264,239,267,238,270,236,274,235,277,232,280,231,283,228,287,226,291,223,295,221,299,219,302,217,305,215,308,212,311,210,315,208,318,206,321,205,324,205,327,205,330,202,334,199,336,196,338,193,340,190,344,188,347,187,350,186,353,183,352,180,350,177,348,173,347,171,350,167,353,162,351,159,349,158,346,159,343,156,339,153,338,150,337,147,336,143,335,140,334,137,337,134,342,130,347,128,350,126,353,125,356,122,353,119,351,116,349,113,346,110,344,106,341,103,338,100,335,97,334,93,333,91,330,89,327,88,323,86,320,83,319,81,316,79,312,79,309,76,308,73,306,73,304,100,260,117,230,138,242,134,249,143,255,170,268,173,262,185,253,208,254,219,254,224,255,230,256" data-imageover="imgs/maps-sections/PO/map_zona_10128.png" data-maparea="10128" data-idarea="10128" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Castellina, Carteano" href="#" shape="poly" coords="218,140,224,131,231,123,231,108,233,106,236,105,238,102,242,100,245,97,245,94,250,85,252,90,252,94,255,96,259,95,262,95,265,94,268,93,272,91,275,89,279,86,282,82,286,79,289,77,291,73,294,70,298,70,301,68,304,67,307,65,310,64,315,62,319,59,321,63,322,66,324,71,326,75,328,79,330,82,330,86,328,89,324,90,320,89,316,89,313,89,310,89,307,90,304,92,301,96,298,98,295,100,293,103,289,106,286,108,284,111,281,113,278,116,281,119,284,121,287,125,289,128,287,132,286,135,284,138,282,142,280,146,279,149,277,152,273,156,271,159,267,164,266,167,265,171,264,174,265,177,269,179,271,182,273,186,275,190,279,193,282,196,283,199,279,203,281,206,285,210,288,213,280,219,236,195,234,187,232,180,227,175,224,172,221,172,213,177,207,165,204,159,211,145,218,140" data-imageover="imgs/maps-sections/PO/map_zona_10328.png" data-maparea="10328" data-idarea="10328" style="cursor: pointer; outline: none;">
                                    	<area data-adminlevel="4" data-next="0" title="Coiano, Santa Lucia, San Martino" href="#" shape="poly" coords="211,145,204,159,195,153,190,150,177,147,182,138,174,132,179,128,183,128,186,128,190,129,197,129,201,128,205,126,207,123,205,115,204,102,231,108,231,123,224,131,218,140,211,145" data-imageover="imgs/maps-sections/PO/map_zona_10329.png" data-maparea="10329" data-idarea="10329" style="cursor: pointer; outline: none;">
                                    </map>
                                  </div>
                                  <div id="box-hover-area-prato" style="background-image: url(&quot;imgs/maps-sections/PO/map_zona_10123.png&quot;); opacity: 0;">
                                    <img src="imgs/maps-sections/transparent.gif" width="360px" height="360px">
                                  </div>
                                </div>
                             </div>
                              <!-- FINE SEZIONE MAPPA PRATO -->
    					</div>
    					<?} ?>
    				</div>





    			</div>
    		</div>
		</div>

  </div>


    <div class="col-md-6 col-md-pull-6">
		
		<div style="height:50px;"></div>
		
      <!-- Sorting / Layout Switcher -->
      <div class="row fs-switcher d-none">

        <div class="col-md-6">
          <!-- Showing Results -->
          <p class="showing-results">14 Results Found </p>
        </div>

        <div class="col-md-6">
          <!-- Layout Switcher -->
          <div class="layout-switcher d-none">
            <a href="#" class="list"><i class="fa fa-th-list"></i></a>
            <a href="#" class="grid"><i class="fa fa-th-large"></i></a>
          </div>
        </div>
      </div>
      <!-- Listings -->
      <div class="listings-container list-layout row fs-listings">
        <div id="estatesList" style="width:100%;height: 80vh; overflow-y:scroll;">
          <?include 'include/estates_list.inc.php'; ?>
        </div>
      </div>
      <!-- Listings Container / End -->

      <!-- Pagination Container -->
      <div class="row fs-listings d-none">
        <div class="col-md-12">

          <!-- Pagination -->
          <div class="clearfix"></div>
          <div class="pagination-container margin-top-10 margin-bottom-45">
            <nav class="pagination">
              <ul>
                <li><a href="#" class="current-page">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li class="blank">...</li>
                <li><a href="#">22</a></li>
              </ul>
            </nav>

            <nav class="pagination-next-prev">
              <ul>
                <li><a href="#" class="prev">Previous</a></li>
                <li><a href="#" class="next">Next</a></li>
              </ul>
            </nav>
          </div>

        </div>
      </div>
      <!-- Pagination Container / End -->


      </div>
    </div>

  </div>

</div>


<div class="clearfix"></div>

<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>

<!--  ITALIANA IMMOBILIARE  -->
<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/qs/6.5.2/qs.js"></script>
<script type="text/javascript" src="scripts/search_result.js"></script>


<!-- Maps
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>-->
<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>





</div>
<!-- Wrapper / End -->



</body>
</html>
