function fillInAddress() {
    var e = autocomplete.getPlace();
    console.log(e.geometry), $("#place_lat").val(e.geometry.location.lat()), $("#place_lng").val(e.geometry.location.lng()), $("#place_perfect_match").val(1)
}

function initialize_autocomplete() {
    autocomplete = new google.maps.places.Autocomplete(document.getElementById("google-maps-autocomplete"), {
        types: ["(cities)"],
         componentRestrictions: {country: "it"}
    }), google.maps.event.addListener(autocomplete, "place_changed", function() {
        fillInAddress()
    })
}

function fillInAddressAdvanced() {
    var e = autocomplete_advanced_search.getPlace();
    console.log(e.geometry), $("#place_lat_advanced").val(e.geometry.location.lat()), $("#place_lng_advanced").val(e.geometry.location.lng()), $("#place_perfect_match_advanced").val(1)
}

function initialize_autocomplete_advanced() {
    autocomplete_advanced_search = new google.maps.places.Autocomplete(document.getElementById("google-maps-autocomplete-advanced"), {
        types: ["(cities)"],
         componentRestrictions: {country: "it"}
    }), google.maps.event.addListener(autocomplete_advanced_search, "place_changed", function() {
        fillInAddressAdvanced()
    })
}
var autocomplete, autocomplete_advanced_search;


function initialize_autocomplete_address() {
    autocomplete = new google.maps.places.Autocomplete(document.getElementById("google-maps-autocomplete-address"), {
        types: ["geocode"],
         componentRestrictions: {country: "it"}
    }), google.maps.event.addListener(autocomplete, "place_changed", function() {
        fillInAddress()
    })
}
