function remove_favourite(item, estate_id, user_id) {
    toggle_favourite(item, estate_id, user_id);
    var list_item = $(item).closest('.estate-list-item');
    $(list_item).fadeOut(400, function() {
        $(this).remove()
    })
}

(function($) {
    $.fn.ajax_form = function(config) {
        config.on_success = (typeof (config.on_success) != 'undefined') ? config.on_success : function() {
            location.reload()
        }
        this.each(function() {
            var form = $(this);

            form.submit(function(e) {
                e.preventDefault();
                form.find('.alert-danger').hide();
                form.find('.alert-success').hide();
                var form_data = form.serialize();
                $.post(form.attr('action'), form_data, function(risp) {
                    console.log(risp);
                    switch (risp.status) {
                        case 1:
                            config.on_success(risp, form);
                            break;
                        case 0:
                            config.on_fail(risp, form);
                            break;
                        default:
                            console.log(risp);
                            break;
                    }
                }, 'json')
            })
        })
    }
})(jQuery);

function set_cookie(cookie_name, cookie_value) {
    $.post('/op.execute.php', {op: 'set_cookie', 'cookie_name': cookie_name, 'cookie_value': cookie_value, }, function(risp) {
    })
}

$(document).ready(function() {
    $('#cookies_alert').on('closed.bs.alert', function() {
        set_cookie('cookies_ok','1')
    })

    $('.btn-dismiss').click(function() {
        var dismissing = $(this).attr('data-dismiss');
        $(this).closest(dismissing).fadeOut(400, function() {
            $(this).remove()
        })
    })

    $('.ajax-form').ajax_form({
        on_success: function(risp, form) {
            var success_wrapper = form.find('.alert-success');
            success_wrapper.show();
            success_wrapper.find('.message-value').html(risp.content);
            if (form.hasClass('send-request-form')) {
                ga('send', 'event', 'button', 'click', 'richiesta');
            }
        },
        on_fail: function(risp, form) {
            var alert_wrapper = form.find('.alert-danger');
            alert_wrapper.show();
            alert_wrapper.find('.message-value').html(risp.content);
        }
    })

    $('.only-numbers').only_numbers();

    $('.intertial-scroll').click(function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var offset_scroll = $(href).offset().top;
        $("html, body").animate({scrollTop: offset_scroll - 130}, 500);
        var li = $(this).closest('li');
        li.siblings().removeClass('active');
        li.addClass('active');
    })

    $('.with-tooltip').tooltip();

})

function toggle_favourite(item, estate_id, user_id) {
    $(item).toggleClass('active');
    var data = {
        estate_id: estate_id,
        user_id: user_id,
        op: 'toggle_favourite'
    }
    $.post('/op.execute.php', data, function(risp) {

    })
}

jQuery.fn.only_numbers =
        function()
        {
            console.log('Solo Numeri Attivo');
            return this.each(function()
            {
                $(this).keydown(function(e)
                {
                    if (e.shiftKey == true) {
                        e.preventDefault();
                        return false;
                    }

                    var key = e.charCode || e.keyCode || 0;
                    // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
                    // home, end, period, and numpad decimal
                    return (
                            key == 8 ||
                            key == 9 ||
                            key == 46 ||
                            key == 110 ||
                            key == 190 ||
                            (key >= 35 && key <= 40) ||
                            (key >= 48 && key <= 57) ||
                            (key >= 96 && key <= 105));

                });
            });
        };
