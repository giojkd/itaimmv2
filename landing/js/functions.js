function set_session_ii(cfg) {
    var data = {
        op: 'set_session',
        values: cfg
    }

    $.post('/op.execute.php', data)
}

function remove_favourite(e, t, o) {
    toggle_favourite(e, t, o);
    var n = $(e).closest(".estate-list-item");
    $(n).fadeOut(400, function() {
        $(this).remove()
    })
}

function scrollToElement(cfg) {
    console.log(cfg);
    var divPosition = $(cfg.element).offset().top;
    cfg.offset = (typeof cfg.offset != 'undefined') ? cfg.offset : 0;
    cfg.speed = (typeof cfg.speed != 'undefined') ? cfg.speed : 'slow';
    $('html,body').animate({
        scrollTop: divPosition + (cfg.offset)},
    cfg.speed);
}

function cookies_alert_off() {
    $.post("/op.execute.php?op=cookies_alert_off", {
        op: "cookies_alert_off"
    }, function(e) {
    })
}

function set_cookie(e, t) {
    $.post("/op.execute.php", {
        op: "set_cookie",
        cookie_name: e,
        cookie_value: t
    }, function(e) {
    })
}

function toggle_favourite(e, t, o) {
    $(e).toggleClass("active");
    var n = {
        estate_id: t,
        user_id: o,
        op: "toggle_favourite"
    };
    $.post("/op.execute.php", n, function(e) {
    })
}
!function(e) {
    e.fn.ajax_form = function(t) {
        t.on_success = "undefined" != typeof t.on_success ? t.on_success : function() {
            location.reload()
        }, this.each(function() {
            var o = e(this);
            o.submit(function(n) {
                n.preventDefault(), o.find(".alert-danger").hide(), o.find(".alert-success").hide();
                var s = o.serialize();
                e.post(o.attr("action"), s, function(e) {
                    switch (console.log(e), e.status) {
                        case 1:
                            t.on_success(e, o);
                            break;
                        case 0:
                            t.on_fail(e, o);
                            break;
                        default:
                            console.log(e)
                    }
                }, "json")
            })
        })
    }
}(jQuery), $(document).ready(function() {
	
	
    $("#cookies_alert").on("closed.bs.alert", function() {
        set_cookie("cookies_ok", "1")
    }), $(".btn-dismiss").click(function() {
        var e = $(this).attr("data-dismiss");
        $(this).closest(e).fadeOut(400, function() {
            $(this).remove()
        })
    }), $(".ajax-form").ajax_form({
        on_success: function(e, t) {
            var o = t.find(".alert-success");
            o.show(), o.find(".message-value").html(e.content), t.hasClass("send-request-form") && ga("send", "event", "button", "click", "richiesta")
        },
        on_fail: function(e, t) {
            var o = t.find(".alert-danger");
            o.show(), o.find(".message-value").html(e.content)
        }
    }), $(".only-numbers").only_numbers(), $(".intertial-scroll").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("href"),
                o = $(t).offset().top;
        $("html, body").animate({
            scrollTop: o - 130
        }, 500);
        var n = $(this).closest("li");
        n.siblings().removeClass("active"), n.addClass("active")
    }), $(".with-tooltip").tooltip()
}), jQuery.fn.only_numbers = function() {
    return console.log("Solo Numeri Attivo"), this.each(function() {
        $(this).keydown(function(e) {
            if (1 == e.shiftKey)
                return e.preventDefault(), !1;
            var t = e.charCode || e.keyCode || 0;
            return 8 == t || 9 == t || 46 == t || 110 == t || 190 == t || t >= 35 && 40 >= t || t >= 48 && 57 >= t || t >= 96 && 105 >= t
        })
    })
};