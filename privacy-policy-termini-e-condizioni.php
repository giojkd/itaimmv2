<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Privacy Policy - Termini e condizioni | Italiana Immobiliare</title>
<meta name="description" content="Informazioni, ai sensi dell’artt. 12, 13, 14 del Regolamento Privacy Europeo N. 2016/679"/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>
<!-- Banner
================================================== -->


<div class="container">
	<div class="row">
		<div class="col-md-12 contenuto">
			
            
            <h1>INFORMATIVA SUL TRATTAMENTO DEI DATI PERSONALI</h1>
            
            <h2>(Informazioni, ai sensi dell’artt. 12, 13, 14 del Regolamento Privacy Europeo N. 2016/679)</h2>
            
            <h3>TRATTAMENTO A CUI FA RIFERIMENTO L'INFORMATIVA:</h3>
            <h4>Gestione clienti - Intermediazione immobiliare</h4>
            <h5>Soggetti Interessati: Clienti</h5>
            
            <p>Ai sensi dell’art. 13 del Regolamento (UE) 2016/679 (“regolamento generale sulla protezione dei dati”, di seguito “Regolamento”) con la presente Italiana Immobiliare Spa La informa che la citata normativa prevede la tutela delle persone e di altri soggetti rispetto al trattamento dei dati personali e che tale trattamento sarà improntato ai principi di correttezza, liceità, trasparenza e di tutela della Sua riservatezza e dei Suoi diritti, in base all’art. 6, comma 1, lett. b) del Regolamento.<br>
            I Suoi dati personali verranno trattati in accordo alle disposizioni legislative della normativa sopra richiamata e degli obblighi di riservatezza ivi previsti.</p>
            
            <h3>Finalità di trattamento</h3>
            <p>L'azienda Italiana Immobiliare Spa, tramite il trattamento "Gestione clienti", tratta i sopraindicati dati per il servizio di intermediazione volto a favorire la conclusione di contratti di compravendita e locazione immobiliare. Il Data Processor e il Data Controller vigilano per garantire agli interessati che i dati saranno trattati solo per la finalità dichiarata e solo per la parte strettamente necessaria al trattamento. Si impegnano inoltre, entro i limiti della ragionevolezza, a modificare e correggere tutti i dati che risultano nel frattempo diversi dagli originali, a tenerli sempre aggiornati e a cancellare tutti quei dati che risultano eccedenti al trattamento dichiarato.</p>
            
            <h3>Categorie destinatari</h3>
            <p>I suoi dati potranno essere comunicati a terzi, in particolare a: </p>
            <ul>
            <li>Consulenti e liberi professionisti, anche in forma associata; </li>
            <li>Banche e istituti di credito;</li>
            <li>Società affiliate al gruppo Italiana Immobiliare;</li>
            <li>Aziende che collaborano e forniscono servizi all’Italiana Immobiliare.</li>
            </ul>
            <p>Il Titolare rende noto, inoltre, che l'eventuale non comunicazione, o comunicazione errata, di una delle informazioni obbligatorie, può causare l'impossibilità del Titolare di garantire la congruità del trattamento stesso </p>
            
            <h3>Categorie di dati trattati:</h3>
            <p>Per le finalità su indicate i dati personali raccolti potranno essere i seguenti:</p>
            <ul>
            <li>Dati anagrafici;</li>
            <li>Dati bancari;</li>
            <li>Email e telefono.</li>
            </ul>
            
            <h3>Durata del trattamento:</h3>
            <p>L’azienda Italiana Immobiliare Spa dichiara che il trattamento è da considerarsi con data indefinita in quanto continuerà a tenerlo in vita per poter proseguire la propria attività. La Italiana Immobiliare Spa si impegna altresì a cancellare i dati una volta raggiunte le finalità della raccolta.</p>
            
            <h3>Modalità del trattamento</h3>
            <p>I suoi dati personali potranno essere trattati nei seguenti modi:</p>
            <ul>
            <li>Trattamento a mezzo di calcolatori elettronici;</li>
            <li>Trattamento manuale a mezzo di archivi cartacei.</li>
            </ul>
            
            <p>I dati personali potranno essere accessibili e trattati dai dipendenti e collaboratori del Titolare del trattamento nella loro qualità di responsabili o incaricati del trattamento, oltre da eventuali terzi, nominati responsabili esterni del trattamento, che supportano il Titolare del trattamento nella gestione dell’intermediazione immobiliare.</p>
            
            <h3>Articolo 8 (dati riguardanti i minori):</h3>
            <p>Nel trattamento "Gestione clienti" non vengono trattati dati di minori.</p>
            
            <h3>Articolo 9 (dati sanitari, biometrici e giudiziari):</h3>
            <p>Nel trattamento "Gestione clienti" non vengono trattati dati sanitari, biometrici e giudiziari.</p>
            
            <h3>Profilazione:</h3>
            <p>Il trattamento non riguarda processi automatizzati o di profilazione.</p>
            
            <h3>Trasferimento dei dati di questo trattamento:</h3>
            <p>I dati non vengono trasferiti in paesi extra UE</p>
            
            
            
            <h3>Diritti dell’interessato</h3>
            <p>Lei ha il diritto in qualunque momento di ottenere la conferma dell'esistenza o meno dei suoi dati e di conoscerne il contenuto e l'origine, verificarne l'esattezza o chiederne l'integrazione o l'aggiornamento, oppure la rettificazione. Lei ha il diritto altresì di chiedere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, nonché di opporsi in ogni caso, per motivi legittimi, al loro trattamento. Le richieste devono essere rivolte al Titolare del trattamento Italiana Immobiliare Spa presso la sede di via Ponte alle Mosse, 136/138 – 50141 Firenze o all’indirizzo email italiana@italianaimmobiliare.it.</p>
            
            <p>Lei ha diritto di formulare reclamo presso l’autorità garante della privacy se il titolare non risponde alle sue richieste.</p>
            <p>Il Regolamento GDPR UE 2016/679 riconosce i seguenti specifici diritti in capo all’interessato:</p>
            <ul>
            <li>Diritto di accesso (art. 15);</li> 
            <li>Diritto di rettifica (art. 16); </li> 
            <li>Diritto alla cancellazione (diritto all’oblio) (art. 17); </li> 
            <li>Diritto di limitazione di trattamento (art. 18);</li>  
            <li>Diritto di ricevere notifica in caso di rettificazione o cancellazione dei dati o limitazione del trattamento (art. 19); </li> 
            <li>Diritto alla portabilità dei dati (art. 20); </li> 
            <li>Diritto di opposizione (art. 21); </li> 
            <li>Diritto relativo al processo decisionale automatizzato, compresa la profilazione (art. 22).</li> 
            </ul>

            
            <div style="height:100px;"></div>
		</div>
	</div>
</div>


<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>



</div>
<!-- Wrapper / End -->

				

</body>
</html>