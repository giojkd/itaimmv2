<?
include $_SERVER['DOCUMENT_ROOT'] . '/include/include.inc.php';
#include ROOT . '/include/session_setter.inc.php';

$answer = $_GET['answer'];

switch ($answer) {
    case "amore":        
        
        $colore = "#a30e07";
        
        $meta_title = "Cosa cerchi nella vita? Amore!"; 
        
        $title = "<h1 class='text-center'>Alla domanda “cosa cerchi nella vita?”<br>
        Hai risposto “L’amore” allora...</h1>";
        
        $descr = "<p>...passione, spirito di squadra ed empatia sono le caratteristiche che ti contraddistinguono.</p>            
        <p>Sei sicuramente <strong class='fontBigger'>empatico</strong>, dai valore ai sentimenti e sai riconoscere e anticipare i bisogni degli altri e riesci a tirare fuori il meglio da loro.</p>";
        
        $landing_type = "landing_lavora-con-noi_amore";
        
        $icona ="landing_amore.png";
    
    break;
    
    case "realizzazione":
        
        $colore = "#4c8a32";
        
        $meta_title = "Cosa cerchi nella vita? Realizzazione!";
        
        $title = "<h1 class='text-center'>Alla domanda “cosa cerchi nella vita?”<br>
        Hai risposto “La Realizzazione” allora...</h1>";
        
        $descr = "<p>...spirito di inziativa, voglia di far carriera e tenacia sono le caratteristiche che ti contraddistinguono.<br>
        Sei sicuramente <strong class='fontBigger'>Leader</strong>, non ti accontenti, punti in alto e fai benissimo, le responsabilità e la crescita professionale sono le tue priorità.</p>";
        
        $landing_type = "landing_lavora-con-noi_realizzazione";
        
        $icona ="realizzazione.png";
        
        break;
        
    case "serenita":
        
        $colore = "#fdc21a";
        
        $meta_title = "Cosa cerchi nella vita? Serenità!";
        
        $title = "<h1 class='text-center'>Alla domanda “cosa cerchi nella vita?”<br>
        Hai risposto “La Serenità” allora...</h1>";
        
        $descr = "<p>...positività, fiducia in te stesso e grande equilibrio sono le caratteristiche che ti contraddistinguono.<br>
        Sei sicuramente <strong class='fontBigger'>ottimista</strong>, ti poni obiettivi e affronti la giornata con positività e fiducia nelle tue capacità, ogni sfida può essere gestita con il sorriso.</p>";
        
        $landing_type = "landing_lavora-con-noi_serenita";        
        
        $icona ="serenita.png";
        
        break;
    
    default:
        ;
    break;
}



?>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script type="text/javascript" src="/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/js/functions.js"></script>
        <link href="/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/include/font-awesome/css/font-awesome.min.css">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <? include ROOT . '/include/bootstrap.inc.php' ?>
        <title><?=$meta_title?></title>
        
        <style>
            body{
                font-size: 16px;
            }
            h1 {
                color:#ffffff;
                text-shadow: 2px 0px 5px #000000ac;
            }
            .testata{
                padding:20px; 
                background: <?=$colore?>; 
                border-radius: 30px 0 0;
                box-shadow: 5px 5px 10px #ccc;
            }
            
            .formbox{
                padding:20px 45px; 
                background: #dedede; 
                border-radius: 30px 0 0;
                box-shadow: 5px 5px 10px #ccc;
                font-size: 14px;
            }
            .fontBigger{
                font-size: 130%;
            }
            
            
        </style>
    </head>
    <body>
    	<? include ROOT . '/include/google_tag_manager.inc.php'; ?>
        <? include ROOT . '/navbars/navbarLanding.php' ?>
        <? height_spacer(60) ?>
        <div id="countdown"></div>
        <div class="container">
            
            <div class="testata text-center">
            	<?=$title ?>
            </div>
            <? height_spacer(60) ?>
                        
            <? height_spacer(10) ?>
            
            <div class="row">
            	<div class="col-md-10 col-md-offset-1">
                    <div class="row">
                    	<div class="col-lg-2 text-right">
                    		<img src='img/icone/<?=$icona ?>' alt="<?=$meta_title?> Italiana Immobiliare" />
                    	</div>
                    	<div class="col-lg-9">
                    		<?=$descr ?>
                    		<p>Sei la persona che stiamo cercando!</p>
                    	</div>
                    </div>
                </div>
            </div>
             <? height_spacer(60) ?>
            
             
            <div class="row">
            	<div class="col-md-10 col-md-offset-1">
                	<p><img src='img/logo_italiana-immobiliare.png' alt="Italiana Immobiliare" style="float: left; margin-right: 12px; margin-top: 2px;" /><br> investe tanto nella valorizzazione delle risorse umane e ti offre un’interessante opportunità di carriera grazie ad una <strong class='fontBigger'>formazione permanente e gratuita</strong>.<br>
                    Entrerai in contatto con un ambiente di lavoro dinamico e stimolante, ricco di occasioni e di incentivi.<br>
                    Il sostegno del Gruppo ti supporterà quotidianamente grazie alla garanzia di un metodo professionale consolidato da quasi 40 anni e ottimamente strutturato, basato su percorsi di formazione unici e qualificanti supportati da una
                    tecnologia all’avanguardia e da strumenti decisamente innovativi.</p>
                    <p>Entra nella nostra squadra, la passione nel lavoro è ad un passo da te!</p>  
            	</div>
            </div>
            
            
            
            
            <? height_spacer(60) ?>
            
             <div class="row">
             	<div class="col-md-10 col-md-offset-1">
             	
             	<div class="formbox">
             	<!--  -->
             		<form id="requestForm" class="form" method="POST" action="op.execute.php" enctype="multipart/form-data">
                        <input type="hidden" name="op" value="work_with_us_request_V2">
                        <input type='hidden' name='landing_type' value='<?=$landing_type?>'> 
                        <input type='hidden' name='curriculum_path' id='curriculum-path'>               
                       
                        <? height_spacer(30) ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Nome*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="Il tuo Nome" type="text" name="recruit_name" required="required">                        
                            </div>
                            
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Cognome*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="Il tuo Cognome" type="text" name="recruit_surname" required="required">                        
                            </div>
                        </div>
                        <? height_spacer(10) ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Comune*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="Il tuo Comune" type="text" name="recruit_comune" required="required">                        
                            </div>
                            
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Provincia*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="La tua Provincia" type="text" name="recruit_provincia" required="required">                        
                            </div>
                        </div>
                        <? height_spacer(10) ?>
                        
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Cellulare*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="Dove vuoi essere richiamato?" type="tel" name="recruit_telephone" required="required">
                                <?
                                if ($_GET['error'] == 'tel') {
                                    height_spacer(10);
                                    ?>
                                    <span class="label label-danger">Il numero di telefono è obbligatorio!</span>
                                    <?
                                }
                                ?>
                            </div>
                            
                            <div class="col-lg-2 col-md-3 col-xs-12">
                                <label>Email*</label>
                            </div>
                            <div class="col-lg-4 col-md-3 col-xs-12">
                                <input class="form-control" placeholder="La tua Email" type="email" name="recruit_email" required="required">
                            </div>
                            
                        </div>
                        <? height_spacer(10) ?>               
                       
                        <div class="row">
                            
                           <div class="col-lg-2 col-md-2 col-xs-12">
                                <label>Allega il tuo curriculum*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-xs-12">
                            	<input type="file" class="form-control" id="curriculum" name="recruit_cv" placeholder="File in PDF" required="required">
                                <div class="input-group-text" style="font-size:14px;">Caricare un file PDF <i class="fa fa-file-pdf-o"></i></div>
                            </div>
                            
                        </div>
                        <? height_spacer(10) ?>
                        
                        <div class="row">
                            <div class="col-xs-12">                            
                            	<div class="form-check">
                                	<input type="checkbox" class="form-check-input col-lg-1" id="privacy" name="privacy" required="required">
                                	<label  style="font-size:14px;" class="form-check-label col-lg-11" for="privacy">Compilando questo modulo autorizzo il trattamento e la gestione dei dati immessi secondo quanto stabilisce il nuovo Regolamento Europeo (GDPR n. 679/2016) in materia di privacy e protezione dati personali.<br>
                                	Per informazioni <a href="https://www.italianaimmobiliare.it/contenuti/Informativa_-_Selezione-del-personale-103.html" target="_blank">Consulta la nostra privacy policy</a> prima di inviare</label>
                              	</div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <img src='img/icone/italiana-immobiliare-lavora-con-noi.png' alt="Lavora con noi! Italiana Immobiliare" /> <button class="btn btn-success btn-lg btn-100">INVIA</button>    
                            </div>
                        </div>
                        
                        <div style="display:none" id="danger-message" class="row alert fade in alert-success">
                            <span class="message-value"></span>
                        </div>
                    </form>
             	
             	<!--  -->
             	</div>
             	
             	
             	</div>             
             </div>
             
            <? height_spacer(60) ?>
            
            
        </div>
        
        <div id="facebookLandingFooter">
            Italiana Immobiliare S.p.a. franchising immobiliare - PIVA: 03033690482 - Tutti i diritti riservati
        </div>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function() {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    
            fbq('init', '1741200202768671');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1741200202768671&ev=PageView&noscript=1"
                       /></noscript>
        <!-- End Facebook Pixel Code -->
    </body>
</html>