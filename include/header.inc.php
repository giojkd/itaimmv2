<!-- Header Container
================================================== -->
<header id="header-container" style="right: 0;left: 0;top: 0; width:100vw;">
	<!-- Header -->
	<div id="header">
		<div class="row" style="margin:0;">

			<!-- Left Side Content -->
			<div class="left-side" style="padding-left:20px;">
				<!-- Logo -->
				<div id="logo">
					<a href="index.php"><img src="imgs/italiana-immobiliare-spa_logo.png" alt="ITALIANA IMMOBILIARE SPA - Francising"></a>
				</div>
				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>


			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="header-right-part">
			    <!-- Main Navigation -->
				<nav id="navigation" class="style-1 pull-right" style="margin: 0;padding: 0;margin-right: 0;">
					<ul id="responsive">
						<li id="client_log_menu" style="line-height: 60px; padding:0 10px"></li>
						<li><a href="agencies_list.php"><span style="font-size:30px!important;" class="fas fa-user-tie fa-2x" v-b-tooltip.hover title="Agenzie"></span><span class="hidden-md hidden-lg" style="margin: 0;font-size: 20px;line-height: 30px;margin-left: 25px;">Agenzie</span></a></li>
						<li><a href="favorites_list.php"><span style="font-size:30px!important;" class="fa fa-heart fa-2x" v-b-tooltip.hover title="Preferiti"></span><span class="hidden-md hidden-lg" style="margin: 0;font-size: 20px;line-height: 30px;margin-left: 25px;">Preferiti</span></a></li>
						<li><a href="#"><span style="font-size:30px!important;" class="fa fa-bell fa-2x" v-b-tooltip.hover title="Novità"></span><span class="hidden-md hidden-lg" style="margin: 0;font-size: 20px;line-height: 30px;margin-left: 25px;">Novità</span></a></li>
						<li><a href="favorites_list.php"><span style="font-size:30px!important;" class="fa fa-sign-in fa-2x" v-b-tooltip.hover title="My Italiana Immobiliare"></span><span class="hidden-md hidden-lg" style="margin: 0;font-size: 20px;line-height: 30px;margin-left: 25px;">My Italiana Immobiliare</span></a></li>
						<li><a href="index.php"><span style="font-size:30px!important;" class="fa fa-home fa-2x" v-b-tooltip.hover title="Torna alla Home"></span><span class="hidden-md hidden-lg" style="margin: 0;font-size: 20px;line-height: 30px;margin-left: 25px;">Torna alla Home</span></a></li>
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->



			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body pb-0">
							<div class="row justify-content-center text-center" style="padding:10px;">
								  <h3>Con la nostra App la casa dei tuoi sogni è a portata di mano!</h3>
								</div>
							<div id="scarica-app" class="row">
							                <div class="col-xs-8 align-self-center justify-content-center text-center" style="padding-left: 35px">

							                        <div class="" style="height:20px"></div>
							                        <a href="https://play.google.com/store/apps/details?id=com.amtitalia.italianaimmobiliare" target="_blank" class="btn btn-default" style="font-size:20px;font-weight:bold;">
																				<img class="img-responsive" src="imgs/google_play.png" style="max-width: 100px;">
																			</a>
							                        <div class="" style="height:20px"></div>
							                        <a href="https://itunes.apple.com/it/app/italianaimmobiliare-2017/id1214355113?mt=8" target="_blank" class="btn btn-default" style="font-size:20px;font-weight:bold;">
																				<img class="img-responsive" src="imgs/apple_store.png" style="max-width: 100px;">
																			</a>
							                        <div class="" style="height:20px"></div>
							                    </div>
							                <div class="col-xs-4 text-right align-self-bottom">
							                    <img class="img-responsive" src="imgs/app-italiana-immobiliare.png" alt="Italiana Immobiliare | APP">
							                </div>
							            </div>
            </div>
            <div class="modal-footer">
							<button type="button" class="btn btn-default" onclick="$('#myModal').removeClass('show');"data-dismiss="modal" style="font-size:15px;background-color: #911938;color:white;">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<script type="text/javascript">

var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	if (/android/i.test(userAgent)) {
		$('#myModal').toggleClass('show');

	}

	// iOS detection from: http://stackoverflow.com/a/9039885/177710
	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		$('#myModal').toggleClass('show');
	}
</script>

<!-- Header Container / End -->
