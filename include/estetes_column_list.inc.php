		<div class="row">
				<div v-if="estatesList.length===0" class="col-md-12 text-center">
					Nessun immobile presente attualmente
				</div>
				
				<!-- Listing Item -->
				
				<div class="col-md-3 p-4" v-for="(item,index) in estatesList">				
				
					<div style="border:1px solid #cccccc; border-radius:2px;">
                            <!-- 
							<div class="listing-badges">
								<span class="featured"></span>								
							</div>
							-->
							<div class="listing-img-content">
								<span class="like-icon fas fa-heart" v-b-tooltip.hover title="Aggiungi ai preferiti" v-bind:data-estate_id="item.estate_id" v-bind:id="'favorite_'+item.estate_id" v-on:click="vuefavorite('#favorite_'+item.estate_id ,item.estate_id,<?=$_SESSION['user']['user_id']?>)"></span>
							</div>
							
						<a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank" class="listing-img-container">	 
							<div class="listing-carousel">
								<!-- 
								<div v-for="i in 1" :key="i">
                                  	<img v-bind:src="'https://www.itimgest.com' + item.photos_array[i].x200" width="100%" >
                                </div>
                                 -->
                                <div v-bind:style="'height:200px; overflow: hidden; background: url(' + (item.photos_array[0] != null ? 'https://www.itimgest.com' + item.photos_array[0].x200 : 'imgs/italiana-immobiliare-spa_logo.png' ) + '); background-size: cover;'">
                                  	
                                </div>
                                
							</div>							
						</a>
						
						<div class="listing-content">
							
							<div class="listing-price-2">
								<a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank">
									<span v-if="item.TrattativaRiservata==0">€ {{ item.price_formatted }}</span>
									<span v-if="item.TrattativaRiservata==1">Trattativa Riservata</span>
								</a>
								<!-- <span class="va pull-right">{{ (item.Contratto === 'V') ? 'Vendita' : 'Affitto' }}</span> -->
							</div>
							
							<ul class="listing-features">
								<li>
    								<img src="imgs/icone/realestate-v2/24/scale.png" v-b-tooltip.hover title="Superficie" />
    								<span>{{ item.MQSuperficie }} mq</span>
								</li>
								<li>
    								<img src="imgs/icone/realestate-v2/24/blue print.png" v-b-tooltip.hover title="Locali" />
    								<span>{{ item.NrLocali }} Locali</span>									
								</li>
								<li>
									<img src="imgs/icone/realestate-v2/24/bathtub.png" v-b-tooltip.hover title="Servizi" />								
									<span>{{ item.count_room_25 }} Servizi</span>
								</li>
								<li v-if="item.count_room_15>0 || item.count_room_21 >0">
									<img src="imgs/icone/realestate-v2/24/garage_1.png"  v-b-tooltip.hover title="Garage / Posto auto" />								
									<span>Garage</span>
								</li>
								<li v-else-if="item.count_room_10>0 || item.count_room_16 >0">
									<img src="imgs/icone/realestate-v2/24/city_1.png"  v-b-tooltip.hover title="Terrazzo / Balcone / Giardino" />								
									<span>Spazi esterni</span>
								</li>
							</ul>
							
							<div class="listing-title">
								<h4><a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank">{{ item.estate_title }}</a></h4>								
							</div>

							<div class="listing-footer">	
								<a v-bind:href="'product_page.php?estate_id='+ item.estate_id + '#location'" class="listing-address popup-gmaps" style="font-size: 12px;">
									<i class="fa fa-map-marker"></i> {{ item.estate_subtitle }}, {{ item.administrative_area_level_3 }}
								</a>															
								<!-- <a v-bind:href="'agency.php?agency_id='+ item.agency_id" style="font-size: 12px;"><i class="fa fa-user"></i> Agenzia {{ item.agency_name }}</a> -->								
							</div>

						</div>

					</div>
				</div>
				<!-- Listing Item / End -->
				
				
			</div>