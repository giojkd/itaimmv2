<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="imgs/italiana-immobiliare-spa_logo.png" alt="ITALIANA IMMOBILIARE SPA - Franchising">
				<br><br>
				<p>Affittare, comprare o vendere casa a Firenze<br>con noi basta un click!</p>
			</div>

			<div class="col-md-4 col-sm-6 ">
				<h4>ITALIANA IMMOBILIARE S.p.A</h4>
				
				<ul class="footer-links">
					<li><a href="chi-siamo.php">Chi siamo</a></li>
					<li><a href="gruppo-italiana-immobiliare.php">Il gruppo Italiana Immobiliare</a></li>
					<li><a href="franchising.php">Franchising</a></li>
					<li><a href="agencies_list.php">Agenzie</a></li>
								
				</ul>
				
				<ul class="footer-links">
					<li><a href="favorites_list.php">Immobili salvati</a></li>
					<li><a href="#">Guarda le novità</a></li>
					<li><a href="favorites_list.php">Accedi</a></li>
					<li><a href="index.php">Home</a></li>
				</ul>

				
				<div class="clearfix"></div>
			</div>		

			<div class="col-md-3  col-sm-12">
				<h4>Esplora</h4>
				<ul class="social-icons margin-top-20">
					<li><a class="facebook" href="https://www.facebook.com/Italiana-Immobiliare-SpA-107187459315325/" target="blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/italianaimmobil" target="blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/105491762627206975653" target="blank"><i class="icon-gplus"></i></a></li>					
				</ul>

			</div>

		</div>
		
		<div class="row">
			<div class="col-12" style="font-size:12px;">
				<a href="privacy-policy-termini-e-condizioni.php">Privacy Policy</a> | 
				<a href="uso-dei-cookie.php">Uso dei Cookie</a> | 
				<a href="newsletter-policy.php">Newsletter Policy</a>
			</div>
		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">Italiana Immobiliare S.p.A. Franchising Immobiliare - PIVA: 03033690482 - Tutti i diritti riservati</div>
			</div>
		</div>

	</div>

</div>