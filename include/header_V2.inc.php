<!-- Header Container
================================================== -->
<header id="header-container">

	<!-- Topbar -->
	<div id="top-bar">
		<div class="container">

			<!-- Left Side Content -->
			<div class="left-side">

				<!-- Top bar -->
				<ul class="top-bar-menu">
					<li><i class="fa fa-phone"></i> <a href="tel:+39055361146">+39 055 361146</a> </li>
					<li><i class="fa fa-envelope"></i> <a href="mailto:info@italianaimmobiliare.it">italiana@italianaimmobiliare.it</a></li>
					<li>
						<div class="top-bar-dropdown">
							<span>Agenzie</span>
							<ul class="options" id="agenciesList">
								<li><div class="arrow"></div></li>								
								<li v-for="item in agenciesList"><a V-bind:href="'?agency_id='+ item.agency_id" > {{ item.agency_name }}</a> </li>
							</ul>							
						</div>
					</li>
				</ul>
				
				

			</div>
			<!-- Left Side Content / End -->


			<!-- Left Side Content -->
			<div class="right-side">

				<!-- Social Icons -->
				<ul class="social-icons">
					<li><a class="facebook" href="https://www.facebook.com/Italiana-Immobiliare-SpA-107187459315325/" target="blank"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/italianaimmobil" target="blank"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="https://plus.google.com/105491762627206975653" target="blank"><i class="icon-gplus"></i></a></li>					
				</ul>

			</div>
			<!-- Left Side Content / End -->

		</div>
	</div>
	<div class="clearfix"></div>
	<!-- Topbar / End -->


	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="index.php"><img src="imgs/ITAIMMSPA_LOGO.jpg" alt="ITALIANA IMMOBILIARE SPA - Francising"></a>
				</div>


				<!-- Mobile Navigation -->
				<div class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>


				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive" class="categoriesList">
						<li v-for="item in categoriesList"><a V-bind:href="'/?categoria='+ item.IDCategoria" > {{ item.Categoria }}</a> </li>
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->
			<div class="right-side">
				<!-- Header Widget -->
				<div class="header-widget">
					<a href="login-register.html" class="sign-in"><i class="fa fa-user"></i> Accedi / Registrati</a>
					<a href="submit-property.html" class="button border">Inserisci un annuncio</a>
				</div>
				<!-- Header Widget / End -->
			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->