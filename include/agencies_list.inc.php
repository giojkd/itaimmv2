<div class="agent p-4">    				
	<div class="row">
		<div class="col-md-3">
			<a v-bind:href="'searchresult.php?filters[equal][e.agency_id]=' + agency.agency_id " class="agent-avatar">
				<img v-bind:src="'https://www.italianaimmobiliare.it/agenzie/' + agency.agency_logo_path" v-bind:alt="agency.agency_name">
			</a>
		</div>
		<div class="col-md-5">
			<div class="agent-content">
				<div class="agent-name">
					<h4><a v-bind:href="'searchresult.php?filters[equal][e.agency_id]=' + agency.agency_id ">{{ agency.agency_name }}</a></h4>
					<span><i class="fa fa-map-marker"></i> {{ agency.agency_address + ', ' + agency.agency_zip + ', ' + agency.agency_city }}</span>
				</div>

				<ul class="agent-contact-details">
					<li><i class="sl sl-icon-call-in" title="Telefono"></i>{{ agency.agency_phone }}</li>
					<li class="d-none"><i class="sl sl-icon-printer" title="Fax"></i> {{ agency.agency_fax }}</li>
					<li><i class="sl sl-icon-envelope-open"></i><a v-bind:href="'mailto:' + agency.agency_company_email">{{ agency.agency_company_email }}</a></li>
				</ul>

				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>   
		</div>
		<div class="col-md-4 text-center">
		<br>
			<a v-bind:href="'searchresult.php?filters[equal][e.agency_id]=' + agency.agency_id " class="agent-avatar">
				<img v-bind:src="'https://www.itimgest.com' + agency.avatar_array[0].x150" v-bind:alt="agency.agent_name + ' ' + agency.agent_surname" class="rounded img-thumbnail" style="height:150px; width:auto;">
			</a>
			
			
			<ul class="agent-contact-details">
				<li><i class="sl sl-icon-user" title="Telefono"></i>{{ agency.agent_name }} {{ agency.agent_surname }}</li>
			</ul>
		
		</div>
		
	</div>	
</div>