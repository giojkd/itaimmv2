<?php

session_start();
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
include ROOT . '/config/server.inc.php';
include ROOT . '/classes/DBmysql.inc.php';
include ROOT . '/include/functions.inc.php';
include ROOT . '/config/config.inc.php';
include ROOT . '/include/user_setter.inc.php';
include ROOT . '/config/search_results_agencies_extra_data.php';
#include ROOT . '/include/language_setter.inc.php';

$excluded_agencies_list = array("'PC'");

$queryString = parse_str($_SERVER['QUERY_STRING']);
$redirectUri = $_SERVER['REDIRECT_URL'];
?>