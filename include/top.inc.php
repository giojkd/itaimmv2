<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link type="text/css" rel="stylesheet" href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css"/>


<link rel="stylesheet" href="css/style.css?v=1.3">
<link rel="stylesheet" href="css/colors/cherry.css" id="colors">

<link rel="stylesheet" href="css/italianaimmobiliare.css?v=1.4">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css?v=1.2" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<!-- <link href="fonts/fontawesome-free-5-3-1-web/css/all.css"  rel="stylesheet"> -->

<link type="text/css" rel="stylesheet" href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css?v=1.2"/>

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<script type="text/javascript" src="scripts/jquery-2.2.0.min.js"></script>

<!-- development version, includes helpful console warnings
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
<!-- production version, optimized for size and speed -->
<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<!-- include VueJS first -->
<script src="https://unpkg.com/vue@latest"></script>

<!-- use the latest release -->
<script src="https://unpkg.com/vue-select@latest"></script>

<!-- <script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script> -->

<script src="//unpkg.com/babel-polyfill@latest/dist/polyfill.min.js"></script>
<script src="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.js"></script>

<!-- <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAF0AXmV7Q8Ay7CsqseJhVL6AYUxor9k14&amp;sensor=false&amp;language=it-IT"></script> -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7_6qXx1LlpVA576SUsX9OPSdh0jw2WIo&amp;libraries=places&amp;language=it-IT"></script>

