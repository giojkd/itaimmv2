					<form class="main-search-form" action="searchresult.php#" method="get">
						<!-- Box -->
						<div class="main-search-box">
							
							<!-- Main Search Input -->
							<div class="main-search-input larger-input justify-content-center">
                                <div class="col-md-9 p-0 m-0">
                                	<!-- Type -->
                                	<div class="row mb-0 justify-content-center">
                						<div class="search-type">
                							<label class="active"><input class="first-tab" checked="checked" type="radio" name="filters[equal][Contratto]" value="V">Vendita</label>
                							<label><input type="radio" name="filters[equal][Contratto]" value="A">Affitto</label>
                							<label><input type="radio" name="filters[equal][Contratto]" value="VE">Venduti</label>                							 
                						</div>                						
                                	</div>
                                	<div class="row mt-0 justify-content-center">                          	
                                    	<div id="zonesList" style="width:calc(100% - 120px);">                                    	
                                          	<v-select v-model="selectedZone" :options="zonelisted" placeholder='Seleziona un comune o una provincia' class="full-width"></v-select>                                            
                                            <input type="hidden" v-model="selectedZone.value" name="filters[area_type_and_id][0][ISTAT]">
                                            <input type="hidden" v-model="selectedZone.area_type" name="filters[area_type_and_id][0][area_type]">
                                            <input type="hidden" value="Tutte" name="filters[area_type_and_id][0][area_id]">                        
                                        </div>
                                        <button class="button"><i class="fas fa-search fa-2x"></i></button> 
                                    </div>                                    	
                                </div>
							</div>

						</div>
						<!-- Box / End -->
					</form>
					