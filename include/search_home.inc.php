				<form class="main-search-form">
							
							<!-- Type -->
							<div class="search-type">
								<label class="active"><input class="first-tab" name="tab" checked="checked" type="radio" name="filters[equal][Contratto]" value="V">Vendita</label>
								<label><input name="tab" type="radio" name="filters[equal][Contratto]" value="A">Affitto</label>
								<label><input name="tab" type="radio" name="filters[equal][Contratto]" value="V">Venduti</label>
								<div class="search-type-arrow"></div>
							</div>

							
							<!-- Box -->
							<div class="main-search-box">
								
								<!-- Main Search Input -->
								<div class="main-search-input larger-input">
                                    <div class="col-md-9 p-0 m-0">
                                    	<div id="zonesList">                                    	
                                          	<v-select :options="zonelisted" placeholder='Seleziona un comune o una provincia' class="full-width"></v-select>                                      
                                        </div>                                    	
                                    </div>
                                    <div class="col-md-3 p-0 m-0">
                                    	<button class="button">CERCA</button>                                    
                                    	<div id='zona-wrapper-home' style="display:none"></div>
                                    </div>
								</div>

								<!-- Row -->
								<div class="row with-forms">

									<!-- Property Type -->
									<div class="col-md-4 tipologiesList">
										<div class="tipologiesList">
                                          	<v-select :options="tipologieslisted" placeholder="Tipologia" class="full-width"></v-select>
                                        </div>										
									</div>
									
									<!-- Min Area -->
									<div class="col-md-4">
										
										<!-- Select Input -->
										<div class="select-input">
											<input type="text" name="filters[between][MQSuperficie][from]" placeholder="Area min" data-unit="MQ">
										</div>
										<!-- Select Input / End -->

									</div>


									<!-- Max Price -->
									<div class="col-md-4">
										
										<!-- Select Input -->
										<div class="select-input">
											<input type="text" name="filters[between][Prezzo][to]"  placeholder="Prezzo max" data-unit="EU">
										</div>
										<!-- Select Input / End -->

									</div>

								</div>
								<!-- Row / End -->


								<!-- More Search Options -->
								<a href="#" class="more-search-options-trigger" data-open-title="Altre opzioni" data-close-title="Meno opzioni"></a>

								<div class="more-search-options">
									<div class="more-search-options-container">

										<!-- Row -->
										<div class="row with-forms">

											<!-- Min Area -->
											<div class="col-md-4">
												<select data-placeholder="Locali" class="chosen-select-no-single" >
													<option label="blank"></option>	
													<option>1</option>
													<option>2</option>
													<option>3</option>
													<option>4</option>
													<option>+5</option>
												</select>
											</div>

											<!-- Max Area -->
											<div class="col-md-4">
												<select data-placeholder="Bagni" class="chosen-select-no-single" >
													<option label="blank"></option>	
													<option>1</option>
													<option>2</option>
													<option>+3</option>
												</select>
											</div>
											
											<!-- Stato immobile -->
											<div class="col-md-4">
												<select data-placeholder="Stato immobile" class="chosen-select-no-single" >
													<option label="blank"></option>	
													<option>Nuovo / In costruzione</option>
													<option>Ottimo / Ristrutturato</option>
													<option>Buono / Abitabile</option>
													<option>Da ristrutturare</option>
												</select>
											</div>

										</div>
										<!-- Row / End -->
			

										<!-- Checkboxes -->
										<div class="checkboxes in-row">
									
											<input id="check-2" type="checkbox" name="filters[room_count][15]">
											<label for="check-2">Garage / Posto auto</label>

											<input id="check-3" type="checkbox" name="filters[room_count][10]">
											<label for="check-3">Terrazzo / Balcone</label>

											<input id="check-4" type="checkbox" name="filters[room_count][16]" >
											<label for="check-4">Giardino</label>

											<input id="check-5" type="checkbox" name="check">
											<label for="check-5">Cantina / Soffitta</label>	

											<input id="check-6" type="checkbox" name="check">
											<label for="check-6">Piscina</label>
									
										</div>
										<!-- Checkboxes / End -->

									</div>
								</div>
								<!-- More Search Options / End -->


							</div>
							<!-- Box / End -->

						</form>