<!-- Search -->
			<section class="search container-fluid">

				<div class="row">

					<div class="col-md-12">

						<!-- Form -->
						<div class="main-search-box no-shadow">
							<form action="searchresult.php#" method="get">
							<!-- Row With Forms -->
							<div class="row with-forms">

								<!-- Main Search Input -->
								<div class="col-sm-4">
									<div id="zonesList">
                                      	<v-select v-model="selectedZone" :options="zonelisted" placeholder='Seleziona un comune o una provincia' class="full-width"></v-select>
                                        <input type="hidden" v-model="selectedZone.value" name="filters[area_type_and_id][0][ISTAT]" id="search_ISTAT">
                                        <input type="hidden" v-model="selectedZone.area_type" name="filters[area_type_and_id][0][area_type]" id="search_area_type">
                                        <input type="hidden" value="Tutte" name="filters[area_type_and_id][0][area_id]">
                                    </div>
								</div>

								<!-- Status -->
								<div class="col-sm-12 col-md-2">
									<select data-placeholder="Tipo di contratto" class="chosen-select-no-single" name="filters[equal][Contratto]">
										<option value="V">Vendita</option>
										<option value="A">Affitto</option>
										<option value="VE">Venduto</option>
									</select>
								</div>


								<!-- Property Type -->
								<div class="col-sm-12 col-md-2">
									<div class="tipologiesList">
                                      	<v-select v-model="selectedTipologies" :options="tipologieslisted" placeholder="Tipologia" class="full-width"></v-select>
                                      	<input type="hidden" v-model="selectedTipologies.value" value="" name="filters[in][IDTipologia][]">
                                    </div>
								</div>


								<div class="col-sm-12 col-md-2">
									<a href="#" class="more-search-options-trigger" data-open-title="Altri Filtri" data-close-title="Altri Filtri"></a>
								</div>


								<div class="col-sm-12 col-md-2 text-left">
									<!-- Search Button -->
									<button class="button fs-map-btn full-width">Cerca</button>

								</div>
							</div>
							<div class="row with-forms" style="margin-bottom:20px;">
								<div class="col-sm-12">


									<div class="col-sm-12 col-md-3" style="text-align:center;">
											<div class="more-search-options relative">
																	<div class="card card-body">
																		<p>Prezzo</p>
																		<div class="select-input">
											<input type="number" name="filters[between][Prezzo][from]" placeholder="Min" value="0" data-unit="">
										</div>
																			<div class="select-input">
											<input type="number" name="filters[between][Prezzo][to]" placeholder="Max" value="" data-unit="">
										</div>
										<!-- <span class="close ">x chiudi</span> -->
																	</div>

															</div>
									</div>

									<div class="col-sm-12 col-md-3" style="text-align:center;">
											<div class="more-search-options relative">
																					<div class="card card-body">
																						<p>N. Locali</p>
																						<div class="select-input">
															<input type="number" name="filters[between][NrLocali][from]" placeholder="Min" value="0" data-unit="">
														</div>
																							<div class="select-input">
															<input type="number" name="filters[between][NrLocali][to]" placeholder="Max" value="" data-unit="">
														</div>
																					</div>
																			</div>


									</div>

									<div class="col-sm-12 col-md-3" style="text-align:center;">
											<div class="more-search-options relative">
																					<div class="card card-body">
																						<p>MQ. Superficie</p>
																						<div class="select-input">
															<input type="number" name="filters[between][MQSuperficie][from]" placeholder="MQ min" value="0" data-unit="">
														</div>
																							<div class="select-input">
															<input type="number" name="filters[between][MQSuperficie][to]" placeholder="MQ max" value="" data-unit="">
														</div>

																					</div>
																			</div>
									</div>
									
									
									
									
									<div class="col-sm-12 col-md-3">									
    									<div class="more-search-options relative">
        									<div class="card card-body">
        									
        									<ul class="list-group">
                                                <li class="list-group-item">
                                                    Giardino
                                                    <div class="material-switch pull-right">
                                                        <input id="filters[room_count][16]" name="filters[room_count][16]" type="checkbox" value="1" />
                                                        <label for="filters[room_count][16]" class="label-default"></label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    Garage
                                                    <div class="material-switch pull-right">
                                                        <input id="filters[room_count][15]" name="filters[room_count][15]" type="checkbox" value="1" />
                                                        <label for="filters[room_count][15]" class="label-default"></label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    Terrazzo/Balcone
                                                    <div class="material-switch pull-right">
                                                        <input id="filters[room_count][10]" name="filters[room_count][10]" type="checkbox" value="1" />
                                                        <label for="filters[room_count][10]" class="label-default"></label>
                                                    </div>
                                                </li>
                                                <li class="list-group-item">
                                                    Ascensore
                                                    <div class="material-switch pull-right">
                                                        <input id="filters[binary_fields][15]" name="filters[binary_fields][15]" type="checkbox" value="1" />
                                                        <label for="filters[binary_fields][15]" class="label-default"></label>
                                                    </div>
                                                </li>
        									</ul>
        									
                                			</div>
                            			</div>
                                    </div>
									
									
									
									

								</div>

								<!-- Min Locali -->


								<!-- MQ -->




							</form>
							</div>
							<!-- Row With Forms / End -->

						</div>
						<!-- Box / End -->
					</div>
				</div>

			</section>
			<!-- Search / End -->
