                    <!-- Listing Item -->
    				<div class="listing-item" v-for="item in estatesList">
    						
    						<!-- 
    						<div class="listing-badges">
    							<span class="featured"></span>    							
    						</div>
    						 -->
    						<div class="listing-img-container">
    						<div class="listing-img-content">
    							<!-- <span class="like-icon fas fa-heart" data-tip-content="Aggiungi ai preferiti"></span> -->
    							<span class="like-icon fas fa-heart" v-b-tooltip.hover title="Aggiungi ai preferiti" v-bind:data-estate_id="item.estate_id" v-bind:id="'favorite_'+item.estate_id" v-on:click="vuefavorite('#favorite_'+item.estate_id ,item.estate_id,<?=$_SESSION['user']['user_id']?>)"></span>
    						</div>
        					<a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank">
        						<div class="listing-carousel-2">
        							<div v-bind:style="'height:200px; overflow: hidden; background: url(' + ((item.photos_array[0].x200.search('https://www.itimgest.com') === -1) ? 'https://www.itimgest.com' + item.photos_array[0].x200.replace('http://www.itimgest.com','') : item.photos_array[0].x200) + '); background-size: cover;'"></div>
        						</div>
        					</a>
        					</div>
    					
    					<div class="listing-content">
    						<div class="listing-title">
    							<h4>
    								<a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank" v-bind:title="(item.Contratto === 'V') ? 'Vendita ' + item.NomeTipologia + ' ' + item.administrative_area_level_3 : 'Affitto ' + item.NomeTipologia + ' ' + item.administrative_area_level_3 ">
    									<span style="font-size:27px; color:#911938;" v-if="item.TrattativaRiservata==0">€ {{ item.price_formatted }}</span>
    									<span style="font-size:27px; color:#911938;" v-if="item.TrattativaRiservata==1">Trattativa Riservata</span>
    								</a>
    							</h4>
    							<h5 style="line-height:normal;">{{ item.estate_title }} | {{ item.NomeTipologia }}</h5>
    							
    							<a v-bind:href="'product_page.php?estate_id='+ item.estate_id" target="_blank" class="details button border">Vedi</a>
    						</div>
    						<ul class="listing-details">
    							<li>
    								<img src="imgs/icone/realestate-v2/24/scale.png" v-b-tooltip.hover title="Superficie" /><br>
    								<span>{{ item.MQSuperficie }} mq</span>
    							</li>
    							<li>
    								<img src="imgs/icone/realestate-v2/24/blue print.png" v-b-tooltip.hover title="Locali" /><br>
    								<span>{{ item.NrLocali }} Locali</span>									
    							</li>
    							<li>
    								<img src="imgs/icone/realestate-v2/24/bathtub.png" v-b-tooltip.hover title="Servizi" /><br>							
    								<span>{{ item.count_room_25 }} Servizi</span>
    							</li>
    							<li v-if="item.count_room_15>0 || item.count_room_21 >0">
    								<img src="imgs/icone/realestate-v2/24/garage_1.png"  v-b-tooltip.hover title="Garage / Posto auto" /><br>							
    								<span>Garage</span>
    							</li>
    							<li v-else-if="item.count_room_10>0 || item.count_room_16 >0">
    								<img src="imgs/icone/realestate-v2/24/city_1.png"  v-b-tooltip.hover title="Terrazzo / Balcone / Giardino" /><br>							
    								<span>Spazi esterni</span>
    							</li>
    						</ul>
    
    						<div class="listing-footer">
    							<div class="col-md-7 text-left">
    							<a v-bind:href="'product_page.php?estate_id='+ item.estate_id + '#location'" class="listing-address popup-gmaps" style="font-size: 12px;">
    								<i class="fa fa-map-marker"></i>
    								{{ item.estate_subtitle }}, {{ item.administrative_area_level_3 }}    								
    							</a>
    							</div>
    							<div class="col-md-5 text-left p-0" style="line-height:normal;">
    								<a v-bind:href="'agencies_list.php?agency_id='+ item.agency_id" style="font-size: 12px;"><i class="fa fa-user"></i> Affiliato {{ item.agency_name }}</a>
    							</div>
    							<!-- <span class="va pull-right">{{ item.count_photos }} foto</span> -->
    														
    						</div>
    
    					</div>
    
    				</div>
    				<!-- Listing Item / End -->