<!DOCTYPE html>
<head id="estate">

<!-- Basic Page Needs
================================================== -->
<title>Italiana Immobiliare</title>
<meta name="description" content="Appartamenti e case in vendita e in affitto a Firenze. Affittare, comprare o vendere casa a Firenze è facile. Realizziamo i sogni degli italiani."/>
<link rel="stylesheet" href="./css/leaflet.css" />
<?include 'include/top.inc.php'; ?>



<style>

.container-class{
  width:calc(100% / 8);
  height:25px;
  color:white;
  text-align: center;
  position: relative;
}

.energy-class{
  position: relative;
}
.class-aplus{
  background-color:#6d9e00;
}
.class-a{
  background-color:#7fb800;
}
.class-b{
  background-color:#91d100;
}
.curr-class{
  position: absolute;
  top: -55px;
  left: 7%;
  padding: 8px 12px;
  white-space: nowrap;
  color: #fff;
  background-color: #999;
  font-weight: 700;
}
.curr-class:before{
  content: '';
  position: absolute;
  border-top: 10px solid #999;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  bottom: -10px;
  left: 8%;
  margin-left: 5px;
}
.class-c{
  background-color:#ebc400;
}
.class-d{
  background-color:#eb9d00;
}
.class-e{
  background-color:#e67300;
}
.class-f{
  background-color:#d22300;
}
.class-g{
  background-color:#b80000;
}

#OSMmap { height: 52vh; width:100%; }
ol {
    counter-reset: li; /* Initiate a counter */
    list-style: none; /* Remove default numbering */
    list-style: decimal; /* Keep using default numbering for IE6/7 */
    font: 15px 'trebuchet MS', 'lucida sans';
    margin-bottom: 4em;
    text-shadow: 0 1px 0 rgba(255,255,255,.5);
}

.POIicon{
	position: absolute;
	left: -10px;
	top: 0px;
	margin-top: -1.3em;
	background: #911938;
	width: 30px!important;
	line-height: 24px;
	border: .3em solid #fff;
	text-align: center;
	font-weight: bold;
	border-radius: 2em;
	transition: all .3s ease-out;
	color: white;
	bottom: 0;
	height: 30px!important;
	margin: auto;
	box-shadow: 0px 0px 6px 0px black;
	font-size:14px;
}

 #POIresults::-webkit-scrollbar {
    width: 5px;
}

#POIresults::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}

#POIresults::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}


ol ol {
    margin: 0 0 0 2em; /* Add some left margin for inner lists */
}

.rounded-list li{
    position: relative;
    display: block;
    padding: .4em .4em .4em 2em;
    *padding: .4em;
    margin: .5em 0;
    background: #ddd;
    color: #444;
    text-decoration: none;
    border-radius: .3em;
    transition: all .3s ease-out;
}

.rounded-list li:hover{
    background: #eee;
}

.rounded-list li:hover:before{
    transform: rotate(360deg);
}

.rounded-list li:before{
    content: counter(li);
    counter-increment: li;
    position: absolute;
    left: -10px;
    top: 0px;
    margin-top: -1.3em;
    background: #911938;
    width: 30px;
    line-height: 24px;
    border: .3em solid #fff;
    text-align: center;
    font-weight: bold;
    border-radius: 2em;
    transition: all .3s ease-out;
    color: white;
    bottom: 0;
    height: 30px;
    margin: auto;
    box-shadow: 0px 0px 6px 0px black;
}
</style>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Compare Properties Widget
================================================== -->
<?/*include 'include/compare.inc.php'; */?>

<!-- Compare Properties Widget / End -->


<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>


<div v-for="estate in estates">

<!-- Titlebar
================================================== -->

<div id="titlebar" class="property-titlebar margin-bottom-0 estate">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<a href="#" class="back-to-listings d-none"></a>
				<div class="property-title" style="margin-left:0;">
					<h2>{{estate.estate_title}} <span class="property-badge">{{ (estate.status == 0 ) ? ((estate.Contratto === 'V') ? 'Vendita' : 'Affitto') : 'VENDUTO'}}</span></h2>
					<span>
						<a href="#location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							{{estate.estate_subtitle}}, {{estate.administrative_area_level_3}}
						</a>
					</span>
				</div>

				<div class="property-pricing">
					<div class="property-price" v-if="estate.TrattativaRiservata==0">€ {{ estate.price_formatted }}</div>
					<div class="property-price" v-if="estate.TrattativaRiservata==1">Trattativa Riservata</div>
					<div class="sub-price">RIF: {{ estate.ref }}</div>
				</div>


			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Slider -->
<div class="fullwidth-property-slider margin-bottom-50" id="#estate_gallery">
	<a v-for="photo in estate.photos_array" class="item mfp-gallery" v-bind:href="'https://www.itimgest.com' + photo.x800" v-bind:data-background-image="((photo.x800.search('https://www.itimgest.com') === -1) ? 'https://www.itimgest.com' + photo.x800.replace('http://www.itimgest.com','') : photo.x800)"></a>
</div>



<div class="container">
	<div class="row">

		<!-- Property Description -->
		<div class="col-lg-8 col-md-7">
			<div class="property-description">

				<!-- Main Features -->
				<ul class="property-main-features" style="text-align:center;">
					<li>
						<img src="imgs/icone/realestate-v2/24/scale.png" v-b-tooltip.hover title="Superficie" />
						<span>{{ estate.MQSuperficie }} mq</span>
					</li>
					<li>
						<img src="imgs/icone/realestate-v2/24/blue print.png" v-b-tooltip.hover title="Locali" />
						<span>{{ estate.NrLocali }} Locali</span>
					</li>
					<li>
						<img src="imgs/icone/realestate-v2/24/bathtub.png" v-b-tooltip.hover title="Servizi" />
						<span>{{ estate.count_room_25 }} Servizi</span>
					</li>
					<li v-if="estate.count_room_15>0 || estate.count_room_21 >0">
						<img src="imgs/icone/realestate-v2/24/garage_1.png"  v-b-tooltip.hover title="Garage / Posto auto" />
						<span>Garage</span>
					</li>
					<li v-else-if="estate.count_room_10>0 || estate.count_room_16 >0">
						<img src="imgs/icone/realestate-v2/24/city_1.png"  v-b-tooltip.hover title="Terrazzo / Balcone / Giardino" />
						<span>Spazi esterni</span>
					</li>
				</ul>

				<ul class="property-main-features" style="display:none">
					<li v-if="estate.Arredamento==='1'">Arredato <span><br></span></li>
					<li>Locali <span>{{ estate.NrLocali }}</span></li>
					<li>Metri quadrati <span>{{ estate.MQSuperficie }} m<sup>2</sup></span></li>
					<li v-for="(feature, index) in estate.room" v-if="index == 25 || index == 11 || index == 16"> {{ feature.room_type }}  <span>{{ feature.quantity }}</span></li>

				</ul>


				<!-- Description -->
				<h3 class="desc-headline">Descrizione</h3>
				<div class="show-more">
					<p>{{ estate.estate_description }}</p>

					<a class="show-more-button" style="color: #911938;cursor:pointer;" onclick="$('.show-more').toggleClass('visible');">Leggi tutto <i class="fa fa-angle-down"></i></a>

				</div>

				<!-- Details -->
				<h3 class="desc-headline">Dettagli</h3>
				<ul class="property-features margin-top-0">
					<li>Anno di costruzione: <span>{{ estate.AnnoCostruzione}}</span></li>
					<li>Classe Energetica: <span id="estateEnergyClass" v-bind:data-EPgl="estate.EPgl">
            {{ (estate.ClasseEnergetica === '') ? 'ND' : '' }}
            {{ (estate.ClasseEnergetica === '1') ? 'A+' : '' }}
            {{ (estate.ClasseEnergetica === '2') ? 'A' : '' }}
            {{ (estate.ClasseEnergetica === '3') ? 'B' : '' }}
            {{ (estate.ClasseEnergetica === '4') ? 'C' : '' }}
            {{ (estate.ClasseEnergetica === '5') ? 'D' : '' }}
            {{ (estate.ClasseEnergetica === '6') ? 'F' : '' }}
            {{ (estate.ClasseEnergetica === '7') ? 'G' : '' }}
            {{ (estate.ClasseEnergetica === '8') ? 'A4' : '' }}
            {{ (estate.ClasseEnergetica === '9') ? 'A3' : '' }}
            {{ (estate.ClasseEnergetica === '10') ? 'A2' : '' }}
            {{ (estate.ClasseEnergetica === '11') ? 'A1' : '' }}
            {{ (estate.ClasseEnergetica === '12') ? 'A' : '' }}
            {{ (estate.ClasseEnergetica === '13') ? 'B' : '' }}
            {{ (estate.ClasseEnergetica === '14') ? 'C' : '' }}
            {{ (estate.ClasseEnergetica === '15') ? 'D' : '' }}
            {{ (estate.ClasseEnergetica === '16') ? 'E' : '' }}
          </span></li>
					<li>Balconi: <span>1</span></li>
					<li>Cucina: <span>Abitabile</span></li>
					<li>Camera da letto: <span>1</span></li>
					<li>Camerina: <span>1</span></li>
					<li v-if="estate.Piano > 0">Piano: <span>{{ estate.Piano }} di {{ estate.PianiEdificio }}</span></li>
				</ul>

        <div class="row" style="margin-top: 60px; margin-left:0; margin-right:0; padding:10px;">
          <div id="energyClassA+" class="container-class">
            <div class="energy-class class-aplus"><p>A+</p></div>
          </div>

          <div id="energyClassA" class="container-class">
            <div class="energy-class class-a"><p>A</p></div>
          </div>

          <div id="energyClassB" class="container-class">
            <div class="energy-class class-b"><p>B</p></div>
          </div>
          <div id="energyClassC" class="container-class">
            <div class="energy-class class-c"><p>C</p></div>
          </div>
          <div id="energyClassD" class="container-class">
            <div class="energy-class class-d"><p>D</p></div>
          </div>
          <div id="energyClassE" class="container-class">
            <div class="energy-class class-e"><p>E</p></div>
          </div>
          <div id="energyClassF" class="container-class">
            <div class="energy-class class-f"><p>F</p></div>
          </div>
          <div id="energyClassG" class="container-class">
            <div class="energy-class class-g"><p>G</p></div>
          </div>
        </div>


				<!-- Features -->
				<h3 class="desc-headline">Caratteristiche</h3>
				<ul class="property-features checkboxes margin-top-0">
					<li>Riscaldamento {{ estate.RiscaldamentoNome }}</li>
					<li>Condizioni:
						{{ (estate.CondizioniImmobile === '1') ? 'Nuovo' : '' }}
						{{ (estate.CondizioniImmobile === '2') ? 'Buono' : '' }}
						{{ (estate.CondizioniImmobile === '3') ? 'Ristrutturato' : '' }}
						{{ (estate.CondizioniImmobile === '4') ? 'Mediocre' : '' }}
						{{ (estate.CondizioniImmobile === '5') ? 'Da Ristrutturare' : '' }}
						{{ (estate.CondizioniImmobile === '6') ? 'Ottimo' : '' }}
						{{ (estate.CondizioniImmobile === '7') ? 'Discreto' : '' }}
						{{ (estate.CondizioniImmobile === '8') ? 'In costruzione' : '' }}
						{{ (estate.CondizioniImmobile === '9') ? 'ND' : '' }}
					</li>
					<li v-for="forcepoint in estate.binary_fields">{{ forcepoint }}</li>
					<li v-for="feature in estate.room"> {{ feature.room_type }}: {{ feature.quantity }}</li>

				</ul>


				<!-- Floorplans -->
				<h3 class="desc-headline no-border">Planimetrie</h3>

				<!-- Accordion -->
				<div v-if="estate.plan_array =='' ">Nessuna presente</div>
				<div role="tablist" v-if="estate.plan_array != ''">
    			    <b-card no-body class="mb-1">
    			      <b-card-header header-tag="header" class="p-1" role="tab">
    			        <b-btn block href="#" style="background-color: #912938;border-color: #912938;" v-b-toggle.accordion1 variant="info">Piano {{ (estate.Piano > 0) ? estate.Piano + ' di ' + estate.PianiEdificio : 'terra' }} <span>{{ estate.MQSuperficie }} mq</span> <i class="fa fa-angle-down"></i></b-btn>
    			      </b-card-header>
    			      <b-collapse id="accordion1" accordion="my-accordion" role="tabpanel">
    			        <b-card-body>
							
							<div v-for="plan in estate.plan_array">
							
    							<a class="floor-pic mfp-image"  v-bind:href="'https://www.itimgest.com' + plan.x800">
     	 							<img v-bind:src="'https://www.itimgest.com' + plan.x640" alt="Planimetria">
     	 						</a>
 	 						</div>
 	 						
    			        </b-card-body>
    			      </b-collapse>
    			    </b-card>
				</div>


				<!-- Location -->
				<h3 class="desc-headline no-border" id="location">Mappa</h3>
				<div id="propertyMap-container">
					<div id="propertyMap" v-bind:data-latitude="estate.lat" v-bind:data-longitude="estate.lng"></div>
					<a href="#" id="streetView">Street View</a>
				</div>


				<!-- Widget -->
				 <?php /*
				<div class="widget">
					<h3 class="margin-bottom-30 margin-top-30">Calcola mutuo</h3>

					<!-- Mortgage Calculator -->
					<form action="javascript:void(0);" autocomplete="off" class="mortgageCalc" data-calc-currency="EUR">
						<div class="calc-input col-md-6">
							<div class="pick-price tip" data-tip-content="Set This Property Price"></div>
						    <input type="text" id="amount" name="amount" placeholder="Prezzo di vendita" v-bind:value="estate.Prezzo" required>
						    <label for="amount" class="fa fa-money"></label>
						</div>

						<div class="calc-input col-md-6">
						    <input type="text" id="downpayment" placeholder="Importo del mutuo">
						    <label for="downpayment" class="fa fa-money"></label>
						</div>

						<div class="calc-input col-md-6">
							<input type="text" id="years" placeholder="Durata mutuo (Anni)" required>
							<label for="years" class="fa fa-calendar-o"></label>
						</div>

						<div class="calc-input col-md-6">
							<input type="text" id="interest" placeholder="Tasso di interesse" required>
							<label for="interest" class="fa fa-percent"></label>
						</div>

						<button class="button calc-button" formvalidate>Calcola</button>
						<div class="calc-output-container"><div class="notification success">Monthly Payment: <strong class="calc-output"></strong></div></div>
					</form>
					<!-- Mortgage Calculator / End -->

				</div>
				
				*/?>
				<!-- Widget / End -->
				</div>
			</div>
			<div class="col-lg-4 col-md-5">
				<div class="sidebar sticky right">

					<!-- Widget -->
					<div class="widget margin-bottom-30 d-none">
						<button class="widget-button with-tip" data-tip-content="Print"><i class="sl sl-icon-printer"></i></button>
						<button class="widget-button with-tip" data-tip-content="Add to Bookmarks"><i class="fa fa-star-o"></i></button>
						<button class="widget-button with-tip compare-widget-button" data-tip-content="Add to Compare"><i class="icon-compare"></i></button>
						<div class="clearfix"></div>
					</div>
					<!-- Widget / End -->


					<!-- Widget -->
					<div class="widget">

						<!-- Agent Widget -->
						<div class="agent-widget">
							<div class="agent-title">
								<div class="agent-photo"><img v-bind:src="'https://www.italianaimmobiliare.it/agenzie/' + estate.agency_logo_path" v-bind:alt="estate.agency_name" /></div>
								<div class="agent-details">
									<h4><a v-bind:href="'agencies_list.php?agency_id=' + estate.agency_id ">{{ estate.agency_name }} </a></h4>
									<span><i class="sl sl-icon-call-in"></i>{{ estate.agency_company_phone }}</span>
								</div>
								<div class="clearfix"></div>
							</div>

							<form role="form" action="https://www.itimgest.com/api/web_send_request.php" class="ajax-form" method="post">
							    <input type="hidden" name="op" value="estate_contact_request">
	                            <input type="hidden" name="contact_reason" value="Info immobile">
	                            <input type="hidden" name="client_id" value="">
	                            <input type="hidden" name="estate_id" v-bind:value="estate.estate_id">
	                            <!-- <input type="hidden" name="agency_email" v-bind:value="estate.agency_email"> -->
	                            <input type="hidden" name="agency_email" value="design@amtitalia.com">
	                            <div class="form-group">
	                                <input name="client_name" placeholder="Nome e Cognome" required="required" id="product-page-name-and-surname-request-form" type="text" class="form-control" value="">
	                            </div>
	                            <div class="form-group">
	                                <input name="client_email" placeholder="La tua Email" required="required" type="email" class="form-control" value="">
	                            </div>
	                            <div class="form-group">
	                                <input name="client_phone" placeholder="Il tuo Telefono" required="required" type="text" class="form-control" value="">
	                            </div>
	                            <div class="form-group">
	                                <textarea name="client_message" class="form-control">REF: {{ estate.ref }} - Vorrei ricevere maggiori informazioni su questo immobile!</textarea>
	                            </div>
	                            <div class="form-group" style="display:block;">
	                                <input type="checkbox" name="privacy" id="check_privacy" required="" style="display:inline;height:20px;width:20px;">
									<label style="display:inline;font-size:12px;" for="check_privacy">Accetto condizioni <a href="privacy-policy-termini-e-condizioni.php">Privacy e Policy</a></label>
	                            </div>
								<button type="submit" class="button fullwidth margin-top-5">Invia il messaggio</button>
								
								<div class="col-md-12 alert fade alert-success" style="display:none">
                                    <span class="message-value"></span>
                                </div>
                                <div class="col-md-12 alert fade alert-danger" style="display:none">
                                    <span class="message-value"></span>
                                </div>
	                        </form>

						</div>
						<!-- Agent Widget / End -->

					</div>
					<!-- Widget / End -->


					<!-- Widget -->
					<div class="widget">
						<h3 class="margin-bottom-30 margin-top-30">Calcola mutuo con Euroansa</h3>
						<iframe src="preventivatore-2018-02-01-16-03-38-876273052.html" width="100%" height="500" border="0" style="border:0px"></iframe>

					</div>



				</div>
			</div>
		</div>
	</div>
  <div class="d-none d-md-block">
        <h3 class="desc-headline no-border" style="text-align:center;">Punti di interesse</h3>
				<!-- Punti di interesse -->

				<div class="row" style="margin: 0;">

    				<div class="container" style="padding-left:0;padding-right:0;">
    					<div class="row" style="margin:0;">
								<div class="col" style="padding-right:0px;">
									<div id="OSMmap"></div>
								</div>
								<div class="col" style="max-width:400px; height:100%;padding-right:30px;">
									<form>
										<select name="poi" onchange="loadPoi();">
											<option selected>Seleziona il punto di interesse...</option>
											<option value="amenity|bank">Banche</option>
											<option value="amenity|pub">Pub</option>
											<option value="amenity|pharmacy">Farmacie</option>
											<option value="shop|supermarket">Supermarket</option>
											<option value="shop|convenience">Discounts</option>
										</select>
									</form>
									<div id='POIresults' style="height:calc(50vh - 58px);position:relative;overflow-y:scroll;"></div>
								</div>
							</div>
    				</div>
				</div>
      </div>



		<div class="row">
			<div class="container">
				<div class="col">



				<!-- Similar Listings Container -->
				<h3 class="desc-headline no-border margin-bottom-35 margin-top-60" style="text-align:center;">Immobili simili</h3>

				<!-- Layout Switcher -->

				<div class="layout-switcher hidden"><a href="#" class="list"><i class="fa fa-th-list"></i></a></div>
				<div class="listings-container list-layout">


    				<div id="estatesList">
    					<?include 'include/estates_list.inc.php'; ?>
    				</div>



				</div>
				<!-- Similar Listings Container / End -->

			</div>
		</div>
		<!-- Property Description / End -->


		<!-- Sidebar -->
		<!-- Sidebar / End -->

	</div>
</div>


</div>
<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>


<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>
<script type="text/javascript" src="scripts/leaflet.js"></script>
<script type="text/javascript" src="scripts/OSMcore.js"></script>

<script type="text/javascript" src="scripts/immobile.js"></script>

</body>
</html>
