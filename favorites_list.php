<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>I tuoi immobili favoriti | Italiana Immobiliare</title>
<meta name="description" content="I tuoi immobili preferiti"/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Compare Properties Widget
================================================== -->
<?/*include 'include/compare.inc.php'; */?>

<!-- Compare Properties Widget / End -->


<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>


<!-- Titlebar
================================================== -->
<div id="titlebar" style="background:url(imgs/slider/unclickeseiacasa.jpg); padding:0;">
<div style="padding-top:70px; padding-bottom:70px; background: rgba(255, 255, 255, 0.5); ">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>My Italiana Immobiliare</h2>
			</div>
		</div>
	</div>
	</div>
</div>


<!-- Content
================================================== -->

<div class="container" id="accedi">
	<div class="row">
		<div class="col-md-12 text-center">
			<button class="facebook-button" onclick="login_fb()"><i class="fa fa-facebook-square fa-2x"></i> Accedi o Registrati con il tuo account Facebook</button>			
			<div style="height:50px;"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
    		<h4>Accedi con il tuo account<br></h4>
    		
    		<form role="form" class="ajax-form" action="https://www.itimgest.com/api/web_base.php">
                <input type="hidden" name="op" value="sign-in">
                <div class="form-group">
                    <label>Il tuo indirizzo email:</label>
                    <input type="email" class="form-control" required="" name="client_email" placeholder="Il tuo indirizzo email">
                </div>
                <div class="form-group">
                    <label>Password:</label>
                    <input type="password" class="form-control" required="" name="client_password" placeholder="La tua password">
                </div>
                <div class="form-group">
                    <p class="text-privacy">Inviando questo modulo si acconsente il trattamento dei dati personali sopra inseriti, per la fruizione del servizio richiesto, i dati saranno tutelati in base al D.L. n. 196 del 30 giugno 2003</p>
                    <label class="checkbox-inline"><input required="" type="checkbox" name="privacy" style="width:30px; height:15px;">Acconsento</label>
                </div>
                <button class="btn btn-success">Accedi</button>
                <div class="col-md-12 alert fade alert-success" style="display:none">
                    <span class="message-value"></span>
                </div>
                <div class="col-md-12 alert fade alert-danger" style="display:none">
                    <span class="message-value"></span>
                </div>
            </form>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-5">
			<h4>Non hai ancora un account? Crealo adesso!</h4>
			<form role="form" class="ajax-form" action="https://www.itimgest.com/api/web_base.php">
                <input type="hidden" name="op" value="sign-up">
                <div class="form-group">
                    <label>Il tuo indirizzo email:</label>
                    <input type="email" class="form-control" required="" name="client_email" placeholder="Il tuo indirizzo email">
                </div>
                <div class="form-group">
                    <label>Password:</label>
                    <input type="password" class="form-control" required="" name="client_password" placeholder="La tua password">
                </div>
                <div class="form-group">
                    <label class="checkbox-inline"><input type="checkbox" name="client_newsletter" style="width:30px; height:15px;">Iscriviti alla nostra newsletter</label>
                </div>
                <div class="form-group">
                    <p class="text-privacy">Inviando questo modulo si acconsente il trattamento dei dati personali sopra inseriti, per la fruizione del servizio richiesto, i dati saranno tutelati in base al D.L. n. 196 del 30 giugno 2003</p>
                    <label class="checkbox-inline"><input required="" type="checkbox" name="privacy" style="width:30px; height:15px;">Acconsento</label>
                </div>
                <button class="btn btn-success">Crea il tuo account</button>
                <div class="col-md-12 alert fade alert-success" style="display:none">
                        <span class="message-value"></span>
                </div>
                <div class="col-md-12 alert fade alert-danger" style="display:none">
                    <span class="message-value"></span>
                </div>
            </form>
		
		
		</div>
	</div>
	<div style="height:50px;"></div>
</div>


<div class="container" id="client_log">
    <div class="row">
    	<div class="col-md-12 text-center">
    	<h2>Ciao <span id="client_log_name"></span> <span id="client_log_email"></span></h2>
		<p><button class="button logout">ESCI <i class="fa fa-sign-out"></i></button></p>
    	</div>
    </div>
	
</div>

<div class="container" id="estatesList">
	<div class="row">
		<div class="col-md-12 text-center">
			<h3>Le tue case preferite</h3>	
			<div id="message_preferiti" class="d-none text-center">
				Per vedere la tua lista di immobili devi effettuare la LOGIN
			</div>	
		</div>
		
	</div>
	<?include 'include/estetes_column_list.inc.php'; ?>
</div>

<div style="height:50px;"></div>


<!-- Footer
================================================== -->

<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>

<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
<script type="text/javascript" src="scripts/favorites_list.js"></script>

</body>
</html>
