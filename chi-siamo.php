<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Chi siamo | Italiana Immobiliare</title>
<meta name="description" content="Appartamenti e case in vendita e in affitto a Firenze. Affittare, comprare o vendere casa a Firenze è facile. Realizziamo i sogni degli italiani."/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>
<!-- Banner
================================================== -->
<div class="container-fluid p-0">
	<div class="row">
		<div class="col-md-12">
			<img src="imgs/italianaimmobiliare_chi_siamo.jpg" alt="Italiana Immobiliare" class="img-flex hidden-xs hidden-sm" />
			<img src="imgs/presidente-lorenzo-coppoli-italiana-immobiliare.jpg" alt="Presidente Italiana Immobiliare Lorenzo Coppoli" class="hidden-md hidden-lg" width="100%" />
						
		</div>
	</div>
</div>



<div class="container">
	<div class="row">
		<div class="col-md-12 contenuto">
			
            <h2 class="text-center headline with-border margin-top-45 margin-bottom-25">Chi siamo</h2>
            
            <p>“<strong>Italiana Immobiliare</strong>” è frutto dell’intuizione del suo fondatore, <strong>Lorenzo Coppoli</strong>, che la costituì a <strong>Firenze</strong> nel <strong>1980</strong> e che rapidamente si è affermata come la più importante e qualificata organizzazione immobiliare sul territorio.</p>
            
            <p>Il successo di Italiana Immobiliare è basato su valori che sin dal principio hanno contraddistinto l’azienda: innovazione, ricerca, trasparenza e professionalità.</p>
            
            <p>Attualmente Italiana Immobiliare vanta un’ampia squadra di professionisti, che vengono preparati con l’esclusivo metodo <strong>Itim Academy</strong>, un processo formativo studiato per esaltare
            la preparazione e la competenza dei collaboratori.</p>
             
            <p>Punto di forza di Italiana Immobiliare è la combinazione tra 40 anni di esperienza e la costante ricerca di miglioramento, che permette all’azienda di svilupparsi all’insegna della
            tradizione ma anche del rinnovamento.</p>
            
            
            
            
            <h3 class="text-center headline with-border margin-top-45 margin-bottom-25">La nostra storia</h3>
            
            <p><mark class="color">Oggi</mark> Italiana Immobiliare rappresenta una solida realtà nel mercato immobiliare in grado di offrire servizi professionali e personalizzati che si adattano alle esigenze del cliente.
            Forte di 40 anni di attività e viva più che mai, vanta un marchio divenuto ormai garanzia di competenza ed affidabilità.<br>
            La sua struttura ben organizzata è improntata sul continuo aggiornamento nell’ambito tecnologico e sul costante investimento nella formazione per la creazione di figure professionali complete ed esperte.</p>
            
            <p><mark class="color">2018</mark> Italiana Immobiliare consolida la partnership con <strong>Euroansa</strong>, famosa Società di Mediazione Creditizia.</p>
            
            <p><mark class="color">2017</mark> Si costituiscono due nuovi comparti: <strong>ItimMedia</strong> ed <strong>ItimWeb</strong>. Entrambi rivolti al mondo della comunicazione e del marketing, costituiscono due pilastri fondamentali per la
            creazione di campagne di visibilità, per l’organizzazione di eventi e per il costante sviluppo nel mondo del web.</p>
            
            <p><mark class="color">2016</mark> Italiana Immobiliare, azienda sempre al passo con i tempi, mette a punto il nuovo gestionale informatico, Itimgest, all’avanguardia ed in costante evoluzione che supporta e
            snellisce l’operato dei collaboratori del gruppo.</p>
            
            <p><mark class="color">2015</mark> Seguendo la spinta ecologica, Italiana Immobiliare, per quanto concerne la mobilità, decide di investire nelle risorse ecosostenibili.</p>
            
            <p><mark class="color">2014</mark> I corsi di educazione immobiliare per tutte le risorse del gruppo vengono strutturati dal reparto formazione <strong>ItimAcademy</strong>.</p>
            
            <p><mark class="color">2013</mark> Si costituisce il gruppo Italiana Immobiliare, forte di pluriennale cultura professionale nel settore immobiliare, ed inizia a riunire i propri settori aziendali, creati nel tempo per arricchire il servizio al cliente finale.</p>
            
            <p><mark class="color">2012</mark> La <strong>Camera di Commercio di Firenze</strong> premia Italiana Immobiliare con la “<strong>Medaglia D’Oro per la fedeltà al lavoro e per il progresso economico</strong>”.</p>
            
            <p><mark class="color">2011</mark> La formula Italiana Immobiliare viene apprezzata e riconosciuta in tutto il mondo. L’azienda viene insignita del premio <strong>Best Franchisee of the World 2011</strong> Vincitore: <strong>Affiliato Prunecchi srl</strong>.</p>
            
            <p><mark class="color">2009</mark> Italiana Immobiliare, sempre attenta alla realtà circostante, iniza ad impegnarsi sul fronte sociale instaurando una collaborazione sentita ed attiva con la <strong>Fondazione ANT</strong> (nota associazione nazionale che sostiene i malati di tumore).</p>
            
            <p><mark class="color">2006</mark> Nasce il periodico mensile Diamocasa.it, edito da <strong>Itimpress srl</strong>, dal taglio innovativo ed elegante. La pubblicazione vanta una tiratura di migliaia di copie.</p>
            
            <p><mark class="color">2000</mark> L’attività di sviluppo franchising inzia a diffondersi in modo capillare sul territorio diventando un preciso punto di riferimento immobiliare per i potenziali acquirenti o venditori
            residenti nell’hinterland del capoluogo regionale.</p>
            
            <p><mark class="color">1994</mark> Italiana Immobiliare si trasforma in <strong>Società per Azioni</strong>. Sono questi gli anni in cui le menti pensanti dell’azienda decidono di dare un’ulteriore accelerazione agli obiettivi
            societari puntando a rafforzare la Società tramite lo sviluppo della <strong>Rete Franchising</strong>.</p>
            
            <p><mark class="color">1985</mark> Iniziano i riconoscimenti ufficiali: <strong>David d’Oro</strong> e <strong>Stemma d’oro Italia Regioni</strong>.</p>
            
            <p><mark class="color">1985</mark> Italiana Immobiliare è fra le prime società della regione ad adottare un <strong>innovativo sistema informatico predisposto</strong> per gestire completamente l’intermediazione immobiliare.</p>
            
            <p><mark class="color">1980</mark> Nasce il marchio “<strong>Italiana Immobiliare</strong>” a <strong>Firenze</strong>.</p>
            
            
                        

			<div style="height:100px;"></div>
		</div>
	</div>
</div>


<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>



</div>
<!-- Wrapper / End -->

				

</body>
</html>