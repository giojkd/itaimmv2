<?php

define('SITE_URL', 'https://www.italianaimmobiliare.it');
define('IMG_URL', 'https://itimgest.com');
define('ADMIN_PATH', '/itaimm_admin');
define('ADMIN_PATH_AGENCY', '/myagency');
define('ADMIN_ROOT', $_SERVER['DOCUMENT_ROOT'] . ADMIN_PATH);
define('ADMIN_ROOT_AGENCY', $_SERVER['DOCUMENT_ROOT'] . ADMIN_PATH_AGENCY);
define('SITE_NAME', 'ItalianaImmobiliare.it');
define('ROOT_ADMIN_PATH','/itaimm_admin');
define('ITIMGEST_URL','https://itimgest.com');
define('IMG_PREFIX','https://www.italianaimmobiliare.it/tmpl/img/offerte/');

$cfg['phone_number'] = "800 10 40 88";

$estate_type['A'] = "Affitto";
$estate_type['V'] = "Vendita";

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$ZOOM_LEVELS['1'] = 17;
$ZOOM_LEVELS['1.5'] = 16;
$ZOOM_LEVELS['2'] = 15;
$ZOOM_LEVELS['2.5'] = 14;
$ZOOM_LEVELS['3'] = 13;
$ZOOM_LEVELS['3.5'] = 15;
$ZOOM_LEVELS['20'] = 17;

$uri = $_SERVER['REQUEST_URI'];

$DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['h'] = "390";
$DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['w'] = "390";
$DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['ac'] = "1,1";

$DEFAULT_PRODUCT_PAGE_PHOTO_SIZE_STRING = "?h=" . $DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['h'] . "&w=" . $DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['w'] . "&ac=" . $DEFAULT_PRODUCT_PAGE_PHOTO_SIZE['ac'];

$DEFAULT_FROM = "richieste@italianaimmobiliare.it";
$DEFAULT_TO = "richieste@italianaimmobiliare.it";
$GRAFICA = "grafica@italianaimmobiliare.it";

$extra_agencies['BAR']['off_id'] = 100;
$extra_agencies['BAR']['off_sigla'] = 'BAR';
$extra_agencies['BAR']['off_descrizione'] = 'Baracchini';
$extra_agencies['BAR']['off_ragione_sociale'] = 'Prunecchi SRL';
$extra_agencies['BAR']['off_cap'] = '50127';
$extra_agencies['BAR']['off_telefono'] = '055 9751115';
$extra_agencies['BAR']['off_fax'] = '055 9751118';
$extra_agencies['BAR']['off_email'] = 'novoli@italianaimmobiliare.it';
$extra_agencies['BAR']['off_indirizzo'] = "Via Baracchini, 35";
$extra_agencies['BAR']['force_agency'] = "D";

$extra_agencies['VG']['off_id'] = 100;
$extra_agencies['VG']['off_sigla'] = 'VG';
$extra_agencies['VG']['off_descrizione'] = 'Immobilcavour (Via Gioberti)';
$extra_agencies['VG']['off_ragione_sociale'] = 'Immobilcavour SRL';
$extra_agencies['VG']['off_cap'] = '50121';
$extra_agencies['VG']['off_telefono'] = '055 9751125';
$extra_agencies['VG']['off_fax'] = '055 9751128';
$extra_agencies['VG']['off_email'] = 'gioberti@italianaimmobiliare.it';
$extra_agencies['VG']['off_indirizzo'] = "Via Gioberti, 105/r";
$extra_agencies['VG']['force_agency'] = "A";

$extra_agencies['G']['off_id'] = 100;
$extra_agencies['G']['off_sigla'] = 'G';
$extra_agencies['G']['off_descrizione'] = 'SESTO INVESTIMENTI';
$extra_agencies['G']['off_ragione_sociale'] = 'Sesto Investimenti SRL';
$extra_agencies['G']['off_cap'] = '50019';
$extra_agencies['G']['off_telefono'] = '055 440446';
$extra_agencies['G']['off_fax'] = '055 442677';
$extra_agencies['G']['off_email'] = 'sesto@italianaimmobiliare.it';
$extra_agencies['G']['off_indirizzo'] = "Via D. Alighieri 39/43";

$extra_agencies['J']['off_id'] = 100;
$extra_agencies['J']['off_sigla'] = 'J';
$extra_agencies['J']['off_descrizione'] = 'PRATO INVESTIMENTI';
$extra_agencies['J']['off_ragione_sociale'] = 'Prato Investimenti SRL';
$extra_agencies['J']['off_cap'] = '59100';
$extra_agencies['J']['off_telefono'] = '0574 29151 ';
$extra_agencies['J']['off_fax'] = '0574 29151';
$extra_agencies['J']['off_email'] = 'pratocentro@italianaimmobiliare.it';
$extra_agencies['J']['off_indirizzo'] = "Via della Sirena, 18";
//$extra_agencies['G']['force_agency'] = "A";

$extra_agencies['A']['off_id'] = 100;
$extra_agencies['A']['off_sigla'] = 'A';
$extra_agencies['A']['off_descrizione'] = 'Immobilcavour';
$extra_agencies['A']['off_ragione_sociale'] = 'Immobilcavour srl';
$extra_agencies['A']['off_cap'] = '0000';
$extra_agencies['A']['off_telefono'] = '055 5048011';
$extra_agencies['A']['off_fax'] = '055 576754';
$extra_agencies['A']['off_email'] = 'immobilcavour@italianaimmobiliare.it';
$extra_agencies['A']['off_indirizzo'] = "Viale Matteotti 12/r";

$extra_agencies['B']['off_id'] = 100;
$extra_agencies['B']['off_sigla'] = 'B';
$extra_agencies['B']['off_descrizione'] = 'Immobilstatuto';
$extra_agencies['B']['off_ragione_sociale'] = 'Immobilstatuto';
$extra_agencies['B']['off_cap'] = '0000';
$extra_agencies['B']['off_telefono'] = '055 4369593';
$extra_agencies['B']['off_fax'] = '055 4368947';
$extra_agencies['B']['off_email'] = 'statuo@italianaimmobiliare.it';
$extra_agencies['B']['off_indirizzo'] = "Piazza Dalmazia 35/36r";

$extra_agencies['M']['off_id'] = 100;
$extra_agencies['M']['off_sigla'] = 'M';
$extra_agencies['M']['off_descrizione'] = 'Immobilcampi';
$extra_agencies['M']['off_ragione_sociale'] = 'Immobilcampi';
$extra_agencies['M']['off_cap'] = '0000';
$extra_agencies['M']['off_telefono'] = '055 8979688';
$extra_agencies['M']['off_fax'] = '055 8979672';
$extra_agencies['M']['off_email'] = 'campi@italianaimmobiliare.it.it';
$extra_agencies['M']['off_indirizzo'] = "Via Buozzi, 71";

$extra_agencies_array = array('BAR','VG','G','J','A','B','M');



$default_lang = 'it';

$default_email_notification_cfg['fromname'] = "ItalianaImmobiliare.it";
$default_email_notification_cfg['from'] = "italianaimmobiliareweb@gmail.com";

$guest_id_cookie_name = 'guest_id4';

$sendgrid_cfg['user'] = 'amtsrl';
$sendgrid_cfg['password'] = 'Fantasea2018';

$sendgrid_cfg['fromname'] = "ItalianaImmobiliare.it";
$sendgrid_cfg['from'] = "italianaimmobiliareweb@gmail.com";

$conversion_path_duration = 60; #in minutes;

$contratto[0]['label'] = "Affitto";
$contratto[0]['value'] = 1;
$contratto[0]['default'] = 1;
$contratto[1]['label'] = "Vendita";
$contratto[1]['value'] = 2;

$ratio_options[] = 1;
$ratio_options[] = 1.5;
$ratio_options[] = 2;
$ratio_options[] = 2.5;
$ratio_options[] = 3;
$ratio_options[] = 3.5;
?>
