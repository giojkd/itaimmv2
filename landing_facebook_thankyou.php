<?
include $_SERVER['DOCUMENT_ROOT'] . '/include/include.inc.php';
include ROOT . '/include/session_setter.inc.php';
?>
<html>
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script type="text/javascript" src="/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="/js/facebook_login.js"></script>
        <link href="/css/lightbox.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="/include/font-awesome/css/font-awesome.min.css">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <? include ROOT . '/include/bootstrap.inc.php' ?>
        <title>Grazie per la tua richiesta!</title>
        <style>
            #facebookLandingFooter{
                background-color:#333;
                color:#fff;
                text-align: center;
                padding:20px;
                margin-top:20px;
            }
        </style>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-57193554-1', 'auto');
            ga('send', 'pageview');
            ga("send", "event", "button", "click", "<?= $_GET['landing_type'] ?>");
        </script>
    </head>
    <body>
        <? include ROOT . '/navbars/navbarFacebookLanding.php' ?>
        <? height_spacer(80) ?>
        <div class="container">
            <div class="jumbotron">
                <h1>Grazie!</h1> 
                <p>Abbiamo ricevuto la tua richiesta! Presto l'agenzia incaricata ti ricontatterà!</p>
                <p><a href="<?= SITE_URL ?>" class="btn btn-primary btn-lg"><i class="fa fa-home"></i> Vai al sito</a></p>
            </div> 
        </div>
        <? #include ROOT . '/include/footer.inc.php' ?>
        <div id="facebookLandingFooter">
            Italiana Immobiliare S.p.a. franchising immobiliare - PIVA: 03033690482 - Tutti i diritti riservati - Sitemap | u3920703
        </div>
        <? #cookies_alert() ?>
    </body>
</html>