<?include 'include/web_session_setter.inc.php';?>

<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Appartamenti e case in vendita e affitto a Firenze</title>
<meta name="description" content="Appartamenti e case in vendita e in affitto a Firenze. Affittare, comprare o vendere casa a Firenze è facile. Realizziamo i sogni degli italiani."/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Compare Properties Widget
================================================== -->
<?/*include 'include/compare.inc.php'; */?>

<!-- Compare Properties Widget / End -->


<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->


<!-- Banner
================================================== -->
<div class="parallax" data-background="imgs/slider/unclickeseiacasa.jpg" data-color="#36383e" data-color-opacity="0.15" data-img-width="2500" data-img-height="auto">
	<div class="parallax-content">

		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<!-- Main Search Container -->
					<div class="main-search-container">
						<h2>Un click e sei a casa!</h2>

						<!-- Main Search -->
						<?include 'include/search_home_essential.inc.php'; ?>
						<!-- Main Search -->

					</div>
					<!-- Main Search Container / End -->

				</div>
			</div>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container" id="estatesList">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline margin-bottom-25 margin-top-65 text-center">Ultimi arrivi</h3>
		</div>

		<!-- Carousel -->
		<div class="col-md-12">
			<?include 'include/estetes_column_list.inc.php'; ?>
		</div>
		<!-- Carousel / End -->

	</div>

</div>
<!-- ##########    -->



<!-- Fullwidth Section -->
<section class="fullwidth margin-top-105" data-background-color="#f7f7f7">

	<!-- Box Headline
	<h3 class="headline-box"></h3>
	-->

	<!-- Content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3>Vuoi sapere quanto vale la tua casa?</h3>
				<br>
				<form role="form" action="https://www.itimgest.com/api/web_send_request.php" class="ajax-form" method="post">
                    <input type="hidden" name="op" value="contact_request">
                    <input type="hidden" name="contact_reason" value="vendo affitto">
                    <div class="form-group">
                        <input type="text" class="form-control" name="client_name" placeholder="Nome e cognome" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="client_email" placeholder="La tua email" required>
                    </div>
                    <div class="form-group">
                        <div id="zonesList2" style='background-color: #fff;border: 1px solid #e0e0e0;'>
                          	<v-select v-model="selectedZone" :options="zonelisted" placeholder="Zona dell'immobile" class="full-width"></v-select>
                          	 <input name="client_zona" type="hidden" v-model="selectedZone.value" >
                          	 <input type="hidden" v-model="selectedZone.value" name="client_ISTAT">
                          	 <input type="hidden" v-model="selectedZone.area_type" name="client_area_type">
                          	 <input type="hidden" v-model="selectedZone.label" name="client_zona">
                             <input type="hidden" name="client_area_id" value="Tutte">
                        </div>
                    </div>
                    <div class="form-group">
                       <select name="client_contratto">
                          <option value="Vendo" selected>Vendo</option>
                          <option value="Affitto">Affitto</option>
                        </select>
                    </div>

                    <div class="checkbox col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <label style="font-size:12px;">
                           <input type="checkbox" name="privacy" id="check_privacy" style="width:30px; height: auto;"> Accetto le condizioni di <a href="https://www.italianaimmobiliare.it/contenuti/privacy-policy-termini-e-condizioni-italiana-immobiliare-85.html">Privacy e Policy</a>
                        </label>
                    </div>

                    <div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12 text-right">
                        <button type="submit" class="button">Contattami</button>
                    </div>
                    <div class="col-md-12 alert fade alert-success" style="display:none">
                        <span class="message-value"></span>
                    </div>
                    <div class="col-md-12 alert fade alert-danger" style="display:none">
                        <span class="message-value"></span>
                    </div>
                </form>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 text-center">
				<img src="imgs/vendi-affitta-con-italiana-immobiliare-v2.jpg" class="img-flex" />
			</div>

		</div>
	</div>
</section>
<!-- Fullwidth Section / End -->


<!-- Fullwidth Section -->
<section class="fullwidth margin-top-105" data-background-color="#ffffff" style="margin-top:-50px!important; border-top:1px solid #cccccc">

	<!-- Box Headline
	<h3 class="headline-box"></h3>
	 -->
	<!-- Content -->
	<div class="container">
		<div class="row">

			<div class="col-lg-6 col-md-6 col-sm-6 text-center">
				<img src="imgs/lavora-con-noi-italiana-immobiliare.jpg" class="img-flex" />
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6">
    			<h3>Lavora con noi! Il successo è garantito</h3>
    			<br>
				<form role="form" action="https://www.itimgest.com/api/web_send_request.php" class="ajax-form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="op" value="work_with_us_request_V2">
                    <input type="hidden" name="contact_reason" value="vendo affitto">
                    <input type="hidden" name="landing_type" value="">
                    <input type="hidden" name="curriculum_path" id="curriculum-path">
                    <div class="form-group">
                        <input type="text" class="form-control" name="recruit_email" placeholder="La tua email" required>
                    </div>
                    <div class="form-group">
                        <input type="file" class="form-control" id="curriculum" name="recruit_cv" placeholder="File in PDF" required="required" style="padding: 10px;height: auto;">
                        <div class="input-group-text" style="font-size:14px;">Caricare un file PDF <i class="fa fa-file-pdf-o"></i></div>
                    </div>
                    <div class="checkbox">
                        <label style="font-size:12px;">
                           <input type="checkbox" name="privacy" id="check_privacy" style="width:30px; height: auto;">
                           <label style="font-size:12px;" class="form-check-label col-lg-11" for="privacy">Compilando questo modulo autorizzo il trattamento e la gestione dei dati immessi secondo quanto stabilisce il nuovo Regolamento Europeo (GDPR n. 679/2016) in materia di privacy e protezione dati personali.<br>
                           Per informazioni <a href="privacy-policy-termini-e-condizioni.php" target="_blank">Consulta la nostra privacy policy</a> prima di inviare</label>
                        </label>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                        <button type="submit" class="button">Invia la tua candidatura</button>
                    </div>
                    <div class="col-md-12 alert fade alert-success" style="display:none">
                        <span class="message-value"></span>
                    </div>
                    <div class="col-md-12 alert fade alert-danger" style="display:none">
                        <span class="message-value"></span>
                    </div>
                </form>
			</div>



		</div>
	</div>
</section>
<!-- Fullwidth Section / End -->



<!-- Container -->
<div class="container" id="estateTotZone">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-bottom-35 margin-top-10">Immobili in vendita<span> Selezioniamo gli immobili più interessanti in tutta la Toscana</span></h3>
		</div>
		<div v-for="(item,index) in zoneTotList" v-bind:class="(index > 10) ? 'col-md-2' : classesArray[index]">
			<!-- Image Box -->
			<a v-bind:href="'searchresult.php#?filters[area_type_and_id][0][ISTAT]=' + item.ISTAT + '&amp;filters[area_type_and_id][0][area_id]=Tutte&amp;filters[area_type_and_id][0][area_type]=' + item.area_type + '&amp;filters[equal][Contratto]=V'" class="img-box" data-background-image="images/popular-location-01.jpg">
				<div class="img-box-content visible">
					<h4>{{ item.name }}</h4>
					<span>{{ item.tot_estates }} Immobili</span>
				</div>
			</a>
		</div>
		<div class="col-md-2">
			<!-- Image Box -->
			<a href="searchresult.php?filters[equal][Contratto]=V" class="img-box" data-background-image="images/popular-location-01.jpg">
				<div class="img-box-content visible">
					<h4>ALTRI</h4>
					<span>in Toscana<br></span>
				</div>
			</a>
		</div>


	</div>
</div>
<!-- Container / End -->


<!-- Flip banner -->
<!--
<a href="listings-half-map-grid-standard.html" class="flip-banner parallax" data-background="images/flip-banner-bg.jpg" data-color="#911938" data-color-opacity="0.9" data-img-width="2500" data-img-height="1600">
	<div class="flip-banner-content">
		<h2 class="flip-visible">Vuoi vendere o affittare un immobile?</h2>
		<h2 class="flip-hidden">Inserisci il tuo annuncio <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
 -->
<!-- Flip banner / End -->




<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>

<!--  ITALIANA IMMOBILIARE  -->
<script type="text/javascript" src="scripts/italianaimmobiliare.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>


</div>
<!-- Wrapper / End -->



</body>
</html>
