<?
include ROOT . '/sample_content/homepage_incontra_gli_agenti.sample.php';

$agents = qa("SELECT a.agent_name,a.agent_surname,a.agency_id,aa.agency_name, aa.agency_logo_path,
(SELECT formats FROM files f WHERE f.file_id = a.file_id) as avatar 
FROM agents a LEFT JOIN agents_profiles ap USING(agent_profile_id) INNER JOIN agency aa USING(agency_id)
WHERE a.agent_profile_id = 1 AND
 a.agency_id <> 8 AND
 a.group_id = 1 AND
 a.is_titolare = 1 AND
 a.agent_status >= 0", 1);

$c = 0;
$count = 0;
foreach ($agents as $index => $agent) {
    $agent['agent_excerpt'] = "Visita la mia agenzia!";
    $agent_groups[$c][] = $agent;
    if (($count + 1) % 4 == 0) {
        $c++;
    }
    $count++;
}
?>

<div class="container" id="incontra_gli_agenti">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>
                Incontra i nostri professionisti
            </h1>
            <h2>
                Più di 150 addetti, tutti professionisti del settore, sempre a tua disposizione, una risorsa inestimabile per scegliere la tua nuova casa.
            </h2>
            <div class='height-spacer-25'></div>
            <div id="carousel-agenti-hompage" class="carousel slide" data-ride="carousel" data-interval="false">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-agenti-hompage" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-agenti-hompage" data-slide-to="1"></li>
                    <li data-target="#carousel-agenti-hompage" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <?
                    $c = 0;
                    foreach ($agent_groups as $group) {
                        ?>
                        <div class="item <?= ($c == 0) ? 'active' : '' ?>">
                            <div class='row'>
                                <?
                                foreach ($group as $agent) {
                                    $avatar = json_decode($agent['avatar'], 1);
                                    $agency_cover = "/agenzie/" . $agent['agency_logo_path'];
                                    //print_rr($agent);
                                        ?>
                                        <div class='col-md-3 text-center'>
                                            <div class='homepage-agent' onclick="location.href = '/searchresult.php?filters[equal][e.agency_id]=<?=$agent['agency_id']?>'">
                                                <div class='agent-image'>
                                                    <img height="210" alt="Agente immobiliare <?= $agent['agent_name'] ?> <?= $agent['agent_surname'] ?>" src='<?=  (($avatar['x400'] != '') ? ITIMGEST_URL . $avatar['x400'] : IMG_URL . $agency_cover)   ?>' class='img-circle'>
                                                </div>
                                                <div class='agency-image'>
                                                    <img height="210" alt="Agenzia immobiliare <?= $agent['agency_name'] ?>"  src='<?= "https://www.italianaimmobiliare.it" . $agency_cover ?>' class='img-circle'>
                                                    <div class="agency-image-details">
                                                        <div class='label_type rent'>
                                                            <?= $agent['agency_name'] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class ='agent-name'>
                                                    <?= $agent['agent_name'] ?>
                                                </p>
                                            </div>

                                        </div>
                                    <?
                                    
                                }
                                ?>
                            </div>
                        </div>
                        <?
                        $c++;
                    }
                    ?>

                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-agenti-hompage" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-agenti-hompage" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
            <div class='height-spacer-25'></div>
            <!--<a class='btn btn-default'>Tutti gli agenti</a>-->
        </div>
    </div>
    <div class='height-spacer-25'></div>
</div>

