<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Il gruppo | Italiana Immobiliare</title>
<meta name="description" content="Appartamenti e case in vendita e in affitto a Firenze. Affittare, comprare o vendere casa a Firenze è facile. Realizziamo i sogni degli italiani."/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>
<!-- Banner
================================================== -->
<div class="container-fluid p-0">
	<div class="row">
		<div class="col-md-12">
			<img src="imgs/italianaimmobiliare_il_gruppo.jpg" alt="Il Gruppo Italiana Immobiliare" class="img-flex" />			
		</div>
	</div>
</div>



<div class="container">
	<div class="row">
		<div class="col-md-12 contenuto">
			
            <h2 class="text-center headline with-border margin-top-45 margin-bottom-25">Il Gruppo Italiana Immobiliare</h2>
            
            <p class="text-center">Il gruppo Italiana Immobiliare S.p.A. è costituito da quattro sezioni, create appositamente per migliorare il servizio fornito al cliente finale.</p>
            
            
            <img src="imgs/italianaimmobiliare_il-gruppo-struttura.jpg" alt="Gruppo Italiana Immobiliare" class="img-flex" />	
            
            <div style="height:20px;"></div>
            
            <div class="row">
            	<div class="col-md-1"><img src="imgs/itimacademy.png" alt="Itim Academy" /></div>
            	<div class="col-md-11">
					<p>Reparto gestione risorse umane e formazione. Offre un professionale e mirato servizio di recruiting del personale lavorativo e struttura il calendario dei corsi di formazione rivolti alle nuove risorse. Periodicamente organizza riunioni di aggiornamento suddivise per categorie lavorative.
					 Italiana Immobiliare S.p.A. crede da sempre nell’importanza dell’educazione immobiliare e nella costanza del sostegno ai propri professionisti.</p>
				</div>
            </div>
            
            <div class="row">
            	<div class="col-md-1"><img src="imgs/itimgest.png" alt="Itim Gest" /></div>
            	<div class="col-md-11">
					<p>Reparto sviluppo gestionale “Itimgest”. Implementa lo sviluppo di una potente piattaforma tecnologica che permette costantemente una gestione immobiliare completa ed esaustiva articolata su tre livelli: gestione immobile/cliente, diffusione inserzioni/campagne pubblicitarie, monitoraggio operato risorse umane.</p>
				</div>
            </div>
                        
            <div class="row">
            	<div class="col-md-1"><img src="imgs/itimmedia.png" alt="Itim Media" /></div>
            	<div class="col-md-11">
					<p>Reparto comunicazione e pubblicità. Dal forte orientamento creativo e comunicativo, realizza quotidianamente campagne pubblicitarie e progetti di comunicazione integrata volti a diffondere l’identità aziendale. Organizza, inoltre, eventi quali inaugurazioni, openhouse, celebrazioni aziendali, meeting, e dunque cura tutta la visibilità del brand Italiana Immobiliare.</p>
				</div>
            </div>
            
            <div class="row">
            	<div class="col-md-1"><img src="imgs/itimweb.png" alt="Itim Web" /></div>
            	<div class="col-md-11">
					<p>Reparto sviluppo web e applicazioni. Rivolto principalmente all’aspetto web e social media marketing, in continua evoluzione, sviluppa progetti web e campagne social media basati sul coinvolgimento emotivo del cliente (user experience). Presta grande attenzione all’andamente tecnologico e alle interazioni tra utente e dispositivi informatici cercando di soddisfare al meglio sia le esigenze degli operatori che quelle dell’utente finale.</p>
				</div>
            </div>            
			
			
			

			<div style="height:100px;"></div>
		</div>
	</div>
</div>


<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>



</div>
<!-- Wrapper / End -->

				

</body>
</html>