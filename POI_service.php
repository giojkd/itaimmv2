<script>
jQuery.each(window.mapMarkers, function() {
    window.map.removeLayer(this);
});
</script>
<?php


/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/


function print_rr($array){
  echo '<pre>';
  print_r($array);
  echo '</pre>';
}

require_once 'Services/OpenStreetMap.php';

const KILOMETERS = 6372.8;
const MILES = 3963.1676;
const NAUTUCAL_MILES = 3443.89849;


/**
 * Given two sets of lat/lon pairs, calculate the distance between them
 *
 * Distance defaults to being calculated in kilometers.
 *
 * @param mixed $aLon
 * @param mixed $aLat
 * @param mixed $bLon
 * @param mixed $bLat
 * @param mixed $unit
 *
 * @return float
 */
function calculateDistance($aLon, $aLat, $bLon, $bLat, $unit = KILOMETERS)
{
    $sinHalfDeltaLat = sin(deg2rad($bLat - $aLat) / 2.0);
    $sinHalfDeltaLon = sin(deg2rad($bLon - $aLon) / 2.0);
    $lonSqrd = $sinHalfDeltaLon * $sinHalfDeltaLon;
    $latSqrd = $sinHalfDeltaLat * $sinHalfDeltaLat;
    $angle = 2 * asin(
        sqrt($latSqrd + cos(deg2rad($aLat)) * cos(deg2rad($bLat)) * $lonSqrd)
    );
    return $unit * $angle;
}

/**
 * sortByDistance
 *
 * @return function
 */
function sortByDistance()
{
    return function ($a, $b) {
        if ($a->distance == $b->distance) {
            return 0;
        }
        return ($a->distance < $b->distance) ? -1 : 1;
    };
}

$lat = null;
$lon = null;
$k = null;
$v = null;

if (isset($_GET['q'])) {
    $v = strpos($_GET['q'], '|');
    if ($v != false) {
        list($k, $v) = explode('|', $_GET['q']);
        $k = trim($k);
        $v = trim($v);
    } else {
        die();
    }
}

if (isset($_GET['lat'])) {
    $lat = $_GET['lat'];
}
if (isset($_GET['lat'])) {
    $lon = $_GET['lon'];
}

//$km = 5;
//$earthRadius = 40075;
//$degree = 360;

//echo 'lat = ' . $lat . '<br/>';
//echo 'lon = ' . $lon . '<br/>';

//$latDist=  $km * ($degree/$earthRadius);

$lat1= $lat - 0.012;
$lat2= $lat + 0.012;

//$lngLine = cos($lon)*40075;
//$lngDist = 1 * ($degree/$lngLine);

$lng1 = $lon - 0.012;
$lng2 = $lon + 0.012;

//echo '$latDist = ' . $latDist . '<br/>';
//echo '$lngLine = ' . $lngLine . '<br/>';
//echo '$lngDist = ' . $lngDist . '<br/>';

//echo '$lat1 = ' . $lat1 . '<br/>';
//echo '$lat2 = ' . $lat2 . '<br/>';
//echo '$lng1 = ' . $lng1 . '<br/>';
//echo '$lng2 = ' . $lng2 . '<br/>';


$osm = new Services_OpenStreetMap();

//$osm->get(35.958834, 47.617818 , 35.979584, 47.997043);
//$osm->get(-7.8564758, 52.821022799999994, -7.7330017, 53.0428644);

$osm->get($lng1,$lat1,$lng2,$lat2);
$osm->getXml();

//file_put_contents("area_covered.osm", $osm->getXml());

//$osm->loadXML("area_covered.osm");

$results = $osm->search(array($k => $v));

$oh = new Services_OpenStreetMap_OpeningHours();


foreach ($results as $result) {
    if ($result->getType() == 'node') {
        $bLat = $result->getLat();
        $bLon = $result->getLon();
    } elseif ($result->getType() == 'way' && $result->isClosed()) {
        $nodes = $result->getNodes();
        array_pop($nodes);
        $bLat = 0;
        $bLon = 0;
        foreach ($nodes as $node) {
            $n = $osm->getNode($node);
            if ($n !== false) {
                $bLat += $n->getLat();
                $bLon += $n->getLon();
            }
        }
        $bLat = $bLat / sizeof($nodes);
        $bLon = $bLon / sizeof($nodes);
    }
    $distance = calculateDistance($lat, $lon, $bLat, $bLon);
    // $distance = $distance * 1000; // convert to metres
    $result->distance = $distance;
    $result->lat = $bLat;
    $result->lon = $bLon;
}

usort($results, sortByDistance());

echo "<p>Risultati nel raggio di 1.7 Km:". count($results) . "</p>";
echo "<hr/>";
?>

<ol class="rounded-list" style="padding-left:15px;padding-right:15px;">
<?
$rCount=1;
foreach ($results as $result) {
?>
<li>
  <?
    $tags = $result->getTags();
    $name = $tags['name'];
    $addrStreet = $tags['addr:street'];
    $addrCity = $tags['addr:city'];
    $addrCountry = $tags['addr:country'];
    $addrHouseName = $tags['addr:housename'];
    $addrHouseNumber = $tags['addr:housenumber'];
    $openingHours = $tags['opening_hours'];
    $phone = $tags['phone'];
    $bLat = $result->lat;
    $bLon = $result->lon;
    $oh->setValue($openingHours);
    $open = $oh->isOpen();

    $line1 = ($addrHouseNumber) ? $addrHouseNumber : $addrHouseName;
    if ($line1 != null) {
        $line1 .= ', ';
    }
    echo  "<p><b>$name</b><p>";
    $distance = $result->distance;

    if ($line1 != null && $addrStreet != null) {
        echo "<p><b>{$line1}{$addrStreet}</b></p>";
    }
    if ($phone != null) {
        echo "<p><b>Telefono:</b><a href='tel:$phone'>$phone</a></p>";
    }
    if ($openingHours !== null) {
        echo  "<p><b>Orari:</b> $openingHours</p>";
        echo "<p><b>Aperto:</b> ";
        if ($open === null) {
            echo "?\n";
        } elseif ($open == true) {
            echo "Si\n";
        } else {
            echo "No\n";
        }
        echo "</p>";
    }
    echo "<p><b>Distanza:</b> ", number_format($distance, 4), "km</p>";
    echo "<script>
        var marker = window.L.marker([",$bLat  ,", ",$bLon," ], {icon: new L.DivIcon({
                className: 'POIicon',
                html: '<span>$rCount</span>'
        })});
        marker.addTo(window.map);
    ";
    echo "marker.bindPopup(\"<b>", htmlspecialchars($name), "</b>\");";
    echo "window.mapMarkers.push(marker);";
    echo "</script>";
    ?>
  </li>
  <?
  $rCount++;
}
?>
</ol>
<?

$result = $results[0];
$bLat = $result->lat;
$bLon = $result->lon;
echo "<script>";
echo "window.map.panTo([",$bLat,", ",$bLon," ]);";
echo "</script>";
