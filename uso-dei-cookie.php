<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Uso dei cookie | Italiana Immobiliare</title>
<meta name="description" content="Informazioni, ai sensi dell’artt. 12, 13, 14 del Regolamento Privacy Europeo N. 2016/679"/>

<?include 'include/top.inc.php'; ?>

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>
<!-- Banner
================================================== -->


<div class="container">
	<div class="row">
		<div class="col-md-12 contenuto">
			
            
            <h1>Uso dei cookie</h1>
            
            <script id="CookieDeclaration" src="https://consent.cookiebot.com/ca07d4b0-7d94-41a1-b497-edb2a004ef47/cd.js" type="text/javascript" async></script>

            
            <div style="height:100px;"></div>
		</div>
	</div>
</div>

<!-- Footer
================================================== -->
<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>



</div>
<!-- Wrapper / End -->

				

</body>
</html>