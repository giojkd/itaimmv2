<?php
include $_SERVER['DOCUMENT_ROOT'] . '/include/include.inc.php';
include ROOT . '/classes/php-spam-filter-master/spamfilter.php';
$op = $_REQUEST['op'];
//$data_post = array_map('trim', $_POST);
$data_post = $_POST;
$p = $_POST;

switch ($op) {
    case 'affittare_casa_landing':
        if ($p['form_data']['tel'] != '') {
//            print_rr($p);
            $recepients = array('lorenzocoppoli@gmail.com', 'italianaimmobiliareweb@gmail.com', 'raveggimartin@gmail.com', 'immobilcavour@italianaimmobiliare.it');
//          
            $p['form_data']['date'] = date("H:i d/m/Y");
            save_array_in_db3($p['form_data'], 'landing_rent_result');
            $notify = load_notify('landing_rent', 'it', $p['form_data']);
            foreach ($recepients as $recepient) {
                $result = json_decode(sendgrid($recepient, "Nuovo immobile da affittare", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            }
            header('location:' . SITE_URL . '/affittare-casa-grazie.php');
            exit;
        } else {
            header('location:' . SITE_URL . '/affittare-casa.php?error=tel');
        }
        break;

    case 'map_agency':
        replace_array_in_db3($p['form_data'], 'agenciesMapper');
        $r = new stdClass();
        $r->status = 1;
        echo json_encode($r);
        break;

    case 'map_zone':
        replace_array_in_db3($p['form_data'], 'zonesMapper');
        $r = new stdClass();
        $r->status = 1;
        echo json_encode($r);
        break;



    case 'newFacebookLandingRequest':
        if ($p['form_data']['tel'] != '') {

            function fixStringForMySql($str) {
                return addslashes(stripslashes($str));
            }

            function wrapWithQuotes($str) {
                return '"' . $str . '"';
            }

            $recepients = array('lorenzocoppoli@gmail.com', 'italianaimmobiliareweb@gmail.com', 'raveggimartin@gmail.com');

            foreach ($p['form_data']['zone'] as $zone) {
                $zone_tmp = explode('_', $zone);
                $zone_id[] = $zone_tmp[0];
            }
            $imploded_zone_id = implode(',', array_map('wrapWithQuotes', $zone_id));

            $p['form_data']['zone'] = array_filter($p['form_data']['zone']);
            $p['form_data']['zone'] = implode(',', array_map('wrapWithQuotes', array_map('fixStringForMySql', $p['form_data']['zone'])));

            if ($imploded_zone_id == 'Tutte') {
                
                $sql_agency = "SELECT AG.agency_id as id, AG.agency_email as email 
                            FROM agency AS AG GROUP BY email";                
                
                $agencies = qa3($sql_agency);
                //$agencies = qa3("SELECT a.off_email as email FROM aaa_agenzie a LEFT JOIN agenciesMapper am USING(off_id) GROUP BY off_id");
                
            } else {
                
                $sql_agency = "SELECT AG.agency_id as id, AG.agency_email as email 
                            FROM agency AS AG
                            LEFT JOIN agenciesMapper AS AM ON AM.off_id=AG.o_agency_id
                            LEFT JOIN aaa_zonesMapper AS ZM ON ZM.zon_id = AM.zon_id
                            LEFT JOIN immobiliareit_quartiere AS IQ ON IQ.block_id=ZM.zone_id
                            WHERE IQ.block_id IN({$imploded_zone_id}) GROUP BY email";  
                $agencies = qa3($sql_agency,1);           
                //$agencies = qa3("SELECT a.off_email as email FROM aaa_agenzie a LEFT JOIN agenciesMapper am USING(off_id) WHERE am.zon_id IN({$imploded_zone_id}) GROUP BY off_id", 1);
            }

            foreach ($agencies as $agency) {
                $recepients[] = $agency['email'];
                $agency_id[] = $agency['id'];
            }

            if (count($p['form_data']['vano']) > 0) {
                $p['form_data']['vano'] = implode(',', $p['form_data']['vano']);
            }


            $p['form_data']['recipients'] = implode(',', $recepients);
            
            $p['form_data']['agency_id'] = implode(',', $agency_id);

            $p['form_data']['post_json'] = json_encode($p);
            
            
            save_array_in_db3($p['form_data'], 'facebook_requests');

            $p['form_data']['date'] = date('d/m/Y H:i:s');

            $notify = load_notify('landing_page_request', 'it', $p['form_data']);
            foreach ($recepients as $recepient) {
                //$result = json_decode(sendgrid($recepient, "Richiesta Immobile Landing Page", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            }

            header('location:' . SITE_URL . '/landing_facebook_thankyou.php?landing_type=' . $p['landing_type']);
        } else {
            header('location:' . SITE_URL . '/landing_facebook.php?error=tel&ISTAT=' . $p['ISTAT']);
        }
        break;
        
    //CAMPAGNA PRESTIGE | AGOSTO 2018
    
    case 'PrestigeLandingRequest':
        if ($p['form_data']['tel'] != '') {
            
            function fixStringForMySql($str) {
                return addslashes(stripslashes($str));
            }
            
            function wrapWithQuotes($str) {
                return '"' . $str . '"';
            }
            
            $recepients = array('lorenzocoppoli@gmail.com', 'italianaimmobiliareweb@gmail.com', 'marketing@amtitalia.com');
            #$recepients = array('raveggimartin@gmail.com', 'design@amtitalia.com');
            
            $zone_id = array();
            
            foreach ($p['form_data']['zone'] as $zone) {
                $zone_tmp = explode('_', $zone);
                $zone_id[] = $zone_tmp[0];
            }
            $imploded_zone_id = implode(',', array_map('wrapWithQuotes', $zone_id));
            
            $p['form_data']['zone'] = array_filter($p['form_data']['zone']);
            $p['form_data']['zone'] = implode(',', array_map('wrapWithQuotes', array_map('fixStringForMySql', $p['form_data']['zone'])));
            
            if ($imploded_zone_id == 'Tutte') {
                
                $sql_agency = "SELECT AG.agency_id as id, AG.agency_email as email
                            FROM agency AS AG GROUP BY email";
                
                $agencies = qa3($sql_agency);                
                
            } else {
                
                $sql_agency = "SELECT AG.agency_id as id, AG.agency_email as email
                            FROM agency AS AG
                            LEFT JOIN agenciesMapper AS AM ON AM.off_id=AG.o_agency_id
                            LEFT JOIN aaa_zonesMapper AS ZM ON ZM.zon_id = AM.zon_id
                            LEFT JOIN immobiliareit_quartiere AS IQ ON IQ.block_id=ZM.zone_id
                            WHERE IQ.block_id IN({$imploded_zone_id}) GROUP BY email";
                $agencies = qa3($sql_agency,1);
            }
            $agency_id = array();
            foreach ($agencies as $agency) {
                $recepients[] = $agency['email'];
                $agency_id[] = $agency['id'];
            }
            
            if (count($p['form_data']['vano']) > 0) {
                $p['form_data']['vano'] = implode(',', $p['form_data']['vano']);
            }
            
            
            $p['form_data']['recipients'] = implode(',', $recepients);
            
            $p['form_data']['agency_id'] = implode(',', $agency_id);
            
            $p['form_data']['post_json'] = json_encode($p);
            
            save_array_in_db3($p['form_data'], 'facebook_requests');
            
            $p['form_data']['date'] = date('d/m/Y H:i:s');
            
            $notify = load_notify('landing_page_request', 'it', $p['form_data']);
            
            $dati = [];
            $dati['html'] = $notify;
            $dati['subject'] = 'Richiesta Immobile Landing Page | PRESTIGE';
            
            foreach ($recepients as $recepient) {
                $dati['to'] = $recepient; 
                $result = json_decode(sendgrid2($dati));
            }
            
            header('location:' . SITE_URL . '/landing_prestige_thankyou.php?landing_type=' . $p['landing_type']);
        } else {
            header('location:' . SITE_URL . '/landing_prestige_desktop.php?error=tel&ISTAT=' . $p['ISTAT']);
        }
        break;
        
    
    //CAMPAGNA CREATA PER borgolacroce@italianaimmobiliare.it | MARZO 2018
    case 'landingRussiaRequest':
        if ($p['form_data']['tel'] != '') {
            
            $p['form_data']['agency_id']=19;

            function fixStringForMySql($str) {
                return addslashes(stripslashes($str));
            }

            function wrapWithQuotes($str) {
                return '"' . $str . '"';
            }

            $recepients = array('lorenzocoppoli@gmail.com', 'italianaimmobiliareweb@gmail.com', 'marketing@amtitalia.com', 'borgolacroce@italianaimmobiliare.it');
            #$recepients = array('raveggimartin@gmail.com', 'design@amtitalia.com');

            $p['form_data']['recipients'] = implode(',', $recepients);
            $p['form_data']['post_json'] = json_encode($p);
            save_array_in_db3($p['form_data'], 'facebook_requests');

            $p['form_data']['date'] = date('d/m/Y H:i:s');

            $notify = load_notify('landing_page_russia_request', 'it', $p['form_data']);
            
            $dati = [];
            $dati['html'] = $notify;
            $dati['subject'] = 'Richiesta Immobile Landing Page | Russia';
            
            foreach ($recepients as $recepient) {
                
                $dati['to'] = $recepient;                
                #$result = json_decode(sendgrid2($recepient, "Richiesta Immobile Landing Page", $notify, strip_tags($notify), $DEFAULT_FROM), 1);                
                $result = json_decode(sendgrid2($dati));
            }

            header('location:' . SITE_URL . '/landing_russia_thankyou.php?landing_type=' . $p['landing_type']);
        } else {
            header('location:' . SITE_URL . '/landing_russia_desktop.php?error=tel&ISTAT=' . $p['ISTAT']);
        }
        break;    
    

    case 'set_session':
        if (count($p['values']) > 0) {
            foreach ($p['values'] as $index => $value) {
                $_SESSION[$index] = $value;
            }
        }
        break;
    case 'disable_modal_pros':
        $_SESSION['modal_pros'] = 'off';
        break;
    case 'register_new_workpro':
        $expert = qa3("SELECT * FROM expert WHERE expert_email LIKE '{$p['expert']['expert_email']}'");
        if (count($expert) == 0) {
            $p['expert'] = array_map('trim', $p['expert']);
            $expert_id = save_array_in_db3($p['expert'], 'expert');
            if (count($p['service_id']) > 0) {
                foreach ($p['service_id'] as $service_id)
                    save_array_in_db3(array('expert_id' => $expert_id, 'service_id' => $service_id), 'expert_services');
            }
            header('location:' . SITE_URL . '/grazie');
        }
        break;

    case 'save_search':
        $_SESSION['guest_search_id'] = save_array_in_db3(array('search' => json_encode($_SESSION['user']['search']), 'saved' => 1, 'guest_email' => $p['guest_email'], 'guest_id' => $_SESSION['guest_id'], 'query_hash' => $p['query_hash'], 'count_results' => $p['count_results'], 'query' => $_SESSION['user']['last_search_query']), 'guest_searchs');
        update_array_db3('guest', array('guest_email' => $p['guest_email']), array('guest_id' => $_SESSION['guest_id']));
        $risp->status = 1;
        $risp->content = "Ricerca salvata con successo!";
        echo json_encode($risp);
        break;

    case 'edit_user_notifications_subscriptions':

        foreach ($p['dem_ids'] as $dem_id => $dem_status) {
            $array['dem_id'] = $dem_id;
            $array['dem_email'] = $p['dem_email'];
            $array['dem_status'] = $dem_status;
            replace_array_in_db3($array, 'dem_subscriptions');
            save_array_in_db3(array('email' => $p['dem_email']), 'forbidden_email');
        }
        $risp = new stdClass();
        $risp->status = 1;
        $risp->content = "Preferenze salvate correttamente";
        echo json_encode($risp);
        break;

    case 'work_with_us_request':
        $request_values['name_and_surname'] = $data_post['name_and_surname'];
        $request_values['email'] = $data_post['email'];
        $request_values['telephone'] = $data_post['telephone'];
        
        $request_values['curriculum_path'] = $data_post['curriculum_path'];
        $notify = load_notify('work_with_us_request', 'it', $request_values);
        if ($request_values['name_and_surname'] != '') {
            
            $dati = [];
            $dati['html'] = $notify;
            $dati['subject'] = 'Curriculum - Lavora con noi';
            $dati['to'] = 'design@amtitalia.com';              
            
            #$first_send = json_decode(sendgrid('simsasso@gmail.com', "Curriculum - Lavora con noi", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            #$second_send = json_decode(sendgrid('recruit@italianaimmobiliare.it', "Curriculum - Lavora con noi", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            
            $first_send = json_decode(sendgrid2($dati), 1);
            
            $dati['to'] = 'recruit@italianaimmobiliare.it';            
            $second_send = json_decode(sendgrid2($dati), 1);
            
        }
        if ($first_send['message'] == "success" && $second_send['message'] == "success") {
            $risp->status = 1;
            $risp->content = "Messaggio inviato con successo!";
        }

        $recruit_name_and_surname = explode(' ', $data_post['name_and_surname']);
        $recruit['recruit_name'] = $recruit_name_and_surname[0];
        $recruit['recruit_surname'] = $recruit_name_and_surname[1];
        $recruit['recruit_telephone'] = $data_post['telephone'];
        $recruit['recruit_email'] = $data_post['email'];
        $recruit['recruit_cv'] = $data_post['curriculum_path'];
        save_array_in_db3($recruit, 'recruits');

        echo json_encode($risp);
        break;
        
    case 'work_with_us_request_V2':        
        
        
         
        $info = pathinfo($_FILES['recruit_cv']['name']);
        
        $ext = $info['extension']; // get the extension of the file     
        $newname = trim($data_post['recruit_name']) . "_" . trim($data_post['recruit_surname']) . "_" . time() . ".".$ext;
        
        $data_post['recruit_cv']='/uploads_static/'.$newname;
        
        $cartella_upload = ROOT . '/uploads_static';
        
        $target = $cartella_upload . '/'.$newname;
        
        if(!is_dir($cartella_upload))
        {
            echo 'La cartella in cui si desidera salvare il file non esiste!';
        }
        // verifichiamo che la cartella di destinazione abbia i permessi di scrittura
        else if(!is_writable($cartella_upload))
        {
            echo "La cartella in cui fare l'upload non ha i permessi!";
        }
        // verifichiamo il successo della procedura di upload nella cartella settata
        //else if(!move_uploaded_file($_FILES["recruit_cv"]["tmp_name"], $cartella_upload . '/' .$_FILES["recruit_cv"]["name"]))
        else if(!move_uploaded_file($_FILES["recruit_cv"]["tmp_name"], $target))
        {
            echo 'Ops qualcosa è andato storto nella procedura di upload!';
        } 
        else
        {
            #move_uploaded_file($_FILES['recruit_cv']['tmp_name'], $target);
            
            $request_values = [];
            $request_values['name_and_surname'] = $data_post['recruit_name'] . ' ' . $data_post['recruit_surname'];
            $request_values['email'] = $data_post['recruit_email'];
            $request_values['telephone'] = $data_post['recruit_telephone'];
            $request_values['curriculum_path'] = $data_post['recruit_cv'];
            
            $notify = load_notify('work_with_us_request', 'it', $request_values);
            
            if ($request_values['name_and_surname'] != '') {
                
                $dati = [];
                $dati['html'] = $notify;
                $dati['subject'] = 'Curriculum - ' . $data_post['landing_type'];
                $dati['to'] = 'design@amtitalia.com';
                
                $first_send = json_decode(sendgrid2($dati), 1);
                
                $dati['to'] = 'recruit@italianaimmobiliare.it';
                $second_send = json_decode(sendgrid2($dati), 1);
                
            }
            
            if ($first_send['message'] == "success" && $second_send['message'] == "success") {
                $risp->status = 1;
                $risp->content = "Messaggio inviato con successo!";
            }
            
            
            
            unset($data_post['op']);
            unset($data_post['curriculum_path']);
            unset($data_post['work_with_us_request_V2']);
            unset($data_post['privacy']);
            
            $risp = save_array_in_db3($data_post, 'recruits');
            
            #echo json_encode($risp);
            header('location:' . SITE_URL . '/landing_lavora-con-noi_thankyou.php?landing_type=' . $data_post['landing_type']);
            
        }
        
        
        break;
        
    case 'set_cookie':
        setcookie($_POST['cookie_name'], $_POST['cookie_value'], time() + (86400 * 30), "/");
        break;
    case 'cookies_alert_off':
        setcookie('cookies_alert_off', 1, time() + (86400 * 30), "/");
        $_SESSION['cookies_alert_off'] = 1;
        break;
    
    case 'save_search':
        print_rr($_POST);
        break;


    case 'get_zones':
        $query_blocks = "SELECT block_id as id, NomeQuartiere as text FROM immobiliareit_quartiere WHERE ISTAT = " . $p['ISTAT'];
        $query_locations = "SELECT location_id as id, NomeLocalita as text FROM immobiliareit_localita WHERE ISTAT = " . $p['ISTAT'];

        $scegli[] = array('id' => '', 'text' => 'Tutte');

        $blocks = qa3($query_blocks, 1);
        $locations = qa3($query_locations, 1);

        if (count($blocks) > 0) {
            $options = $blocks;
            $area_type = 'block';
        } else if (count($locations) > 0) {
            $options = $locations;
            $area_type = 'location';
        }
        if (count($options) > 0) {
            ob_start();
            ?>
            <input type="hidden" name="filters[area_type_and_id][0][area_type]" value="<?= $area_type ?>">
            <div class="from-group">
                <label>Zona</label>
                <select class="form-control" name="filters[area_type_and_id][0][area_id]">
                    <option value="">Tutte</option>
                    <?
                    foreach ($options as $option) {
                        ?>
                        <option value="<?= $option['id'] ?>"><?= $option['text'] ?></option>
                        <?
                    }
                    ?>
                </select>
            </div>
            <?
            height_spacer();
            $content = ob_get_clean();
            $risp->status = 'ok';
            $risp->content = $content;
        } else {
            $risp->status = 'ko';
        }
        echo json_encode($risp);

        break;
        
        
            

    case 'map_agency_marker_content':
        //$agency_id = $_POST['id'];
        $agency_address_id = $_POST['id'];
        include ROOT . '/include/infowindow.agency.php';
        break;

    case 'message_to_an_agency':
        #$data_post['email_to'] = "go@2dopal.com"; #COMMENTARE IN PRODUZIONE
        $risp = new stdClass();

        if ($data_post['email'] != "") {
            $name_and_surname = explode(' ', $data_post['name_and_surname']);
            $request_values['name'] = $name_and_surname[0];
            $request_values['surname'] = $name_and_surname[1];
            $request_values['email'] = $data_post['email'];
            $request_values['telephone'] = $data_post['telephone'];
            $request_values['message'] = $data_post['message'];
            $notify = load_notify('message_to_an_agency', 'it', $request_values);
            if ($request_values['name'] != '')
                $first_send = json_decode(sendgrid($data_post['email_to'], "Richiesta Dal Sito Web", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            if ($first_send['message'] == "success") {
                $risp->status = 1;
                $risp->content = "Messaggio inviato con successo!";
            } else {
                $risp->status = 0;
                $risp->content = "Invio richiesta fallito, riprovare!";
            }
        } else {
            $risp->status = 0;
            $risp->content = "L'indirizzo email è obbligatorio!";
        }
        echo json_encode($risp);
        break;
        
    
        
    case 'message_to_an_agency2':
        #$data_post['email_to'] = "go@2dopal.com"; #COMMENTARE IN PRODUZIONE
        $risp = new stdClass();

        if ($data_post['email'] != "") {
            $name_and_surname = explode(' ', $data_post['name_and_surname']);
            $request_values['name'] = $name_and_surname[0];
            $request_values['surname'] = $name_and_surname[1];
            $request_values['email'] = $data_post['email'];
            $request_values['telephone'] = $data_post['telephone'];
            $request_values['message'] = $data_post['message'];
            $notify = load_notify('message_to_an_agency', 'it', $request_values);
            
    
            if ($request_values['name'] != ''){
                #$first_send = json_decode(sendgrid($data_post['email_to'], "Richiesta Dal Sito Web", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
                $dati = [];
                $dati['html'] = $notify;
                $dati['subject'] = 'Richiesta Dal Sito Web';
                $dati['to'] = $data_post['email_to'];   
                
                $first_send = json_decode(sendgrid2($dati),1);  
                
                if ($first_send['message'] == "success") {
                    
                    $risp->status = 1;
                    $risp->content = "Messaggio inviato con successo!";
                    
                  
                } else {
                    $risp->status = 0;
                    $risp->content = "Invio richiesta fallito, riprovare!";
                }
                
            
            } 
        } else {
            $risp->status = 0;
            $risp->content = "L'indirizzo email è obbligatorio!";
        }
        echo json_encode($risp);
        break;    
        
    
    case 'mailinglist_subscription':
        print_rr($data_post);
        break;

    case 'send_estate_via_email':
        $estate = get_estate_detail($data_post['estate_id']);
        $values['ref'] = $estate['ref'];
        $values['img_link'] = $estate['photos'][0];
        $values['description'] = $estate['description'];
        $values['link'] = SITE_URL . '/product_page.php?estate_id=' . $data_post['estate_id'];
        $values['message'] = $data_post['message'];
        $values['price'] = format_price($estate['price'],$estate['TrattativaRiservata'],0) . " &euro;";
        $notify = load_notify('send_estate_via_email', 'it', $values);
        $first_send = json_decode(sendgrid($data_post['email'], "Un immobile per te!", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
        if ($first_send['message'] == "success") {
            $risp->status = 1;
            $risp->content = "L'immobile è stato inviato con successo!";
        } else {
            $risp->status = 0;
            $risp->content = "Invio richiesta fallito, riprovare!";
        }
        echo json_encode($risp);
        break;

    case 'contact_us_request':
        #$DEFAULT_TO = "go@2dopal.com"; #COMMENTARE IN PRODUZIONE
        $risp = new stdClass();

        $data_post = array_map('trim', $data_post);

        $name_and_surname = explode(' ', $data_post['name_and_surname']);
        $request_values['contact_reason'] = ucwords($data_post['contact_reason']);
        $request_values['name'] = $name_and_surname[0];
        $request_values['surname'] = $name_and_surname[1];
        $request_values['company'] = $data_post['company'];
        $request_values['email'] = $data_post['email'];
        $request_values['telephone'] = $data_post['phone'];
        $request_values['message'] = $data_post['message'];
        $notify = load_notify('contact_us_request', 'it', $request_values);
        $filter = new SpamFilter();

        $result = $filter->check_text($data_post['message']);
        if ($result) {
            $risp->status = 0;
            $risp->content = "Invio richiesta fallito, riprovare!";
        } else {
            if ($request_values['email'] != '' && $request_values['email'] != '' && $request_values['contact_reason'] != '' && $request_values['name'] != '') {
                $first_send = json_decode(sendgrid($GRAFICA, "Richiesta Dal Sito Web - " . ucwords($data_post['contact_reason']) . ' - ' . $data_post['utm_campaign'], $notify, strip_tags($notify), $DEFAULT_FROM), 1);
                $first_send = json_decode(sendgrid('antonio.roffo@italianaimmobiliare.it', "Richiesta Dal Sito Web - " . ucwords($data_post['contact_reason']) . ' - ' . $data_post['utm_campaign'], $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            }
            if ($first_send['message'] == "success") {
                $risp->status = 1;
                $risp->content = "Richiesta inoltrata con successo!";
            } else {
                $risp->status = 0;
                $risp->content = "Invio richiesta fallito, riprovare!";
            }
        }
        echo json_encode($risp);
        break;

    case 'send_request':
        $recipients = array();
        $recipients[] = $data_post['agency_email'];
        $name_and_surname = explode(' ', $data_post['name_and_surname']);
        $request_values['name'] = $name_and_surname[0];
        $request_values['surname'] = $name_and_surname[1];
        $request_values['email'] = $data_post['email'];
        $request_values['telephone'] = $data_post['phone'];
        $request_values['ref'] = $data_post['ref'];
        $request_values['message'] = $data_post['message'];
        if ($data_post['email'] != '') {
            $notify = load_notify('estate_request', 'it', $request_values);
            $request_values['request_date'] = $now;
            $request_values['id'] = $data_post['id'];
            unset($data_post['privacy']);
            $request_id = save_array_in_db($data_post, 'estate_request');
            foreach ($recipients as $recipient) {
                $send_result_json = json_decode(sendgrid($recipient, "Richiesta Immobile", $notify, strip_tags($notify), $DEFAULT_FROM), 1);
            }
            $risp->status = 1;
            $risp->content = "Richiesta inoltrata con successo!";
        } else {
            $risp->status = 0;
            $risp->content = "Devi compilare almeno la mail!";
        }
        echo json_encode($risp);
        break;
        
    case 'send_request2':
        
        $risp = new stdClass();
        $recipients = array();
        $recipients[] = $data_post['agency_email'];
        
        $name_and_surname = explode(' ', $data_post['name_and_surname']);
        $request_values['name'] = $name_and_surname[0];
        $request_values['surname'] = $name_and_surname[1];
        $request_values['email'] = $data_post['email'];
        $request_values['telephone'] = $data_post['phone'];
        $request_values['ref'] = $data_post['ref'];
        $request_values['message'] = $data_post['message'];
        if ($data_post['email'] != '') {
            $notify = load_notify('estate_request', 'it', $request_values);
            $request_values['request_date'] = $now;
            $request_values['id'] = $data_post['id'];
            unset($data_post['privacy']);
            $request_id = save_array_in_db($data_post, 'estate_request');
            
            $dati = [];
            $dati['html'] = $notify;
            $dati['subject'] = 'Richiesta Immobile';
            $dati['to'] = $data_post['agency_email'];      
            $send_result_json = json_decode(sendgrid2($dati),1);            
            
            #foreach ($recepients as $recepient) {                
                #$dati['to'] = $recepient;      
                #$send_result_json = json_decode(sendgrid2($dati),1);                
            #} 
            
            if ($send_result_json['message'] == "success") {
                $risp->status = 1;
                $risp->content = "Richiesta inoltrata con successo!";
            }else{
                $risp->status = 0;
                $risp->content = "Invio richiesta fallito, riprovare!";
            }
            
            
        } else {
            $risp->status = 0;
            $risp->content = "Devi compilare almeno la mail!";
        }
        echo json_encode($risp);
        break;
    
    
    case 'map_estate_marker_content':
        $estate_id = $_POST['id'];
        include ROOT . '/include/infowindow.estate.php';
        break;

    case 'sign-up':

        $risp = new stdClass();
        $user_temp = qa("SELECT * FROM clients WHERE client_email = '" . $data_post['user_email'] . "'");
        if (count($user_temp) > 0) {
            $risp->content = "L'utente esiste già.";
            $risp->status = 0;
        } else {
            $user['client_email'] = $data_post['user_email'];
            $user['client_password'] = md5($data_post['user_password']);
            $user['client_newsletter'] = $data_post['user_newsletter'];
            $user_id = save_array_in_db($user, 'clients');

            $_SESSION['user']['user_id'] = $user_id;

            $risp->status = 1;
            $risp->user_id = $user_id;
        }

        echo json_encode($risp);
        break;

    case 'facebook_login':
        $risp = new stdClass();
        $risp->status = 1;
        $user = qa("SELECT * FROM user WHERE user_facebook_id = " . $data_post['id_facebook']);
        if (count($user) > 0) {
            $_SESSION['user']['user_id'] = $user['user_id'];
            $_SESSION['user']['user_email'] = $user['user_email'];
            $_SESSION['user']['user_name'] = $user['user_name'];
            $_SESSION['user']['user_surname'] = $user['user_surname'];
            $_SESSION['user']['user_mobile'] = $user['user_mobile'];
            $_SESSION['user']['user_newsletter'] = $user['user_newsletter'];
            $_SESSION['user']['user_facebook_id'] = $user['user_facebook_id'];

            $user_favourites = qa("SELECT * FROM user_favourite WHERE user_id = " . $user['user_id']);
            if (count($user_favourites) > 0) {
                foreach ($user_favourites as $favourite) {
                    $_SESSION['user']['user_favourite'][] = $favourite['estate_id'];
                }
                $_SESSION['user']['user_favourite'] = array_unique($_SESSION['user']['user_favourite']);
            }
        } else {
            $user = array();
            $user['user_email'] = $data_post['email'];
            $user['user_name'] = $data_post['first_name'];
            $user['user_surname'] = $data_post['last_name'];
            $user['user_facebook_id'] = $data_post['id_facebook'];

            $user_id = save_array_in_db($user, 'user');

            $_SESSION['user']['user_id'] = $user_id;
            $_SESSION['user']['user_email'] = $user['user_email'];
            $_SESSION['user']['user_name'] = $user['user_name'];
            $_SESSION['user']['user_surname'] = $user['user_surname'];
        }
        echo json_encode($risp);
        break;

    case 'sign-in':
        $risp = new stdClass();
        $user = qa("SELECT * FROM clients WHERE client_email = '" . $data_post['user_email'] . "' AND client_password = '" . md5($data_post['user_password']) . "'");
        if (count($user) > 0) {
            $risp->status = 1;

            $_SESSION['user']['user_id'] = $user['client_id'];
            $_SESSION['user']['user_email'] = $user['client_email'];
            $_SESSION['user']['user_name'] = $user['client_name'];
            $_SESSION['user']['user_surname'] = $user['client_surname'];
            $_SESSION['user']['user_mobile'] = $user['client_mobile'];
            $_SESSION['user']['user_newsletter'] = $user['user_newsletter'];
            $_SESSION['user']['user_facebook_id'] = $user['user_facebook_id'];

            $user_favourites = qa("SELECT * FROM clients_favourites WHERE client_id = " . $user['client_id'], 1);
            if (count($user_favourites) > 0) {
                foreach ($user_favourites as $favourite) {
                    $_SESSION['user']['user_favourite'][] = $favourite['estate_id'];
                }
                $_SESSION['user']['user_favourite'] = array_unique($_SESSION['user']['user_favourite']);
            }
        } else {
            $risp->status = 0;
            $risp->content = "Verifica i dati!";
        }
        echo json_encode($risp);
        break;

    case 'edit_user_data':
         $risp = new stdClass();

        $temp_user = qa("SELECT * FROM clients WHERE client_id != " . $p['user_id'] . " AND client_email = '" . $p['user_email'] . "'");
        if (count($temp_user) > 0) {
            $risp->status = 0;
            $risp->content = "Indirizzo email non valido";
            echo json_encode($risp);
            exit;
        }

        if ($p['user_password'] != "") {
            if ($p['user_password'] != $p['user_password_confirm']) {
                $risp->status = 0;
                $risp->content = "Le password digitate non coincidono";
                echo json_encode($risp);
                exit;
            } else {
                $user['client_password'] = md5($p['user_password']);
            }
        }

        $user['client_email'] = $p['user_email'];
        $user['client_name'] = $p['user_name'];
        $user['client_surname'] = $p['user_surname'];
        $user['client_mobile'] = $p['user_mobile'];
        $user['client_newsletter'] = $p['user_newsletter'];

        $where['client_id'] = $p['user_id'];
        update_array_db('clients', $user, $where);

        $_SESSION['user']['user_email'] = $user['client_email'];
        $_SESSION['user']['user_name'] = $user['client_name'];
        $_SESSION['user']['user_surname'] = $user['client_surname'];
        $_SESSION['user']['user_mobile'] = $user['client_mobile'];
        $_SESSION['user']['user_newsletter'] = $user['client_newsletter'];

        $risp->status = 1;
        $risp->content = d("Modifiche salvate con successo", $_SESSION['lang']);

        echo json_encode($risp);
        break;

    case 'toggle_favourite':
        if (in_array($data_post['estate_id'], $_SESSION['user']['user_favourite'])) {
            unset($_SESSION['user']['user_favourite'][array_search($data_post['estate_id'], $_SESSION['user']['user_favourite'])]);
            if ($data_post['user_id'] > 0) {
                q("DELETE FROM clients_favourites WHERE client_id = " . $data_post['user_id'] . " AND estate_id = " . $data_post['estate_id']);
            }
        } else {
            $_SESSION['user']['user_favourite'][] = $data_post['estate_id'];
            if ($data_post['user_id'] > 0) {
                $new_favourite['client_id'] = $data_post['user_id'];
                $new_favourite['estate_id'] = $data_post['estate_id'];
                save_array_in_db($new_favourite, 'clients_favourites');
            }
        }
        break;
}
?>
