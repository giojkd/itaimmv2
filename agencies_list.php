<!DOCTYPE html>
<head>

<!-- Basic Page Needs
================================================== -->
<title>Agenzie | Italiana Immobiliare</title>
<meta name="description" content="Le agenzie di Italiana Immobiliare. Apri anche tu un'agenzia!"/>

<?include 'include/top.inc.php'; ?>


<style>
    #map-canvas-agencies{
        width:100%;
        height:600px;
    }
    

    .agency-item{
        color:#565a5c;
        border-radius: 3px;
    }

    .agency-item:hover{
        background-color:#ddd;
    }

    .agency-item h4{
        font-weight: bold;
        color:#565a5c;
        height:50px;
    }


    .agency-item-infowindow{
        color:#565a5c;
        border-radius: 3px;
    }


    .agency-item-infowindow h4{
        font-weight: bold;
        color:#565a5c;
    }
</style>


</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Compare Properties Widget
================================================== -->
<?/*include 'include/compare.inc.php'; */?>

<!-- Compare Properties Widget / End -->


<!-- Header Container
================================================== -->

<?include 'include/header.inc.php'; ?>

<!-- Header Container / End -->

<div style="height:70px;"></div>


<!-- Titlebar
================================================== -->
<div id="titlebar" style="background:url(imgs/slider/unclickeseiacasa.jpg); padding:0;">
<div style="padding-top:70px; padding-bottom:70px; background: rgba(255, 255, 255, 0.5); ">

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Le nostre agenzie</h2>
				<p><span>Monitoriamo costantemente il territorio e ti offriamo<br>oltre 2mila immobili in vendita ed in affitto accuratamente selezionati dai nostri esperti</span></p>
				
				<form class="" action="#" method="get">
					<!-- Box -->
					<div class="main-search-box">
						
						<!-- Main Search Input -->
						<div class="main-search-input larger-input justify-content-center">
                            <div class="col-md-7 p-0 m-0">
                            	<!-- Type -->
                            	<div class="row mb-0 justify-content-center">
            						<div class="search-type">
            							<label class="active">L’agenzia della tua zona preferita</label>
            						</div>
                            	</div>
                            	<div class="row mt-0 justify-content-center">                          	
                                	<div id="agenzie_cities" style="width:calc(100% - 120px);">                                    	
                                      	<v-select v-model="selectedZone" :options="zonelisted" placeholder='Seleziona una zona' class="full-width"></v-select>                                            
                						<input type="hidden" v-model="selectedZone.value" name="zona" id="agenzia_zona">                           
                                    </div>
                                    <button class="button" id="button_agenzia_zona"><i class="fas fa-search fa-2x"></i></button> 
                                </div>                                    	
                            </div>
						</div>

					</div>
					<!-- Box / End -->
				</form>
				
			</div>
		</div>
	</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row hidden-xs"> 
		<div class="col-md-12 text-center">
			<h3>Prova a trovarci sulla mappa... basta un click!</h3>				
			<a href="#mappa_agenzie" id="button_to_map"><img src="imgs/map-button.jpg" /></a>
			<div style="height:20px;"></div>
		</div>
	</div>

	<div class="row" id="Firenze">
		<div class="col-md-12" id="agenciesList_Firenze">
			<h2 style="color:#911938" class="text-center">Agenzie di Firenze</h2>
			<br>
			<div v-for="agency in agenciesList_Firenze">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	<div class="row" id="Bagno a Ripoli">
		<div class="col-md-12" id="agenciesList_Bagno_a_Ripoli">
			<h2 style="color:#911938" class="text-center">Agenzie di Bagno a Ripoli</h2>
			<br>
			<div v-for="agency in agenciesList_Bagno_a_Ripoli">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Campi Bisenzio">
		<div class="col-md-12" id="agenciesList_Campi_Bisenzio">
			<h2 style="color:#911938" class="text-center">Agenzia di Campi Bisenzio</h2>
			<br>
			<div v-for="agency in agenciesList_Campi_Bisenzio">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Fiesole">
		<div class="col-md-12" id="agenciesList_Fiesole">
			<h2 style="color:#911938" class="text-center">Agenzia di Fiesole</h2>
			<br>
			<div v-for="agency in agenciesList_Fiesole">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Scandicci">
		<div class="col-md-12" id="agenciesList_Scandicci">
			<h2 style="color:#911938" class="text-center">Agenzia di Scandicci</h2>
			<br>
			<div v-for="agency in agenciesList_Scandicci">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Prato">
		<div class="col-md-12" id="agenciesList_Prato">
			<h2 style="color:#911938" class="text-center">Agenzia di Prato</h2>
			<br>
			<div v-for="agency in agenciesList_Prato">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Pontassieve">
		<div class="col-md-12" id="agenciesList_Pontassieve">
			<h2 style="color:#911938" class="text-center">Agenzia di Pontassieve</h2>
			<br>
			<div v-for="agency in agenciesList_Pontassieve">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	<div class="row" id="Sesto Fiorentino">
		<div class="col-md-12" id="agenciesList_Sesto_Fiorentino">
			<h2 style="color:#911938" class="text-center">Agenzia di Sesto Fiorentino</h2>
			<br>
			<div v-for="agency in agenciesList_Sesto_Fiorentino">
				<!-- Agency -->    			
    			<?include 'include/agencies_list.inc.php'; ?>    			
			</div>			
		</div>
	</div>
	
	
	
</div>

<a name="mappa_agenzie" id="mappa_agenzie"></a>

 <div id="map-canvas-agencies" class="hidden-xs"></div>



<!-- Footer
================================================== -->

<?include 'include/footer.inc.php'; ?>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<?include 'include/scripts.inc.php'; ?>

<script type="text/javascript" src="scripts/agencies_list.js?v=1.2"></script>

</body>
</html>
